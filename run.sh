#!/bin/bash
mpirun -machinefile data_$2/mfile -np $1 -ppn 4 ./$2 -nolocal
cd data_$2
python ekevolve.py
bash datacombine.sh
cd ..
mkdir fig_$2
cd ploter
python $2
