#!/bin/bash
if [ ! -d "backup" ];then 
    mkdir backup
fi
cp *rank*.txt backup
if [ -d ptcls ]; then
    python ./trancate.py
fi
mydirs="ex ey ez bx by bz jx jy jz rho"
for ndir in $mydirs
do
    if [[ `ls *rank*$ndir*".txt"| wc -l` > 0 ]]; then
        rm -rvf $ndir
        mkdir $ndir
        mv *rank*$ndir*".txt" $ndir
        cp gather.py $ndir
        cd $ndir
        python gather.py
        cd ../
        rm -rvf $ndir
    fi
done
mydirl="density_H density_Ele density_Ion ek_Ele* ek_Ion* ek_Pho ek_Pos ek_Car ek_Cu density_Pho density_Car density_Pos density_Cu density_Au ek_Au density_Pro ek_Pro* density_Al ek_Al ek_H"
for ndir in $mydirl
do
    if [[ `ls *rank*$ndir*".txt"| wc -l` > 1 ]]; then
        if [ -d $ndir ]; then
            rm -rvf $ndir
        fi
        mkdir $ndir
        mv *rank*$ndir*".txt" $ndir
        cp gathers.py $ndir
        cd $ndir
        python gathers.py
        cd ../
        rm -rvf $ndir
    fi
done

if [ ! -d ptcls ]; then
    mkdir ptcls
fi

mydirp="_Electron _Ion _Photon _Carbon _Posi _Cu _Au _detector _Proton _Al _H"
for ndir in $mydirp
do
    if [[ `ls *rank*$ndir*".txt"| wc -l` > 1 ]]; then
        ls *rank*$ndir".txt"
        if [ -d $ndir ]; then
            rm -rvf $ndir
        fi
        mkdir $ndir
        mv *rank*[0-9]$ndir*".txt" $ndir
        cp gatherp.py $ndir
        cp sss.py $dir
        cd $ndir
        python gatherp.py
        python sss.py
        cd ..
        mv $ndir/* ptcls
        rm -rvf $ndir
    fi
done

mydirp="dist_Electron dist_Ion dist_Photon dist_Carbon dist_Posi dist_Cu dist_Au dist_detector dist_Proton dist_Al dist_H"
for ndir in $mydirp
do
    if [[ `ls *rank*$ndir*".txt"| wc -l` > 1 ]]; then
        ls *rank*$ndir".txt"
        if [ -d $ndir ]; then
            rm -rvf $ndir
        fi
        mkdir $ndir
        mv *rank*[0-9]_$ndir*".txt" $ndir
        cp gatherdist.py $ndir
        cd $ndir
        python gatherdist.py
        cd ..
        #rm -rvf $ndir
    fi
done


