import os
import shutil as st
ff = os.listdir('.')
pdir = "ptcls"
pf = os.listdir(pdir)
olddir = "oldfiles"
if olddir not in olddir:
    os.mkdir(olddir)
for fi in ff:
    if ".txt" in fi and "rank" not in fi and "time" not in fi:
        st.copyfile(fi, olddir + "/" + fi)
        os.remove(fi)
        print fi + " has been moved to " + olddir

for fi in pf:
    if ".txt" in fi and "rank" not in fi and "time" not in fi:
        st.copyfile(pdir+"/"+  fi, olddir + "/" + fi)
        os.remove(pdir +"/" + fi)
        print fi + " has been moved to " + olddir
