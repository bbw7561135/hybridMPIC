#!/bin/python
import os
import numpy as np
import string
frlist = os.listdir(".")
flist = set("")
for ff in frlist:
    if ".txt" in ff:
        flist.add(ff)
flist = list(flist)
ranklist = set("")
idlist = set("")
fnameprefix = flist[0].split("_")[0] + "_"
newprefix = "./../" + flist[0].split("_")[0] + "_"
fnameappendix = flist[0].split("_")[-1]
for fname in flist:
    #print fname.split("_")[3]
    ranklist.add(string.atoi(fname.split("_")[3]))
    idlist.add(fname.split("_")[1])
ranklist = list(ranklist)
ranklist.sort()
idlist = list(idlist)
idlist.sort()
for ids in idlist:
    mydata = np.loadtxt(fnameprefix + ids + "_rank_" + str(ranklist[0]) + "_" + fnameappendix)
    for rank in ranklist[1:]:
        data = np.loadtxt(fnameprefix + ids + "_rank_" + str(rank) + "_" + fnameappendix)
        mydata = np.concatenate((mydata, data), axis = 1)
    #mydata2 = np.flipud(mydata2)
    np.savetxt(newprefix + ids + "_" + fnameappendix, mydata)
    print newprefix + ids + "_" + fnameappendix

for ff in flist:
    os.remove(ff)

