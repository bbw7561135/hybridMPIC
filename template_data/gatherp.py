#!/bin/python
import os
import numpy as np
import string
frlist = os.listdir(".")
flist = set("")
for ff in frlist:
    if ".txt" in ff:
        flist.add(ff)
flist = list(flist)
ranklist = set("")
idlist = set("")
fnameprefix = flist[0].split("_")[0] + "_"
newprefix = "./" + flist[0].split("_")[0] + "_"
fnameappendix = flist[0].split("_")[-1]
if "detector" in flist[0]:
    fnameappendix = flist[0].split("_")[-2] + "_" + flist[0].split("_")[-1]
for fname in flist:
    ranklist.add(string.atoi(fname.split("_")[3]))
    idlist.add(fname.split("_")[1])
ranklist = list(ranklist)
ranklist.sort()
idlist = list(idlist)
idlist.sort()
mydata = np.zeros((0, 0))
for ids in idlist:
    for myrank in ranklist:
        try:
            data = np.loadtxt(fnameprefix + ids + "_rank_" \
                    + str(myrank) + "_" + fnameappendix)
            if(data.shape[0] != 0):
                if(mydata.shape[0] == 0):
                    mydata = data
                    print data
                else:
                    #print mydata.shape, data.shape
                    mydata = np.concatenate((mydata, data), axis = 0)
        except:
            pass
    if(mydata.shape[0] != 0):
        mydata.sort(axis = 0)
        np.savetxt(newprefix + ids + "_" + fnameappendix, mydata)
    mydata = np.zeros((0, 0))
    #print newprefix + ids + "_" + fnameappendix

for ff in flist:
    os.remove(ff)

