#hybridMPIC
## This is a project of fully relativisitc Particle-In-Cell(PIC) 2d program, with Monte Carlo simulation of QED processes.


####How to compile:
    + make a folder like the para\_testsrc, let's call it test
    + copy the load.inp into this folder then modify it as you wish
    + copy the denfunc.h into this folder then modify as you wish
    + then in the parent foldr, run ./precompile test, you will get the CMakeLists.txt
    + then run make it will generate the program you named in the load.inp


####How to combine the mpi output data:
    + just run the datacombine.sh


####How to plot data:
    + in the ploter folder, a file named "uplotme.py", you just need change the progname value to your progname
    + then make a folder called fig\_progname
    + run the uplotme.py


####this time,
    i have make the random probability for energy = random not random * maxp


    20160626:
        add the load balancing for initialization
