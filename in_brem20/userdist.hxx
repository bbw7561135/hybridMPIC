#include "simbox.h"
#include "hist.hxx"
#include "fileopened.hxx"
#include <cstring>
#include "hist2d.hxx"
#include "userdef.h"
#include "gethost.cxx"
#include "userdef.h"
mFileManager filepool = mFileManager();


double theta_min = -PI, theta_max = PI;
int t_bins = 100;
mHist1d theta_hist = mHist1d(theta_min, theta_max, t_bins);


double ek_min = 0, ek_max = 2000 * MeV;
int ek_bins = 20000;
mHist1d ek_hist = mHist1d(ek_min, ek_max, ek_bins);


//x-px
double xmin = 0, xmax = x_len;
int x_bins = 500;
double pxmin = -1000 * mc, pxmax = 1000 * mc;
int px_bins = 1000;
string prefix = file_name;
mHist2d xpx_hist = mHist2d(xmin, xmax, x_bins, \
        pxmin, pxmax, px_bins, prefix + "momentum_grid");


/// if i want to dump ptcl each step:
// dump ek
std::ofstream ekout;
// dump number
std::ofstream numout;



// instaniate this function to add some global info
void __Simbox::userPreLoop(string& prefix, int rank)
{
    string ekname = prefix + "_num_species.txt";
    numout.open(ekname);
    filepool.registers(&numout);
    ekname = prefix + "_ek_species.txt";
    ekout.open(ekname);
    filepool.registers(&ekout);
    ofstream whereiam("whereiam", ios::app);
    whereiam<<"simulation "<<simname<<"\t in rank "<<rank<<" on host "<<hostname()<<endl;
    whereiam.close();
}


bool condition(const __Ptcls& myptcl)
{
  return true;
}

void dist_myen(const __Ptcls& myptcl, mHist1d& myhist) {
  myhist.record(myptcl.energy, myptcl.weight);
}

void dist_myphi(const __Ptcls& myptcl, mHist1d& myhist){
    double vxy = myptcl.velocity.member[1] * myptcl.velocity.member[1] + \
                 myptcl.velocity.member[2] * myptcl.velocity.member[2];
                 
    double theta = acos(myptcl.velocity.member[1] / sqrt(vxy)) * sign(\
            myptcl.velocity.member[2]);
    myhist.record(theta, myptcl.weight);
}

void dist_mytheta(const __Ptcls& myptcl, mHist1d& myhist){
    double vxy = myptcl.velocity.member[0] * myptcl.velocity.member[0] + \
                 myptcl.velocity.member[1] * myptcl.velocity.member[1];
                 
    double theta = acos(myptcl.velocity.member[0] / sqrt(vxy)) * sign(\
            myptcl.velocity.member[1]);
    myhist.record(theta, myptcl.weight);
}


void dist_xpx(const __Ptcls& myptcl, mHist2d& myhist){
    double px = myptcl.mass * myptcl.velocity.member[0];
    myhist.record(myptcl.position.member[0], px, myptcl.weight);
}


void __Simbox::userStep(string& prefix)
{
  std::vector<double> convec;
  std::vector<double> uservec;
  string filename;
  ekout<<get_field_energy()<<"\t";
  for(int i = 0; i < species; i ++)
  {
      // dump ek for each species
      filename = prefix + "_" + spptcl[i].name + "_ek.txt";
      loopPtcls<mHist1d>(condition, convec, dist_myen, uservec, i, ek_hist);
      ek_hist.write_to_file(filename);
      ek_hist.refresh();
      // dump theta for each species
      loopPtcls<mHist1d>(condition, convec, dist_mytheta, uservec, i, theta_hist);
      filename = prefix + "_" + spptcl[i].name + "_theta.txt";
      theta_hist.write_to_file(filename);
      theta_hist.refresh();
      // dump phi for each species
      loopPtcls<mHist1d>(condition, convec, dist_myphi, uservec, i, theta_hist);
      filename = prefix + "_" + spptcl[i].name + "_phi.txt";
      theta_hist.write_to_file(filename);
      theta_hist.refresh();
      loopPtcls<mHist2d>(condition, convec, dist_xpx, uservec, i, xpx_hist);
      filename = prefix + "_" + spptcl[i].name + "_xpx.txt";
      xpx_hist.write_to_file(filename);
      xpx_hist.refresh();
      // record num and ek
      ekout<<get_sp_energy(i)<<"\t";
      numout<<get_sp_num(i)<<"\t";


  }
  ekout<<std::endl;
  numout<<std::endl;
}

