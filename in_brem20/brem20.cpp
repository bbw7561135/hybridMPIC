#include <iostream>
#include <fstream>
#include <string>
#include "fields.h"
#include "cellinf.h"
#include "cellinf.cxx"
#include "simbox.h"
#include "simbox.cxx"
#include "laser.h"
#include "common.h"
#if __MPI
#include "mpi_data.h"
#endif
#if __OMP
#include "omp.h"
#endif
#include "injector.h"
#include "denfunc.h"
#include "detector.h"
#include "utils.h"
#include "load_balance.h"
using namespace std;

long __Ptcls::total_id = 0;
long __Ptcls::total_num = 0;
long& __Injector::pid = __Ptcls::total_id;
int main(int argc, char* argv[])
{


    int myrank = 0, ranksize = 1;
#if __MPI
    MPI_Status status;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
    MPI_Comm_size(MPI_COMM_WORLD, &ranksize);
#endif
    if(myrank == 0)
    {
        version_info();
        whats_new();
    }

#if __OMP
    omp_set_num_threads(8);
#endif

    string simname = simu_name;
    int x_cell_sim, y_cell_sim;
    double x_len_sim, y_len_sim;
    x_cell_sim = x_cell; y_cell_sim = y_cell;
    x_len_sim= x_len; y_len_sim = y_len;
    __Vect2<double> time_range;
    double dx = x_len_sim / x_cell_sim;
    double dy = y_len_sim / y_cell_sim;
    __Vect2<double> dxy(dx, dy);
        // 有两个是ghost cell
    __Vect2<double> basexymax(x_len_sim, y_len_sim);
    __Vect2<double> basexymin(0, 0);
    

    __Vect2<int> xy_cell_num;

    __Vect2<double> xymin;
    __Vect2<double> xymax;
    calc_simbox(ranksize, myrank, xy_cell_num, xymin, xymax, dx, dy);

	cout<<"rank "<<myrank<<"\twith cells "<<xy_cell_num<<"\tbottom "<<xymin<<endl;
    
    double xmin_box = xymin.member[0], ymin_box = xymin.member[1];
    double xmax_box = xymax.member[0], ymax_box = xymax.member[1];
   
    __Vect2<double> xrange(xmin_box, xmax_box);
    __Vect2<double> yrange(ymin_box, ymax_box);
	
    // ghost cell
    int x_grid_min = xy_cell_num.member[0] * myrank - myrank, x_grid_max = \
                      xy_cell_num.member[0] * (myrank + 1) - myrank;
    int y_grid_min = 0, y_grid_max = xy_cell_num.member[1];
    
    __Vect2<int> xygridmin(x_grid_min, y_grid_min);
    __Vect2<int> xygridmax(x_grid_max, y_grid_max);

    string fname = file_name;
    __Vect2<double> laser0_pos(0, y_len_sim * 0.5);
    __Vect2<double> laser1_pos(x_len_sim - 2.0 * dxy.member[0], y_len_sim * 0.5);
    
    double gamma = 2000; // 10 MeV
    double vx = c_light_speed * sqrt(1.0 - 1.0 / gamma / gamma);
    __Vect3<double> velocity(vx, 0, 0.0);
    /// species to be merged
    vector<int> msp = MSP;
    int max_ppc = MAX_PPC;
    double delta_e = MERGE_BIAS_E;
    double delta_v = MERGE_BIAS_V;
    __Vect2<int> position(myrank, 0);

    // 没有设置，以后在设置
    int bd_type[4] = bc_type;
    if(myrank != ranksize - 1 && ranksize > 1) bd_type[3] = 3;

    vector<string> fname1d, fname2d;
    string sigmabr, sigmabh;
    sigmabr = SIGMABR;
    sigmabh = SIGMABH;
    //files for nonlinear Compton scattering and Breit-Wheeler pair prodution
    fname1d.push_back("tables/etaforp.txt");
    fname1d.push_back("tables/chiforp.txt");
    fname1d.push_back("tables/fforp.txt");
    fname1d.push_back("tables/heta.txt");
    fname1d.push_back("tables/tchi.txt");
    fname1d.push_back("tables/chifort.txt");
    fname1d.push_back("tables/etaforh.txt");
    fname1d.push_back("tables/ele_ek.txt");
    fname1d.push_back("tables/xrange.txt");
    fname2d.push_back("tables/petachi.txt");
    fname2d.push_back("tables/pchif.txt");
    fname2d.push_back("tables/chietap.txt");
    //Bremsstrahlung and Bethe-Heitler pair production cs files.
    fname2d.push_back(sigmabr);
    fname2d.push_back(sigmabh);
    int startind = damp_length;
    // 下上左右
    int startindex[4] = {startind, int(y_cell_sim) - startind, startind, \
        xy_cell_num.member[0] - startind};
    int damp_len[4] = {startind, startind, startind, startind};
	const int species = species_all;
    int dump_ek[species] = DUMP_EK;
    int dump_ptcls[species] = DUMP_PTCLS;
    int dump_density[species] = DUMP_DENSITY;
	double delta_t;
    delta_t = dx / c_light_speed / sqrt(2.0) * 0.8;
    /***
    if(DISABLE_EM)
    {
#ifdef DELTA_T
        delta_t = DELTA_T;
#endif
    }
    ***/
    int t_steps = (time.member[1] - time.member[0]) / delta_t;
    double wavelength = wave_length;
    double duration = duration_um; // cTl = 3.2 um
    double t_center = laser_center;
    double waist = laser_waist;
    double scalea = scaled_a;
    double omega = laser_omega;
    double intensity = laser_intensity;
    double amp = laser_amplitude;
    int polarization = polarize; // z polarized
    double dumpsec = dump_in_sec;
    int dumper = dumpsec / delta_t;
    //dumper = 1;
    int ifdump = DUMP;
    vector<Particles> spptcls;
    vector<int> ppc = {};
#ifdef __INITSP
    init_sp(spptcls, ppc);
#endif
    if(myrank == 0)
    {
        print_star(60);
        cout<<" simulation domain: "<<xrange<<"\t"<<yrange<<endl;
        cout<<" cell size: "<<xy_cell_num<<endl;
        cout<<" simulation time: "<<time<<endl;
        cout<<" boundary type "<<bd_type[0]<<"\t"<<bd_type[1]<<"\t"<<bd_type[2]\
            <<"\t"<<bd_type[3]<<endl;
        cout<<" Intensity = "<<intensity<<" w/m^2"<<endl;
        cout<<" amp = "<<amp<<endl;
        cout<<" dx "<<dx<<endl;
        cout<<" corant condition "<<(dx > c_light_speed * delta_t)<<endl;
        cout<<" delta_t = "<<delta_t<<endl;
        cout<<" steps: "<<t_steps<<endl;
        cout<<" wavelength = "<<wavelength<<endl;
        cout<<" laser period per dx = "<<wavelength / dx<<endl;
        cout<<" propagate dx need dt "<<wavelength / c_light_speed / delta_t<<endl;
#if DUMP
        ofstream myout;
	    string outname;
	    outname = fname + "info.txt";
        myout.open(outname);
	    myout<<0<<"\t"<<0<<"\t"<<x_len_sim<<"\t"<<y_len_sim<<endl;
	    myout.close();
#endif
        // 两个约束条件，corant condition，omegape * dt < 2
#if !DISABLE_EM
        if(dx < c_light_speed * delta_t)
        {
            print_star(40);
            print_star(40);
            cout<<" corant condition been violated!"<<endl;
            print_star(40);
            print_star(40);
            exit(1);
            return 1;
        }
#endif
        cout<<" dumper = "<<dumper<<" delta t "<<delta_t<<endl;
        print_star(60);
        cout<<endl<<endl;
        cout<<".........PRESS ENTER TO CONTINUE........."<<endl;
        //getchar();
    }
#if __MPI
    MPI_Barrier(MPI_COMM_WORLD);
#endif
    /// 0 不需要开启，１需要开启, 顺序0-syn, 1-brem, 2-bwpair, 3-bhpair
    int monte_flag[4] = mc_flag;
	int maxid = 0;
    __Ptcls tempptcls;
    int local_init_id = max_ppc * species * xy_cell_num.member[0] * xy_cell_num.member[1] * myrank;
    tempptcls.total_id = local_init_id;
    __Simbox mybox(simname, \
			xygridmin, xygridmax, xy_cell_num, xrange, \
			yrange, time,t_steps, delta_t, species, spptcls,\
            maxid, ppc, position, DISABLE_EM);
    mybox.set_data_file(fname1d, fname2d);
    mybox.set_monte_carlo(monte_flag);
    mybox.set_field_boundary(bd_type, startindex, damp_len);
    mybox.init(fname, myrank);
    //cout<<" rank "<<myrank<<" box init over "<<endl;
    mybox.max_ppc = max_ppc;
    mybox.delta_e = delta_e;
    mybox.delta_v = delta_v;
    __Cellgroup& mygroup = mybox.cellgroup;

    //cout<<" rank "<<myrank<<" initing ptcls"<<endl<<endl;
    //mygroup.init_ptcls();
    init_ptcls(mygroup, myrank);
    //cout<<" rank "<<myrank<<" ptcls init over "<<endl<<endl;
    //mygroup.set_sp_velocity(0, velocity);
    mybox.init_flag();
    __Vect2<int> gridsize(mybox.fieldgroup.member.size(), mybox.fieldgroup.member[0].size());
    __Laser mylaser0(0, 0, delta_t, t_center, duration, laser0_pos, wavelength, xymin, xymax,\
                gridsize, dxy, waist, amp, polarization);
    //__Laser mylaser1(1, 0, delta_t, t_center, duration, laser1_pos, wavelength, xymin, xymax,\
                gridsize, dxy, waist, amp, polarization);
    __Vect2<int> ipos(1, y_cell_sim / 2);
    long total_inject = 1e7;
    __Vect2<double> ekrange(MeV, 10.0 * MeV);
    __Vect2<double> detect_xymin(100 * UNITS, 2 * UNITS);
    __Vect2<double> detect_xymax(101 * UNITS, 150 * UNITS);
    __Detector mydetector(0, 3, ekrange, detect_xymin, detect_xymax, dxy);
    mydetector.init(mybox);
#if DUMP
    /// open a file for time step out put
    string tfile = fname + "time.txt";
    ofstream myout;
    myout.open(tfile);
#endif
    if(myrank == 0)
    {
        cout<<" max steps "<<t_steps<<endl;
    }
    for(int i = 0; i < t_steps; i++)
    {
        long pdata_tags[8] = {i, t_steps + i, 2 * t_steps + i, 3 * t_steps + i, \
            4 * t_steps + i, 5 * t_steps + i, 6 * t_steps + i, 7 * t_steps + i};
        long fdata_tags[16] = {\
            8 * t_steps + i, 9 * t_steps + i, 10 * t_steps + i, 11 * t_steps + i, \
            12 * t_steps + i, 13 * t_steps + i, 14 * t_steps + i, 15 * t_steps + i, \
            16 * t_steps + i, 17 * t_steps + i, 18 * t_steps + i, 19 * t_steps + i, \
            20 * t_steps + i, 21 * t_steps + i, 22 * t_steps + i, 23 * t_steps + i};
        // 向左发送6个，三个size，三个data
        // 向右同，先Field，后ptcls, 再cellfield

        if(myrank == 0)
        {
            cout<<" step : "<<i<<endl;
        }
#if __Debug
        cout<<myrank<<" size "<<mybox.get_sp_num(0)<<endl;
#endif
	int dumpid = i / dumper;
        string prefix = getPrefix(fname, myrank, dumpid);
        if(myrank == 0 && DISABLE_EM == 0)
        {
            mylaser0.update();
            mylaser0.setting_fields(mybox);
        }
        /***
        if(myrank == ranksize - 1 && DISABLE_EM == 0)
        {
            mylaser1.update();
            mylaser1.setting_fields(mybox);
        }
        ***/
/// 第一步是更新电磁场的B
#if __MPI
        mpi_exchange_bfields_lr(myrank, ranksize, mybox, fdata_tags, status);
        //MPI_Barrier(MPI_COMM_WORLD);
        shift_data_tags(fdata_tags);
#endif
#if __Debug
        cout<<myrank<<" before update 0.5 E pre "<<endl;
#endif
        //0.5 E
        mybox.advancest2(myrank, i);
#if __MPI
        mpi_exchange_efields_lr(myrank, ranksize, mybox, fdata_tags, status);
        //MPI_Barrier(MPI_COMM_WORLD);
        shift_data_tags(fdata_tags);
#endif
        // 0.5 B
#if __Debug
        cout<<myrank<<" before update 0.5 B pre "<<endl;
#endif
        mybox.advancest1(myrank, i);
#if __MPI
        mpi_exchange_bfields_lr(myrank, ranksize, mybox, fdata_tags, status);
        //MPI_Barrier(MPI_COMM_WORLD);
        shift_data_tags(fdata_tags);
#endif
#if __Debug
        cout<<myrank<<" before adst4 "<<endl;
#endif
        //weight to ptcls, mc, advance
        mybox.advancest4(myrank, i);
#if __MPI
        mpi_exchange_ptcls(myrank, ranksize, mybox, pdata_tags, status);
        //MPI_Barrier(MPI_COMM_WORLD);
#endif
/// 将越界粒子发送到自己的网格，将粒子产生的电荷与电流weight到网格上
#if __Debug
        cout<<myrank<<" before adst5 "<<endl;
#endif
        mybox.advancest5(myrank, i);
#if __MPI
        mpi_exchange_grid(myrank, ranksize, mybox, fdata_tags, status);
        //MPI_Barrier(MPI_COMM_WORLD);
        shift_data_tags(fdata_tags);
        mpi_exchange_current_lr(myrank, ranksize, mybox, fdata_tags, status);
        //MPI_Barrier(MPI_COMM_WORLD);
        shift_data_tags(fdata_tags);
#endif
#if __Debug
        cout<<myrank<<" before cell copy "<<endl;
#endif
        mybox.cell_copy_to_field(0, myrank);
#if __MPI
        //MPI_Barrier(MPI_COMM_WORLD);
#endif
#if __Debug
        cout<<myrank<<" before update 0.5 B post "<<endl;
#endif
        // 0.5 B
        mybox.advancest3(myrank, i);

#if __MPI
        mpi_exchange_bfields_lr(myrank, ranksize, mybox, fdata_tags, status);
        //MPI_Barrier(MPI_COMM_WORLD);
        shift_data_tags(fdata_tags);
#endif
#if __Debug
        cout<<myrank<<" before update 0.5 E post "<<endl;
#endif
        //0.5 E
        mybox.advancest2(myrank, i);
#if __MPI
        mpi_exchange_efields_lr(myrank, ranksize, mybox, fdata_tags, status);
        MPI_Barrier(MPI_COMM_WORLD);
        shift_data_tags(fdata_tags);
#endif
#if __Debug
        cout<<myrank<<" before squeeze "<<endl;
#endif
        mybox.squeeze();
        if(i % 10 == 0)
        {
            for(int imsp = 0; imsp < species; imsp ++)
            {
                if(msp[imsp] == 1)
                {
                    mybox.merge(imsp);
                }
            }
        }
        if(i%dumper == 1 && ifdump == 1)
        {
            mybox.userStep(prefix);
            if(myrank == 0)
            {
                cout<<" dumping "<<i<<" time "<<i * delta_t * 1e15<<" fetosec."<<endl;
#if DUMP
                myout<<dumpid<<"\t"<<delta_t * i<<endl;
#endif
            }
            if(!DISABLE_EM)
            {
#if Dump_rho
                mybox.fieldgroup.dump(myrank, prefix, 0);
#endif
#if Dump_efield

                // 0 rho, 1 ex, 6 bz, 7 jx
                mybox.fieldgroup.dump(myrank, prefix, 1);
                mybox.fieldgroup.dump(myrank, prefix, 2);
                mybox.fieldgroup.dump(myrank, prefix, 3);
#endif
#if Dump_bfield
                mybox.fieldgroup.dump(myrank, prefix, 4);
                mybox.fieldgroup.dump(myrank, prefix, 5);
                mybox.fieldgroup.dump(myrank, prefix, 6);
#endif
#if Dump_jfield
                mybox.fieldgroup.dump(myrank, prefix, 7);
                mybox.fieldgroup.dump(myrank, prefix, 8);
                mybox.fieldgroup.dump(myrank, prefix, 9);
#endif
                // 0-e- 1 ion 2 e+ 3 gamma
            }
#if Dump_density
            for(int spp = 0; spp < species; spp ++)
            {
                if(dump_density[spp] == 1) mybox.cellgroup.dump(myrank, prefix, spp);
            }
#endif
#if Dump_ek
            for(int spp = 0; spp < species; spp ++)
            {
                if(dump_ek[spp] == 1) mybox.cellgroup.dump(myrank, prefix, spp + species);
            }
#endif
#if Dump_ptcls
            for(int spp = 0; spp < species; spp ++)
            {
                if(dump_ptcls[spp] == 1) mybox.cellgroup.dump_ptcls(myrank, prefix, spp);
            }
#endif
        }
        if(i == EXIT)
        {
            //mydetector.dump(myrank, i, fname);
#if DUMP
            myout.close();
#endif
#if __MPI
            MPI_Finalize();
#endif
            return 0;
        }
    }
#if DUMP
    myout.close();
#endif
    //mydetector.dump(myrank, t_steps, fname);
#if __MPI
    MPI_Finalize();
#endif
    return 0;
}
