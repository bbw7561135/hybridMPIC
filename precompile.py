#!/usr/bin/python
import sys
if len(sys.argv) < 2:
    inf = "load.inp"
    denfunc = "denfunc.h"
else:
    inf = sys.argv[1] + "/load.inp"
    denfunc = sys.argv[1] + "/denfunc.h"
folder = sys.argv[1]
print "using inp file " + inf
outf = folder + "/userdef.h"
ofile = open(outf, 'w')
file_obj = open(inf, 'r')
ofile.writelines("#ifndef __User\n")
ofile.writelines("")
ofile.writelines("#define __User\n")
ofile.writelines("#include \"physicalconstants.h\"\n")
mpiflag = 0
ompflag = 0
debugflag = 0
prgname = ""
for line in file_obj:
    if '__Debug' in line and '1' in line:
        debugflag = 1
        print line
    if '#' in line:
        continue
    if line != '' and '=' in line and "#" not in line and "mpi" not in line and "omp" not in line:
        print "setting ", line
        var = line.split('=')
        if var[0].strip() == "program":
            print 'var 0', var[0], var[-1]
            prgname = var[-1].strip()
        else:
            ofile.writelines("#define " + var[0] + " " + var[-1])
    if "use_mpi" in line:
        if "1" in line:
            mpiflag = 1
            print "will use mpi"
            ofile.writelines("#define __MPI 1\n")
        else:
            print "will run in seriel mode without MPI support"
    if "omp" in line:
        if "1" in line:
            ompflag = 1
            print "will use omp"
            ofile.writelines("#define __OMP 1\n")
        else:
            ofile.writelines("#define __OMP 0\n")
            print "will run in seriel mode without omp support"

ofile.writelines("#endif")
file_obj.close()
ofile.close()

outf = "CMakeLists.txt"
ofile = open(outf, 'w')
print "will generate ", prgname

openmpipara = ''
debugpara = ''
compiler = "g++"
if ompflag:
    openmpipara = ' -fopenmp '
if debugflag:
    debugpara = ' -g -O0 '
    print "will run in debug mode"
else:
    debugpara = ' -O6 '
if mpiflag:
    compiler = "mpic++"
cxx_flags = openmpipara + debugpara + "-fPIC -std=c++11 -static"
ofile.writelines("cmake_minimum_required(VERSION 3.1)\n")
ofile.writelines("PROJECT(" + prgname + ")\n")
ofile.writelines("FILE(GLOB SRC \"" + folder +"/" + prgname + ".cpp\")\n")
ofile.writelines("INCLUDE_DIRECTORIES(\"./src\" \"" + "./" + folder + "\")\n")
ofile.writelines("SET(CMAKE_CXX_COMPILER \"" + compiler + "\")\n")
ofile.writelines("SET(CMAKE_CXX_FLAGS \"" + cxx_flags + "\")\n")
ofile.writelines("ADD_EXECUTABLE(" + prgname + " ${SRC})\n")
ofile.close()
