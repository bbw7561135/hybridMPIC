#ifndef __User
#define __User
#include "physicalconstants.h"
#define     DPHOTON   1
#define     UNITS   1E-6
#define     T_UNITS   1E-15
#define     wave_length   1e-6
#define     duration_um   30 * T_UNITS * c_light_speed
#define     laser_center   15e-15
#define     laser_waist   1e-6
#define     scaled_a   5
#define     laser_omega   2.0 * PI * c_light_speed / wave_length
#define     laser_intensity   1e24 
#define     laser_amplitude   2.0 * sqrt(intensity / c_light_speed / epsilon_0)
#define     DENSITY   0.05 * ncrit(omega)
#define     polarize   1 
#define     dump_in_sec   1 * T_UNITS
#define     species_all   4
#define     damp_length   8
#define     bc_type   {1, 1, 3, 3}
#define     time_range   time(0, 80 * T_UNITS)
#define     x_cell   400
#define     y_cell   400
#define     x_len   8 * UNITS
#define     y_len   8 * UNITS
#define     simu_name   "test"
#define     mc_flag   {1, 0, 0, 0}
#define     file_name   "./data_syn20/syn20_"
#define     ekmin   1022 * KeV
#define     SIGMABR   "tables/csbr79.txt"
#define     SIGMABH   "tables/cspp79.txt"
#define     ELECTRON_EK   "tables/ele_ek.txt"
#define     ENERGY_RATIO   "tables/xrange.txt"
#define     EXIT   9400
#define     MAX_PPC   25
#define     MSP   {0, 0, 0, 0, 0}
#define     MERGE_BIAS_E   0.2
#define     MERGE_BIAS_V   0.2
#define     DISABLE_EM   0
#define     DENSITY_CHANGE   1
#define     NUM_THREADS   8
#define __OMP 0
#define     MAX_NUM_THREADS   8
#define     MAX_NUM_DOMAIN   1.0E+3
#define     PHOTON_DYNAMICS   1
#define     __Debug   0
#define     DUMP   1
#define     Dump_rho   0
#define     Dump_ptcls   0
#define     Dump_angular   1
#define     DUMP_PTCLS   {1, 0}
#define     Dump_bfield   1
#define     Dump_efield   1
#define     Dump_jfield   1
#define     Dump_ek   1
#define     DUMP_EK   {1, 1, 0, 1}
#define     Dump_density   1
#define     DUMP_DENSITY   {1, 1, 0, 1}
#define     Dist_species_all   3
#define     Dist_species   {0, 1, 3}
#define     Dist_start   50 * KeV
#define     Dist_end   2000 * MeV
#define     Dist_dk   10.0 * KeV
#define     target_id   1
#endif