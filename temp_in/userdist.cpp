#include "simbox.h"
#include "hist.hxx"
__Simbox::userDist()
{
  //angular
  double theta_min = 0, theta_max = 2.0 * PI;
  int t_bins = 100;
  mHist1d theta_hist = mHist1d(theta_min, theta_max, t_bins);
  double ek_min = 50 * KeV, ek_max = 1000 * MeV;
  int ek_bins = 2000;
  mHist1d ek_hist = mHist1d(ek_min, ek_max, ek_bins);
}

__Simbox::userStep()
{
  std::vector<double> convec;
  std::vector<double> uservec;
  loopPtcls(condition, convec, userfunc, uservec, 0, ek_hist);
  string filename = "test.txt";
  ek_hist.write_to_file(filename);
  ek_hist.refresh();
}


bool condition(__Ptcls& myptcl)
{
  return true;
}

void userfunc(__Ptcls& myptcl, mHist1d& myhist) {
  myhist.record(myptcl.energy, myptcl.weight);
}
