#ifndef __USRDENS
#define __USRDENS
#include "type.h"
#define densitye 1e24
#define ATOM_NUM  79.0
#define ngold 5.89e28
#define nhy 5.3e24
#define nal 6.02e28
#include "physicalconstants.h"
#include "common.h"
#include "userdef.h"
void calc_density(const __Vect2<double>& position, vector<double>& density)
{
    //__Vect2<double>  pos1(2.0 * UNITS, 1.0 * UNITS), pos2(79 * UNITS, 29 * UNITS);
    __Vect2<double>  pos1(2 * UNITS, 0.0 * UNITS), pos2(4 * UNITS, 8 * UNITS);
    double omega = 1.78e15;
    double ncritt = ncrit(omega);
    
    if(position > pos1 && position < pos2)
    {
        //density = {0.05 * ncritt, 0.05 * ncritt, 0, 0, 0};
        density = {79 * ngold, ngold, 0, 0};
    }
}
    
#endif



#ifndef __INITSP
#define __INITSP

void init_sp(vector<Particles>& spptcls, vector<int>& ppc)
{
    /// particle species struct: {name , sp_id, mass, charge, energy, position, velocity}
    __Vect3<double> velocity(2e8, 0, 0), zeros(0, 0, 0);
    __Vect2<double> zero2(0, 0);
    Particles electrons = {"Electron", 0, e_mass, - e_charge, 0, zero2, zeros};
    Particles ions = {"Au", 1, 197 * 1837 * e_mass, e_charge * 79, 0.0, zero2, zeros};
    Particles position = {"Positron", 2, e_mass, e_charge, 0.0, zero2, zeros};
    Particles photon = {"Photon", 3, 0, 0, 0, zeros};
    Particles Carbon = {"Carbon", 4, 12 * 1837 * e_mass, 6 * e_charge, 0.0, zero2, zeros};
    spptcls.push_back(electrons);
    spptcls.push_back(ions);
    spptcls.push_back(position);
    spptcls.push_back(photon);
    /***
    spptcls.push_back(Carbon);
    ***/
    ppc = {50, 10, 0, 0};
}
#endif
