clear all
mfmin = 1e-6; mfmax = 0.999;
mysize = 10000;
myf = linspace(log(mfmin), log(mfmax), mysize);
myf = exp(myf);
mdata = zeros(mysize, 4);
fid = fopen('pf.txt', 'w+');
for i = 1: mysize
    mdata(i, :) = [myf(i), pf(myf(i), 0.1), pf(myf(i), 10), pf(myf(i), 100)]; 
end
mdata(:, 2) = mdata(:, 2) / sum(mdata(:, 2));
mdata(:, 3) = mdata(:, 3) / sum(mdata(:, 3));
mdata(:, 4) = mdata(:, 4) / sum(mdata(:, 4));
save mdata.txt mdata -ascii -double
