# this is the reduced photon energy used by penelopy
import numpy as np
reducedeV = [1.0E-12
                                                            , 0.025
                                                            , 0.05
                                                            , 0.075
                                                            , 0.1
                                                            , 0.15
                                                            , 0.2
                                                            , 0.25
                                                            , 0.3
                                                            , 0.35
                                                            , 0.4
                                                            , 0.45
                                                            , 0.5
                                                            , 0.55
                                                            , 0.6
                                                            , 0.65
                                                            , 0.7
                                                            , 0.75
                                                            , 0.8
                                                            , 0.85
                                                            , 0.9
                                                            , 0.925
                                                            , 0.95
                                                            , 0.97
                                                            , 0.99
                                                            , 0.995
                                                            , 0.999
                                                            , 0.9995
                                                            , 0.9999
                                                            , 0.99995
                                                            , 0.99999
                                                            , 1.0]
ou = np.zeros((len(reducedeV)))
i = 0
for r in reducedeV:
    ou[i] = r
    i = i + 1
np.savetxt("greduce.txt", ou)

