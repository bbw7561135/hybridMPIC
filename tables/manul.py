# python calc_intcs.py z pdbxx.txt
# calculate the integrated cs for brems, pdbxx.txt is duplicated from the pdebr.xx.08
# by the read.cpp
import numpy as np
import sys
eV = 1.6e-19
mc2 = 0.511 * 1e6 * eV
units = 1e-31 # they had used 1e-27 * cm^2
ngrid = 500*1.j
def calc_intcs():
    if len(sys.argv) < 2:
        return
    z = int(sys.argv[1])
    pdbname = sys.argv[2]
    pdb = np.loadtxt(pdbname)
    greduce = np.loadtxt("greduce.txt")
    pdbek = np.loadtxt("pdbek.txt") * eV
    newx = np.mgrid[greduce[0]:greduce[-1]:ngrid]
    newek = np.exp(np.mgrid[np.log(pdbek[0]):np.log(pdbek[-1]):ngrid])
    temp1 = np.zeros((len(pdbek), len(newx)))
# yinterp = np.interp(xvals, x, y)
    for i in np.arange(len(pdbek)):
        temp1[i, :] = np.interp(newx, greduce, pdb[i, :])
    temp2 = np.zeros((len(newek), len(newx)))
    res = np.zeros(temp2.shape)
    for i in np.arange(len(newx)):
        temp2[:, i] = np.interp(newek, pdbek, temp1[:, i])
    print 'interpolation over'
    beta = np.sqrt(newek * (newek + 2.0 * mc2)) / (newek + mc2)
    ibeta = 1.0 * beta
    for i in np.arange(len(newek)):
        zbeta = z * z #* ibeta[i] * ibeta[i]
        npdb = temp2[i, :]
        print 'calc ',i,'th row'
        for j in np.arange(len(newx) - 1):
            thisx = 0.5 * (newx[j] + newx[j + 1])
            dx = newx[j + 1] - newx[j]
            thispdb = 0.5 * (npdb[j + 1] + npdb[j])
            res[i, j + 1] = res[i, j] + thispdb / thisx * dx
        res[i, :] = res[i, :] * zbeta
    res = res * units
    np.savetxt("csbr" + str(z) + ".txt", res)
    np.savetxt("xrange.txt", newx)
    np.savetxt("ele_ek.txt", newek)
    np.savetxt("newpdb.txt", temp2)
calc_intcs()



