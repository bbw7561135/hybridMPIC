function ret = pf(f, chi)
fmf = 1.0 / (f * (1.0 - f));
pre = 2.0 / fmf + 1.0;
poly = besselK(2.0 / 3.0, 1.0 * fmf / chi);
ret = pre * poly;
end