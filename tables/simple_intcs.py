# python calc_intcs.py z pdbxx.txt
# calculate the integrated cs for brems, pdbxx.txt is duplicated from the pdebr.xx.08
# by the read.cpp
import numpy as np
import sys
eV = 1.6e-19
mc2 = 0.511 * 1e6 * eV
units = 1e-31 # they had used 1e-27 * cm^2
def calc_intcs():
    if len(sys.argv) < 2:
        return
    z = int(sys.argv[1])
    pdbname = sys.argv[2]
    pdb = np.loadtxt(pdbname)
    greduce = np.loadtxt("xrange.txt")
    pdbek = np.loadtxt("pdbek.txt") * eV
    beta = np.sqrt(pdbek * (pdbek + 2.0 * mc2)) / (pdbek + mc2)
    ibeta = 1.0 / beta
    res = np.zeros((pdb.shape))
    for i in np.arange(pdb.shape[0]):
        zbeta = z * z * ibeta[i] * ibeta[i]
        for j in np.arange(pdb.shape[1]):
            print i, j
            if j == 0:
                res[i, j] = pdb[i, j]
                print res[i, j], pdb[i, j]
            else:
                res[i, j] = res[i, j - 1] + pdb[i, j] / greduce[j] * (greduce[j] - greduce[j - 1])
        res[i, :] = res[i, :] * zbeta
    res = res * units
    np.savetxt("csbr" + str(z) + ".txt", res)
    np.savetxt("ele_ek.txt", pdbek)
calc_intcs()



