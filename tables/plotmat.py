#plot 1d or 2d matrix
#if data is 1d use line plot
#if data is 2d use imshow
#if with log will plot in log form
#python plotmat.py mat.txt [ext xmin, xmax, ymin, ymax] [log]
#save as mat.png
import sys
import numpy as np
import matplotlib.pyplot as plt
logflag = 0
xmin = 0
ymin = 0
xmax = 0
ymax = 0
extflag = 0
if len(sys.argv) > 2:
    if sys.argv[2] == "ext":
        xmin = float(sys.argv[3])
        xmax = float(sys.argv[4])
        ymin = float(sys.argv[5])
        ymax = float(sys.argv[6])
        extflag = 1
if '.txt' in sys.argv[1]:
    if len(sys.argv) > 2:
        if 'log' == sys.argv[-1]:
            logflag = 1
    print "ploting ", sys.argv[1]
    data = np.loadtxt(sys.argv[1])
    fname = sys.argv[1].split('.')[0] + '.png'
    if len(data.shape) == 1:
        plt.plot(data)
        if logflag:
            plt.yscale('log')
        if extflag:
            plt.xlim([xmin, xmax])
            plt.ylim([ymin, ymax])
    elif data.shape[1] == 2:
        plt.plot(data[:, 0], data[:, 1], linewidth = 5)
        if logflag:
            plt.yscale('log')
        if extflag:
            plt.xlim([xmin, xmax])
            plt.ylim([ymin, ymax])
    else:
	if logflag:
            data = np.log(data) / np.log(10)
        if extflag:
            ext = [xmin, xmax, ymin, ymax]
            print ext
            plt.imshow(data, extent = ext, aspect = "auto")
        else:
            plt.imshow(data)
        plt.colorbar()
    plt.savefig(fname)
