import numpy as np
import matplotlib.pyplot as plt


def geta(eta):
    return (1.0 + 4.8 * (1.0 + eta) * np.log(1.0 + 1.7 * eta) + 2.44 * eta * eta) ** -(2.0 / 3)

eta0 = 1e-3
eta1 = 10.0
myeta = np.exp(np.mgrid[np.log(eta0):np.log(eta1):500j])

plt.plot(np.log10(myeta), np.log10(geta(myeta)))
plt.savefig("geta.png")
plt.close()

