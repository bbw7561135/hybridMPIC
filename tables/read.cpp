#include<iostream>
#include<fstream>
#include<cstring>
using namespace std;
int main(int argc, char* argv[])
{
    if(argc <= 1) return 0;
    ifstream in;
    in.open(argv[1]);
    double temp;
    in>>temp;
    string oname = "pdb" + to_string(int(temp)) + ".txt";
    string ekname = "pdbek.txt";
    ofstream out, oek;
    out.open(oname);
    oek.open(ekname);
    int i = 0;
    while(!in.eof())
    {
        cout<<i<<endl;
        in>>temp;
        i++;
        if(i % 34 == 1)
        {
            oek<<temp<<endl;
            out<<endl;
            continue;
        }
        if(i % 34 == 0) continue;
        out<<temp<<"\t";
    }
    in.close();
    out.close();
    oek.close();
    return 0;
}


