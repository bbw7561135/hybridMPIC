function plotmat(matfile)
data = load(matfile, '-ascii');
plot(data(:, 1), data(:, 2));
end
