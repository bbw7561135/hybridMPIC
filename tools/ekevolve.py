import numpy as np
import matplotlib.pyplot as plt
import os
namelist = os.listdir('.')
thisname = []
tdict = {}
species = 'Photon'
for name in namelist:
    if 'dist' in name and species in name:
        thisname.append(name)
    if 'time' in name and '.txt' in name:
        tdata = np.loadtxt(name)
for i in np.arange(tdata.shape[0]):
    tdict[tdata[i, 0]] = tdata[i, 1]
res = np.zeros((0, 2))
num = np.zeros((0, 2))
print res.shape
thisname.sort()
print thisname
#print tdict
for na in thisname:
    for keys in tdict:
        if int(keys) < 10:
            mkey = '0000' + str(int(keys))
        elif int(keys) < 100:
            mkey = '000' + str(int(keys))
        elif int(keys) < 1000:
            mkey = '00' + str(int(keys))
        elif int(keys) < 10000:
            mkey = '0' + str(int(keys))
        if mkey in na:
            print na, str(int(keys))
            #data = np.loadtxt(na)[104:, :]
            data = np.loadtxt(na)
            print data[0, 0]
            thisres = np.sum(data[:, 0] * data[:, 1])
            res = np.concatenate((res, np.array([[tdict[keys], thisres]])), axis = 0)
            num = np.concatenate((num, np.array([[tdict[keys], np.sum(data[:, 1])]])), axis = 0)

np.savetxt(species + "ek.txt", res)
np.savetxt(species + "num.txt", num)
plt.plot(res[:, 0], res[:, 1], linewidth = 3)
plt.savefig(species + 'ek.png')
plt.close()
plt.plot(num[:, 0], num[:, 1], linewidth = 3)
plt.savefig(species + 'num.png')
plt.close()


namelist = os.listdir('.')
thisname = []
tdict = {}
species = 'Positron'
for name in namelist:
    if 'dist' in name and species in name:
        thisname.append(name)
for i in np.arange(tdata.shape[0]):
    tdict[tdata[i, 0]] = tdata[i, 1]
res = np.zeros((0, 2))
num = np.zeros((0, 2))
print res.shape
thisname.sort()
print thisname
#print tdict
for na in thisname:
    for keys in tdict:
        if int(keys) < 10:
            mkey = '0000' + str(int(keys))
        elif int(keys) < 100:
            mkey = '000' + str(int(keys))
        elif int(keys) < 1000:
            mkey = '00' + str(int(keys))
        elif int(keys) < 10000:
            mkey = '0' + str(int(keys))
        if mkey in na:
            print na, str(int(keys))
            #data = np.loadtxt(na)[104:, :]
            data = np.loadtxt(na)
            print data[0, 0]
            thisres = np.sum(data[:, 0] * data[:, 1])
            res = np.concatenate((res, np.array([[tdict[keys], thisres]])), axis = 0)
            num = np.concatenate((num, np.array([[tdict[keys], np.sum(data[:, 1])]])), axis = 0)

np.savetxt(species + "ek.txt", res)
np.savetxt(species + "num.txt", num)
plt.plot(res[:, 0], res[:, 1], linewidth = 3)
plt.savefig(species + 'ek.png')
plt.close()
plt.plot(num[:, 0], num[:, 1], linewidth = 3)
plt.savefig(species + 'num.png')
plt.close()



