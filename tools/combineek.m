ekfiles = dir('*rank*ek_species*');
timef = ls('*time.txt');
prefix = regexp(timef, '_', 'split');
prefix = prefix(1);
time = load([prefix{1}, '_time.txt'], '-ascii');
len = length(ekfiles) - 1;
mdata = 0;
numdata = 0;
for i = 0:len
    fname = strcat(prefix, '_00000_rank_', num2str(i), '_ek_species.txt');
    data = load(fname{1}, '-ascii');
    mdata = mdata + data;
    fname = strcat(prefix, '_00000_rank_', num2str(i), '_num_species.txt');
    data = load(fname{1}, '-ascii');
    numdata = numdata + data;
end


mdata = [time(:, 2), mdata];
numdata = [time(:, 2), numdata];

filename = strcat(prefix, '_ek_species.txt');
save(filename{1}, 'mdata', '-ascii');
filename = strcat(prefix, '_num_species.txt');
save(filename{1}, 'numdata', '-ascii');


plot(numdata(:, 1), numdata(:, 2));
hold on
plot(numdata(:, 1), numdata(:, 3))
plot(numdata(:, 1), numdata(:, 4))
plot(numdata(:, 1), numdata(:, 5));
set(gca, 'YScale', 'log');
legend('electron', 'ion', 'positron', 'photon');