import numpy as np
import os
import glob

mprefix = os.getcwd().split('_')[-1]

eknum = ["ek_species", "num_species"]

time = np.loadtxt(mprefix + "_time.txt")
time = np.int_(time[:, 0])
ranksize = len(glob.glob('*rank*num*'))

def getprefix(num):
    if num < 10:
        return '0000' + str(num)
    elif num < 100:
        return '000' + str(num)
    elif num < 1000:
        return '00' + str(num)
    elif num < 10000:
        return '0' + str(num)
    else:
        return str(num)



def intdata(prefix, appendix, ranksize):
    data = 0
    for rank in np.arange(ranksize):
        fname = prefix + '_rank_' + str(rank) + '_' + appendix + ".txt"
        if rank == 0:
            data = np.loadtxt(fname)
        else:
            data = np.concatenate((data, np.loadtxt(fname)), axis = 1)
        print fname
    print prefix + '_' + appendix
    np.savetxt(prefix + '_' + appendix, data)

def adddata(prefix, appendix, ranksize):
    data = 0
    for rank in np.arange(ranksize):
        fname = prefix + '_rank_' + str(rank) + '_' + appendix + ".txt"
        if rank == 0:
            data = np.loadtxt(fname)
        else:
            data = data + np.loadtxt(fname)
        print fname
    print prefix + '_' + appendix
    np.savetxt(prefix + '_' + appendix, data)

def addnoone(prefix, appendix, ranksize):
    data = 0
    for rank in np.arange(ranksize):
        fname = prefix + '_rank_' + str(rank) + '_' + appendix + ".txt"
        if rank == 0:
            data = np.loadtxt(fname)
        else:
            temp = np.loadtxt(fname)
            data[:, 2:] = data[:, 2:] + temp[:, 2:]
        print fname
    print prefix + '_' + appendix
    np.savetxt(prefix + '_' + appendix, data)




for sp in species:
    mysp = sp
    for ki in kind:
        myki = ki
        appendix = myki + '_' + mysp
        for num in time:
            prefix = mprefix + '_' + getprefix(num)
            intdata(prefix, appendix, ranksize)
    for dk in distkind:
        mydk = dk
        appendix = sp + "_" + mydk
        for nu in time:
            prefix = mprefix + '_' + getprefix(nu)
            addnoone(prefix, appendix, ranksize)
    for pk in phasekind:
        mypk = pk
        appendix = sp + '_' + mypk
        for num in time:
            prefix = mprefix + '_' + getprefix(num)
            adddata(prefix, appendix, ranksize)
