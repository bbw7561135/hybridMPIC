clf
ekall = load('bubble_ekout.txt', '-ascii');
ekepoch = load('/public/home/wanfeng/data/bubble/matlabs/penergy.txt', '-ascii');
hold on
% plot(ekall(:, 1), ekall(:, 2))
plot(ekall(:, 1), ekall(:, 3), 'r');
plot(ekall(:, 1), ekall(:, 4), 'k');

% 
% plot(ekepoch(:, 1), ekepoch(:, 2), 'k--')
% plot(ekepoch(:, 1), ekepoch(:, 3), 'r--')
% 
% legend('benchmark field ek', 'benchmark all particle ek', 'epoch particle ek', 'epoch field ek');
set(gca, 'yscale', 'log');

xlim([0, 140e-15])