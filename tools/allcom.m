function allcom(rank)
species = {'Electron', 'Au', 'Al', 'Photon', 'Positron', 'H'};
pre = {'density', 'ek'};

for i = 1:length(pre)
    for j = 1:length(species)
        mids = strcat(pre{i}, '_', species{j});
        mycombine2d(mids{1}, rank);
    end
end

field = {'ex', 'ey', 'ez', 'jx', 'jy', 'jz'};
for i = 1:length(field)
    mycombine2d(field{i}, rank);
end

end
