/// 网格信息文件
#ifndef __cellinf
#define __cellinf

#include <iostream>
#include <vector>
#include <string>
#include "type.h"
#include "particles.h"
#include <cstring>
#include "physicalconstants.h"
#include "stdio.h"
#include "events.h"
#include "common.h"
#include "userdef.h"
#include "denfunc.h"
#ifdef __OMP
#include "omp.h"
#endif
#ifndef target_id
#define target_id 1
#endif
using namespace std;
///用来存储MPI传输的Ghost cell数据，只有场，没有粒子
struct __Cdata{
    /// 0 左下， 1, 左上， 2 右下， 3, 右上
    /// 0 先jz，后jx[0], jx[1], jy[0], jy[1]
    //double data_j[5];
    /// 电磁场同例
    double data_e[5];
    double data_b[5];
};

double get_density(const __Vect2<double>& position, const int& sp);
///单个cell的场信息
class __Cellfield{
public:
    /// 下标规则，总是先下后上，先左后右
    __Vect2<double> exfield;
    __Vect2<double> bxfield;
    //__Vect2<double> jxfield;
    __Vect2<double> eyfield;
    __Vect2<double> byfield;
    double ezfield;
    double bzfield;
    __Cellfield(){
        __Vect2<double> zero(0, 0);
        exfield = zero;
        eyfield = zero;
        bxfield = zero;
        byfield = zero;
        ezfield = 0;
        bzfield = 0;
    }
    __Cellfield(const __Cellfield& other){
        exfield = other.exfield;
        eyfield = other.eyfield;
        ezfield = other.ezfield;
        bxfield = other.bxfield;
        byfield = other.byfield;
        bzfield = other.bzfield;
    }
    __Cellfield(__Cdata& cdata);
    ~__Cellfield(){}
    __Cellfield operator=(const __Cellfield& other){
        this->exfield = other.exfield;
        this->eyfield = other.eyfield;
        this->ezfield = other.ezfield;
        this->bxfield = other.bxfield;
        this->byfield = other.byfield;
        this->bzfield = other.bzfield;
        return *this;
    }
    ///所有数据清空为 0
    void clear();
};

void __Cellfield::clear(){
    __Vect2<double> zero(0, 0);
    exfield = zero;
    eyfield = zero;
    bxfield = zero;
    byfield = zero;
    ezfield = 0;
    bzfield = 0;
    }

__Cellfield::__Cellfield(__Cdata& cdata)
{
    ezfield = cdata.data_e[0];
    bzfield = cdata.data_b[0];
    exfield.member[0] = cdata.data_e[1];
    exfield.member[1] = cdata.data_e[2];
    eyfield.member[0] = cdata.data_e[3];
    eyfield.member[1] = cdata.data_e[4];


    bxfield.member[0] = cdata.data_b[1];
    bxfield.member[1] = cdata.data_b[2];
    byfield.member[0] = cdata.data_b[3];
    byfield.member[1] = cdata.data_b[4];
}

/// 单个cell内部信息，包括场和粒子两部分
class __Cellinfo{
    public:
       __Cellinfo(){}
       /// 网格位置
       __Vect2<int> position;
       /// 网格左下角坐标
       __Vect2<double> xymin;
       /// 网格右下角坐标
       __Vect2<double> xymax;
       /// 网格中的粒子束
       vector<__Ptclslist> plist;
       /// 网格中的场
       __Cellfield fields;
       /// particle species total
       int species;
       /// 粒子种类启用标志
       /// 粒子种类密度
       vector<double>  density = {};
       /// area of this cell
       double area;
       /// 0:synchrotron, 1:bremsstrahlung, 2:bw_pair, 3:bh_pair
       int events_types[5];
       __Cellinfo(__Vect2<int>& position, __Vect2<double>& xymin, __Vect2<double>& xymax, \
               vector<double>& dens, const int& species):position(position), xymin(xymin),\
                                                           xymax(xymax), density(dens), species(species){}
       __Cellinfo(__Vect2<int>& position, __Vect2<double>& xymin, __Vect2<double>& xymax, \
               const int& species):position(position), xymin(xymin), xymax(xymax), species(species){
                                                                for(int i = 0; i < species; i ++)
                                                                {
                                                                    density.push_back(0);
                                                                }
       }
       /// 使用其它网格数据进行初始化
        __Cellinfo(const __Cellinfo& other){
           position = other.position;
           xymin = other.xymin;
           xymax = other.xymax;
           plist = other.plist;
           fields = other.fields;
           density = other.density;
           species = other.species;
       }
        /// 可以使用=来直接对网格赋值
       __Cellinfo& operator=(const __Cellinfo& other){
           this->position = other.position;
           this->xymin = other.xymin;
           this->xymax = other.xymax;
           for(int i = 0; i < other.plist.size(); i ++) plist[i] = other.plist[i];
           this->fields = other.fields;
           this->density = other.density;
           this->species = other.species;
           return *this;
       }

       /// 网格粒子推进
       void advance(const double& delta_t, vector<vector<__Ptcls> >& templist, const int& myrank);
       /// 网格sp种类例子，偏移offset处粒子，删除
       void pop(const int& sp, const int& offset);
       /// 将myptcl加入到当前网格中
       void push(const __Ptcls& myptcl);
       /// 压缩空间
       void squeeze();
       /// 将rho和j weight到grid上
       void weight_to_grid();
       /// weight 场到坐标，粒子推动使用
       __Vect3<double> weight_field_to_coords(const int& type, const __Vect2<double>& coords);
        // type 0--efield, type 1--bfield
        /// 设置当前网格的场值
       //void set_field(const __Cellfield& otherfield);
       /// 初始化当前网格，假定density已经给定
       void init(const vector<int>& ppc);
       /// 初始化sp类粒子，给定ppc和dens
       void init(const vector<int>& ppc, const vector<Particles>& spptcl, const int& disable_em, int rank);
       /// 设这sp类粒子的速度
       void set_velocity(const int& sp, const __Vect3<double>& velocity);
       /// 设置当前网格的粒子种类密度，dens是数组
       void set_density(const vector<double>& dens);
       /// 将场插值到粒子，推动粒子使用
       //void weight_field_to_ptcls();
       /// 同步辐射，采用光学深度算法
       void friend synchrotron(__Cellinfo& mycell, const double& delta_t, vector<vector<double> >& tables1d, \
               vector<vector<vector<double> > >& tables2d);
       /// 韧致辐射，采用MC
       void friend bremsstrahlung(__Cellinfo& mycell, const double& delta_t, \
               vector<vector<double> >& tables1d, \
               vector<vector<vector<double> > >& tables2d);
       /// 光子光子产生正负电子对Breit_wheeler过程，采用光深，以后可以修改为与背景温度相关的MC过程。
       void friend bw_pair(__Cellinfo& mycell, const double& delta_t, \
               vector<vector<double> >& tables1d, \
               vector<vector<vector<double> > >& tables2d);
       /// 光子在原子核的库仑场中产生正负电子对，采用MC
       void friend bh_pair(__Cellinfo& mycell, const double& delta_t, \
               vector<vector<double> >& tables1d, \
               vector<vector<vector<double> > >& tables2d);
       /// 粒子信息输出重载算符，可以直接输出当前粒子的信息
       friend ostream& operator<<(ostream& out, const __Cellinfo& other){
           out<<"position "<<other.position<<"\t"<<other.xymin<<"\t"<<other.xymax<<endl;
           // 注意这个地方的定义必须是引用
           for(int i = 0; i < other.species; i++){
                for(int j = 0; j < other.plist[i].member.size(); j++){
                    out<<other.plist[i].member[j]<<endl;
               }
           }
           out<<"________________________"<<endl;
           return out;
       }
       long size();
       /// 计算各种粒子的密度(本网格内部)

       ~__Cellinfo(){}
};


//void __Cellinfo::synchrotron(const double& delta_t, vector<vector<double> >& tables1d, \
               vector<vector<vector<double> > >& tables2d)
void synchrotron(__Cellinfo& mycell, const double& delta_t, vector<vector<double> >& tables1d, \
               vector<vector<vector<double> > >& tables2d)
{
#ifndef DPHOTON
	cout<<" turn on DPHOTON flag in load.inp to enable this feature"<<endl;
	return;
#endif
    /// 电子
    vector<__Ptclslist>& plist = mycell.plist;
    if(plist[0].member.size() > 0)
    {
#pragma omp parallel for shared(plist, delta_t, tables1d, tables2d)
        for(int i = plist[0].member.size() - 1; i >= 0; i --)
        {
            __Ptcls child0;
            int status = synchrotron_rad(delta_t, plist[0].member[i], \
                    child0, tables1d, tables2d);
#ifndef PHOTON_DYNAMICS
        status = 0;
#endif
#pragma omp critical(plist)
            if(status == 1)
            {
                plist[3].push(child0);
            }
            else {
                child0.total_id -= 1;
            }
        }
    }
    if(plist[2].member.size() > 0)
    {
        //cout<<" starting positron "<<endl;
        /// 正电子
#pragma omp parallel for shared(plist, delta_t, tables1d, tables2d)
        for(int i = plist[2].member.size() - 1; i >= 0; i --)
        {
            __Ptcls child0;
            int status = synchrotron_rad(delta_t, plist[2].member[i], \
                    child0, tables1d, tables2d);
#ifndef PHOTON_DYNAMICS
        status = 0;
#endif
#pragma omp critical(plist)
            if(status == 1)
            {
                plist[3].push(child0);
                //cout<<" new photon generated by posiron"<<endl;
            }
            else {
                child0.total_id -= 1;
            }
        }
    }
}
void bremsstrahlung(__Cellinfo& mycell, const double& delta_t, vector<vector<double> >& tables1d, \
               vector<vector<vector<double> > >& tables2d)
//void __Cellinfo::bremsstrahlung(const double& delta_t, \
        vector<vector<double> >& tables1d, \
        vector<vector<vector<double> > >& tables2d)
{
#ifndef DPHOTON
	cout<<" turn on DPHOTON flag in load.inp to enable this feature"<<endl;
	return;
#endif
    vector<__Ptclslist>& plist = mycell.plist;
#pragma omp parallel for shared(plist, delta_t, tables1d, tables2d)
    for(int i = plist[0].member.size() - 1; i >= 0; i --)
    {
        __Ptcls child;
        int status = bremsstrahlung_rad(delta_t, mycell.density[target_id], plist[0].member[i], \
                child, tables1d, tables2d);
#ifndef PHOTON_DYNAMICS
        status = 0;
#endif
#pragma omp critical
        if(status == 1)
        {
#if __Debug
            cout<<" photon generated "<<endl;
#endif
            plist[3].push(child);
        }
        else {
            child.total_id -= 1;
        }
    }
#pragma omp parallel for shared(plist, delta_t, tables1d, tables2d)
    for(int i = plist[2].member.size() - 1; i >= 0; i --)
    {
        __Ptcls child;
        int status = bremsstrahlung_rad(delta_t, mycell.density[target_id], plist[2].member[i], \
                child, tables1d, tables2d);
#ifndef PHOTON_DYNAMICS
        status = 0;
#endif
#pragma omp critical
        if(status == 1)
        {
            plist[3].push(child);
        }
        else {
            child.total_id -= 1;
        }
    }
}


void bw_pair(__Cellinfo& mycell, const double& delta_t, vector<vector<double> >& tables1d, \
               vector<vector<vector<double> > >& tables2d)

//void __Cellinfo::bw_pair(const double& delta_t, vector<vector<double> >& tables1d, \
               vector<vector<vector<double> > >& tables2d)
{
#ifndef DPHOTON
	cout<<" turn on DPHOTON flag in load.inp to enable this feature"<<endl;
	return;
#endif
    //cout<<" in bw pair "<<endl;
    ///　因为会删除粒子因此，必须倒序才能避免跳过粒子，或者对空list操作
    vector<__Ptclslist>& plist = mycell.plist;
    if(plist[3].member.size() > 0)
    {
#pragma omp parallel for shared(plist, delta_t, tables1d, tables2d)
        for(int i = plist[3].member.size() - 1; i >= 0; i --)
        {
            __Ptcls child0, child1;
            int status = bw_pair_create(delta_t, plist[3].member[i], \
                    child0, child1, tables1d, tables2d);
#pragma omp critical(plist)
            if(status == 2)
            {
                ///产生粒子后这个list 会减小。
                plist[0].push(child0);
                plist[2].push(child1);
                plist[3].pop(i);
            }
            else {
                child0.total_id -= 2;
            }
        }
    }
}

void bh_pair(__Cellinfo& mycell, const double& delta_t, vector<vector<double> >& tables1d, \
               vector<vector<vector<double> > >& tables2d)
//void __Cellinfo::bh_pair(const double& delta_t, \
        vector<vector<double> >& tables1d, \
        vector<vector<vector<double> > >& tables2d)
{
#ifndef DPHOTON
	cout<<" turn on DPHOTON flag in load.inp to enable this feature"<<endl;
	return;
#endif
    vector<__Ptclslist>& plist = mycell.plist;
    if(plist[3].member.size() > 0)
    {
#pragma omp parallel for shared(plist, delta_t, tables1d, tables2d)
        for(int i = plist[3].member.size() - 1; i >= 0; i --)
        {
            __Ptcls child0, child1;
            int status = bh_pair_create(delta_t, mycell.density[target_id], plist[3].member[i], \
                    child0, child1, tables1d, tables2d);
#pragma omp critical
            if(status == 2)
            {
                plist[0].push(child0);
                plist[2].push(child1);
                plist[3].pop(i);
            }
            else {
                child0.total_id -= 2;
            }
        }
    }
}




void __Cellinfo::set_density(const vector<double>& dens)
{
    density = dens;
}


/// 一阶插值函数，这里采用面积插值, mycoords处的场插值到other，或者相反（结果是一样的），给定cell_length
double weightfunc(const __Vect2<double>& mycoords, const __Vect2<double>& other, \
        const __Vect2<double>& cell_length){
    __Vect2<double> temp(0, 0);
    temp = (mycoords - other).Abs();
    //cout<<"temp "<<temp<<endl;
       if(cell_length > temp){
           return (cell_length.member[0] - temp.member[0]) * \
               (cell_length.member[1] - temp.member[1])\
               / (cell_length.member[0] * cell_length.member[1]);
       }
    else {
        return 0;
    }
}


/// 二阶插值,没有使用，需要矫正

double weightfunc1d(const __Vect2<double>& pcoords, const __Vect2<double> ccoords, \
        const __Vect2<double>& cell_length)
{
    __Vect2<double> temp = pcoords - ccoords;
    double x, y;
    if(abs(temp.member[0]) < 0.5 * cell_length.member[0])
    {
        x = 0.75 - temp.member[0] * temp.member[0];
    }
    else if(abs(temp.member[0]) < 1.0 * cell_length.member[0])
    {
        x =  0.5 * (0.5 + abs(temp.member[0])) * (0.5 + abs(temp.member[0]));
    }
    else{
        return 0;
    }
    if(abs(temp.member[1]) < 0.5 * cell_length.member[1])
    {
        y = 0.75 - temp.member[1] * temp.member[1];
    }
    else if(abs(temp.member[1]) < 1.0 * cell_length.member[1])
    {
        y =  0.5 * (0.5 + abs(temp.member[1])) * (0.5 + abs(temp.member[1]));
    }
    else{
        return 0;
    }
    return x * y;
}





__Vect3<double> __Cellinfo::weight_field_to_coords(const int& type, const __Vect2<double>& coords){
    //cout<<" start type "<<type<<endl;
    __Vect3<double> retfields(0, 0, 0);
    __Vect2<double> dxy = xymax - xymin;
    __Vect2<double> ucenter(xymin.member[0] + dxy.member[0] / 2.0, xymax.member[1]);
    __Vect2<double> dcenter(xymin.member[0] + dxy.member[0] / 2.0, xymin.member[1]);
    __Vect2<double> lcenter(xymin.member[0], xymin.member[1] + dxy.member[1] / 2.0);
    __Vect2<double> rcenter(xymax.member[0], xymin.member[1] + dxy.member[1] / 2.0);
    __Vect2<double> center = xymin + dxy / 2.0;
    double wuc, wdc, wlc, wrc, wz;
    //cout<<" start weight func "<<endl;
    //cout<<" ucenter " <<ucenter<<endl<<" coords "<<coords<<endl<<" dxy "<<dxy<<endl;
    wuc = weightfunc(ucenter, coords, dxy);
    //cout<<" wuc over "<<endl;
    wdc = weightfunc(dcenter, coords, dxy);
    wlc = weightfunc(lcenter, coords, dxy);
    wrc = weightfunc(rcenter, coords, dxy);
    wz = weightfunc(center, coords, dxy);
    //cout<<" weight func finish "<<endl;
    switch(type)
    {
        // x: u/d center up, down
        // y: l/r center left, right
        // z: center

        case 0:
            retfields.member[0] += fields.exfield.member[0] * wdc + fields.exfield.member[1] * \
                                   wuc;
            retfields.member[1] += fields.eyfield.member[0] * wlc + fields.eyfield.member[1] * \
                                   wrc;
            retfields.member[2] += fields.ezfield * wz;
            //cout<<" e finish "<<endl;
            break;
        case 1:
            retfields.member[0] += fields.bxfield.member[0] * wdc + fields.bxfield.member[1] * \
                                   wuc;
            retfields.member[1] += fields.byfield.member[0] * wlc + fields.byfield.member[1] * \
                                   wrc;
            retfields.member[2] += fields.bzfield * wz;
            //cout<<" b finish "<<endl;
            break;
        default:
            break;
    }
    //cout<<"retfields is "<<retfields<<endl;

    return retfields;
}

void __Cellinfo::init(const vector<int>& ppc, const vector<Particles>& spptcl, const int& disable_em, int rank){
    //cout<<"with xymin "<<xymin<<endl;
    __Vect2<double> zero2(0, 0);
    __Vect3<double> zero3(0, 0, 0);
    __Vect2<double> xydelta = xymax - xymin;
    __Vect2<double> xycenter = xymin + xydelta * 0.5;
    area = xydelta.member[1] * xydelta.member[0];
    for(int i = 0; i < species; i++){
        __Ptclslist mylist(xydelta, i);
        if(ppc[i] > 0 && abs(density[i]) > 1e-30 && !disable_em){
            __Vect2<double> pos(0, 0);
            double weight = double(density[i] * area / ppc[i]);
            for(int j = 0; j < ppc[i]; j ++)
            {
                __Ptcls myptcl(spptcl[i]);
                myptcl.weight = weight;
                //cout<<weight<<endl;
                pos = xymin + xydelta / ppc[i] * (1.0 * j + 0.5);
                myptcl.position = pos;
                myptcl.bfield = zero3;
                myptcl.efield = zero3;
                mylist.push(myptcl);
            }
        }
#if DPHOTON
        if(species == 0 || species == 2 || species == 3)
        {
            for(int pi = 0; pi < mylist.member.size(); pi ++)
            {

                mylist.member[pi].rod_factor = - log(1.0 - random_generator());
            }
        }
#endif
        plist.push_back(mylist);
    }
}

void __Cellinfo::set_velocity(const int& sp, const __Vect3<double>& velocity){
    for(int i = 0; i < plist[sp].size(); i ++){
        plist[sp].member[i].velocity = velocity;
    }
}



void __Cellinfo::squeeze(){
    for(int i = 0; i < plist.size(); i ++)
    {
#if check_squeeze
	    int before = plist[i].member.capacity();
#endif
	    plist[i].squeeze();
#if check_squeeze
	    before -= plist[i].member.capacity();
	    if(before > 0) cout<<" squeezed "<<before<<" now "<<plist[i].member.capacity()<<endl;
#endif
    }
}


void __Cellinfo::push(const __Ptcls& myptcl)
{
    //cout<<"pushing into cell "<<position<<endl;
    for(int i = 0; i < plist.size(); i++)
    {
        //cout<<"trying sp "<<i<<endl;
        //cout<<"sp "<<myptcl.species<<"\t"<<plist[i].species<<endl;
        if(plist[i].species == myptcl.species){
            //cout<<"species is "<<i<<endl;
            //cout<<"----------------------------"<<endl;
            plist[i].push(myptcl);
            //cout<<"plist i size now "<<plist[i].member.size()<<endl;
        }
    }
}

void __Cellinfo::pop(const int& sp, const int& offset){
    plist[sp].pop(offset);
}



void __Cellinfo::advance(const double& delta_t, vector<vector<__Ptcls> >& templist, const int& myrank)
{

    for(int i = 0; i < plist.size(); i ++)
    {
        //cout<<"prepare to advance ps "<<i<<"\t"<<plist[i].size()<<endl;
        plist[i].advance(delta_t, templist[i], xymin, xymax, myrank);
    }

}

/****
void __Cellinfo::set_field(const __Cellfield& otherfield){
    fields = otherfield;
}
***/





///网格二维数组群，来表征一个box内的cell信息，cell是其内部的member
class __Cellgroup{
public:
    /// 当前group的gridmin
    __Vect2<int> xygridmin;
    /// 当前group的gridmax
    __Vect2<int> xygridmax;
    /// maxid current for ptcls
    int maxid;
    /// 当前group的xymin,左下角坐标
    __Vect2<double> xymin;
    /// 当前group的xymax，右上角坐标
    __Vect2<double> xymax;
    /// 当前group的cell num，在x 和 y 方向
    __Vect2<int> cellnum;
    /// 当前group的cell宽度，dx 和 dy
    __Vect2<double> dxy;
    /// species 是否启用
    vector<Particles> spptcl;
    /// species total
    int species;
    /// species 的 particle per cell
    vector<int> ppc;
    /// 是否关闭em
    int disable_em = 0;
    /// bd_type
    int bd_type[4];
    /// dt
    double delta_t;
    ///这个位置用来记录在MPI中的位置
    __Vect2<int> position;
    ///能谱最小值，最大值，和间隔
    double energy_start_end_and_dk[3];
    ///输出能谱的种类
    vector<int> dist_species;
    /// cellinfo 数据
    vector<vector<__Cellinfo> > member;
    vector<vector<__Vect3<double> > > jfield;
    vector<vector<double> >rhofield;
    vector<vector<vector<double> > >density_grid;
    vector<vector<vector<double> > >energy_grid;
    vector<vector<vector<double> > >weight_grid;
    __Cellgroup(){}
    __Cellgroup(const __Vect2<int>& xygridmin, const __Vect2<int>& xygridmax, \
            const __Vect2<double>& xymin, const __Vect2<double>& xymax, __Vect2<int>&\
            cellnum, const int& species, const vector<int>& ppc, const vector<Particles>& spptcl, \
            const double& delta_t, const __Vect2<int> position, const int& maxid):xygridmin(xygridmin), \
                                                                xygridmax(xygridmax), \
                                                                xymin(xymin), \
                                                                xymax(xymax), \
                                                                cellnum(cellnum), \
                                                                delta_t(delta_t), \
                                                                position(position), \
                                                                maxid(maxid), species(species), ppc(ppc){
                                                                this->spptcl = spptcl;
                                                                }

    __Cellgroup(const __Cellgroup& other){
        xygridmin = other.xygridmin;
        xygridmax = other.xygridmax;
        xymin = other.xymin;
        xymax = other.xymax;
        cellnum = other.cellnum;
        delta_t = other.delta_t;
        position = other.position;
        species = other.species;
        ppc = other.ppc;
        spptcl = other.spptcl;
        member = other.member;
        dxy = other.dxy;
        rhofield = other.rhofield;
        member = other.member;
        jfield = other.jfield;
        density_grid = other.density_grid;
        energy_grid = other.energy_grid;
        weight_grid = other.weight_grid;
        cout<<"copying construction "<<endl;

    }
//    __Cellgroup(){}
    ~__Cellgroup(){}

    /// 初始化，创建cell，没有粒子
    void init(int rank);
    /// 压缩当前box的空间
    void squeeze();
    /// 将粒子密度和电流密度插值到网格
    void weight_to_grid(const int& myrank, const int& step);
    void weight_to_gridb(const int& myrank);
    void weight_to_gridc();
    /// 将场数据插值到粒子
    //void weight_to_ptcls();
    /// 设置网格位置position处sp类粒子的速度
    void set_velocity(const int& sp, const __Vect2<int>& position, const __Vect3<double>& velocity);
    /// 设置当前box内sp类粒子的速度
    void set_sp_velocity(const int&sp, const __Vect3<double>& velocity);
    /// 初始化当前box内的粒子
    friend void init_ptcls(__Cellgroup& mygroup, int rank);
    /// 推动当前cellgroup内粒子运动
    //void advance(const double& delta_t, vector<vector<__Ptcls> >& templist, const int& myrank, const int& n);
    friend void advance(__Cellgroup& mygroup, const double& delta_t, vector<vector<__Ptcls> >& templist, const int& myrank, const int& n);
    /// 输出当前进程rank，当前步n，文件名前缀prefix，数据类型sp到文件
    void dump(const int& rank, string prefix, const int& sp);
    void dump_ptcls(const int& rank, string prefix, const int& sp);
    /// 使用这个函数来产生事件, 其实只是设置了events_type变量，过程在cell中进行, 0: synchrotron radiation 1: bremsstrahlung radiation, 2: Breit_Wheeler pair, 3: Bethe-Hetiler pair
    friend void monte_carlo(__Cellfield& mygroup, const int& type, const double& delta_t, \
            vector<vector<double> >& tables1d, \
            vector<vector<vector<double> > >& tables2d);
    /// 将rho, j field 清０
    void clear_rhoj();
    /// 合并粒子
    void merge_ptcls(const int& max_ppc, const double& delta_e, const double& delta_v, const int& sp);
    long size(const int& sp);
    void set_bd(int bd_types[])
    {
        memcpy(bd_type, bd_types, 4 * sizeof(int));
    }
    void mpi_copy_to(__Grid& cdatal, __Grid& cdatar, int direction, const int& myrank);
    void copy_to_mpidata(__Grid& cdatal, __Grid& cdatar, int direction, const int& myrank);
    void dist_en(double energy_start_end_and_dk[], int& dist_species, int& dist_size, double results[]);
    void dump_disten(const int& rank, const int& n, string prefix);
    __Cellgroup& operator = (const __Cellgroup& other){
        xygridmin = other.xygridmin;
        xygridmax = other.xygridmax;
        xymin = other.xymin;
        xymax = other.xymax;
        cellnum = other.cellnum;
        delta_t = other.delta_t;
        position = other.position;
        species = other.species;
        ppc = other.ppc;
        spptcl = other.spptcl;
        member = other.member;
        rhofield = other.rhofield;
#if __Debug
        cout<<" copying cellgroup "<<rhofield.size()<<"\t"<<other.rhofield.size()<<endl;
#endif
        dxy = other.dxy;
    }
};

long __Cellgroup::size(const int& sp)
{
    long sizes;
    for(int i = 0; i < member.size(); i ++)
    {
        for(int j = 0; j < member[i].size(); j ++)
        {
            sizes += member[i][j].plist[sp].member.size();
        }
    }
    return sizes;
}





void monte_carlo(__Cellgroup& mygroup, const int& type, const double& delta_t, \
        vector<vector<double> >& tables1d, \
        vector<vector<vector<double> > >& tables2d)
{
    //cout<<"size "<<member[50][30].plist[0].member.size()<<endl;
    switch(type)
    {
        case 0:
            {
                for(int i = _start; i < mygroup.member.size() - _end; i ++)
                {
                    for(int j = _start; j < mygroup.member[0].size() - _end; j ++)
                    {
                        //cout<<" monte "<<i<<"\t"<<j<<endl;
                        synchrotron(mygroup.member[i][j], delta_t, tables1d, tables2d);
                        //mygroup.member[i][j].synchrotron(delta_t, tables1d, tables2d);
                    }
                }
            }
            break;
        case 1:
            {
                for(int i = _start; i < mygroup.member.size() - _end; i ++)
                {
                    for(int j = _start; j < mygroup.member[0].size() - _end; j ++)
                    {
                        bremsstrahlung(mygroup.member[i][j], delta_t, tables1d, tables2d);
                        //mygroup.member[i][j].bremsstrahlung(delta_t, tables1d, tables2d);
                    }
                }
            }
            break;
        case 2:
            {
                for(int i = _start; i < mygroup.member.size() - _end; i ++)
                {
                    for(int j = _start; j < mygroup.member[0].size() - _end; j ++)
                    {
                        bw_pair(mygroup.member[i][j], delta_t, tables1d, tables2d);
                        //mygroup.member[i][j].bw_pair(delta_t, tables1d, tables2d);
                    }
                }
            }
            break;
        case 3:
            {
                for(int i = _start; i < mygroup.member.size() - _end; i ++)
                {
                    for(int j = _start; j < mygroup.member[0].size() - _end; j ++)
                    {
                        bh_pair(mygroup.member[i][j], delta_t, tables1d, tables2d);
                        //mygroup.member[i][j].bh_pair(delta_t, tables1d, tables2d);
                    }
                }
            }
            break;
        default:
            break;
    }
}


void __Cellgroup::clear_rhoj()
{
    __Vect3<double> zero(0.0, 0.0, 0.0);
    for(int i = 0; i < jfield.size(); i ++)
    {
        for(int j = 0; j < jfield[i].size(); j ++)
        {
            jfield[i][j] = zero;
            rhofield[i][j] = 0.0;
        }
    }
    for(int i = 0; i < species; i ++)
    {
        for(int j = 0; j < density_grid[i].size(); j ++)
        {
            for(int k = 0; k < density_grid[i][j].size(); k ++)
            {
                density_grid[i][j][k] = 0.0;
                energy_grid[i][j][k] = 0.0;
                weight_grid[i][j][k] = 0.0;
            }
        }
    }
}


void __Cellgroup::dump_ptcls(const int& rank, string prefix, const int& sp)
{
    if(sp >= species)
    {
        cout<<"dump species error: max sp "<<ppc.size()<<endl;
        return;
    }
    string filename;
    if(sp < species)
    {
        filename = prefix + "_" + spptcl[sp].name + ".txt";
        ofstream myout;
        myout.open(filename.c_str());
        for(int i = 0; i < member.size(); i ++)
        {
            for(int j = 0; j < member[0].size(); j ++)
            {
                __Ptclslist &mylist = member[i][j].plist[sp];
                for(int k = 0; k < mylist.size(); k ++)
                {
                    __Ptcls& myptcl = mylist.member[k];
                    myout<<rank<<"\t"<<i<<"\t"<<myptcl<<endl;
                }
            }
        }
    }

}



void __Cellgroup::squeeze(){
    for(int i = 0; i < member.size(); i++){
        for(int j = 0; j < member[i].size(); j++){
            member[i][j].squeeze();
        }
    }
}


void __Cellgroup::weight_to_gridc(){
    // 先清空cell内场数据, 只需要清空j和rho就可以了
    __Vect2<double> zero2(0, 0);
    __Vect2<double> xhalf(0.5 * dxy.member[0], 0.0);
    __Vect2<double> yhalf(0.0, 0.5 * dxy.member[1]);
    clear_rhoj();
    double area = dxy.member[0] * dxy.member[1];
    double frac = _frac2; //_frac2;
    double idtx = dxy.member[0] / delta_t, idty = dxy.member[1] / delta_t;
    double idx = 1.0 / dxy.member[0], idy = 1.0 / dxy.member[1];
    /// 保存临时单个粒子的贡献
    double wx[3], wy[3], hx[3], hy[3];
    for(int i = _start; i < member.size() - _end; i ++){
        //cout<<i<<endl;
        for(int j = _start; j < member[i].size() - _end; j ++){
            //cout<<"wt "<<i<<"\t"<<j<<endl;
            __Vect2<double>& cellmin = member[i][j].xymin;
            for(int k = 0; k < member[i][j].plist.size(); k ++){
                //cout<<"sp "<<k<<endl;
                __Ptclslist& mylist = member[i][j].plist[k];
                for(int l = 0; l < mylist.member.size(); l ++)
                {
                    /// 对相邻的三个cell中的场进行插值到粒子
                    /// 这里采用相对坐标local
                    __Ptcls& myptcl = mylist.member[l];
                    /// 两个是坐标是粒子最近格点，另一个是第一次的格点

                    __Vect2<double> nex_pos = myptcl.position + __Vect2<double>(myptcl.velocity.member[0], \
                            myptcl.velocity.member[1]) * delta_t;
                    double cell_xr = (nex_pos.member[0] - xymin.member[0]) * idx, \
                                     cell_yr = (nex_pos.member[1] - xymin.member[1]) * idy;
                    double wx[3], wy[3], hx[3], hy[3];

                    /// 两个是坐标是粒子最近格点，另一个是半格点
                    int indx1 = floor(cell_xr + 0.5), indy1 = floor(cell_yr + 0.5);
                    int indx2 = floor(cell_xr), indy2 = floor(cell_yr);

                    double fracx = indx1 - cell_xr, fracy = indy1 - cell_yr;
                    //最近格点的权重
                    get_wxy(fracx, fracy, wx, wy);

                    fracx = indx2 - cell_xr + 0.5, fracy = indy2 - cell_yr + 0.5;
                    //最近半格点的权重
                    get_wxy(fracx, fracy, hx, hy);

                    double cw = myptcl.charge * myptcl.weight / area, wtarea = myptcl.weight / area;
                    double jx = cw * myptcl.velocity.member[0] * frac, jy = cw * myptcl.velocity.member[1] * frac, \
                                 jz = cw * myptcl.velocity.member[2] * frac;
                    //cout<<"wtx1 "<<wtx0[2]<<"\t"<<wtx0[1]<<"\t"<<wtx0[3]<<endl;
                    /// 相邻的3个个点，xy方向都可能有影响
                    int cxmin = _xmin, cxmax = _xmax, cymin = _ymin, cymax = _ymax;
                    for(int s = cxmin; s <= cxmax; s ++)
                    {
                        for(int t = cymin; t <= cymax; t++)
                        {
                            //cout<<indx1<<"\t"<<indy1<<"\t"<<indx2<<"\t"<<indy2<<"\t"<<s<<"\t"<<t<<endl;
                            jfield[indx2 + s][indy1 + t].member[0] += jx * hx[s + 1] * wy[t + 1];
                            jfield[indx1 + s][indy2 + t].member[1] += jy * wx[s + 1] * hy[t + 1];
                            jfield[indx1 + s][indy1 + t].member[2] += jz * wx[s + 1] * wy[t + 1];
                            rhofield[indx1 + s][indy1 + t] += wtarea * myptcl.charge * wx[s + 1] * wy[t + 1];
                            density_grid[k][indx1 + s][indy1 + t] += wtarea * wx[s + 1] * wy[t + 1];
                            energy_grid[k][indx1 + s][indy1 + t] += wtarea * myptcl.energy * \
                                                                          wx[s + 1] * wy[t + 1];
                            weight_grid[k][indx1 + s][indy1 + t] += myptcl.weight * \
                                        wx[s + 1] * wy[t + 1] * frac;
                        }
                    }
                }
            }
        }
    }
    // 先使用PIC 1st order，这样简单，每个粒子仅仅插值到当前网格的格点上
    // 开始weighting
}
// 先使用PIC 1st order，这样简单，每个粒子仅仅插值到当前网格的格点上
// 开始weighting

void advance(__Cellgroup& mygroup, const double& delta_t, vector<vector<__Ptcls> >& templist, const int& myrank, const int& n)
//void __Cellgroup::advance(const double& delta_t, vector<vector<__Ptcls> >& templist, const int& myrank, const int& n)
{
    // 计算去掉guardcell
	int maxx = mygroup.member.size(), maxy = mygroup.member[0].size();
    if(DENSITY_CHANGE)
    {
        for(int i = _start; i < mygroup.member.size() - _end; i ++){
            for(int j = _start; j < mygroup.member[i].size() - _end; j++){
        //for(int i = 0; i < mygroup.member.size(); i ++){
            //for(int j = 0; j < mygroup.member[i].size(); j++){
                for(int k = 0; k < mygroup.species; k ++)
                {
                    //cout<<i<<"\t"<<j<<"\t"<<k<<endl;
                    if(i != maxx -1)
                    {
                        if(j != maxy - 1)
                        {
                                        mygroup.member[i][j].density[k] = (mygroup.density_grid[k][i][j] \
                                            + mygroup.density_grid[k][i+1][j] \
                                            + mygroup.density_grid[k][i][j+1] \
                                            + mygroup.density_grid[k][i+1][j+1]) * 0.25;
                        }
                        else{
                                        mygroup.member[i][j].density[k] = (mygroup.density_grid[k][i][j] \
                                            + mygroup.density_grid[k][i+1][j]) * 0.25;
                        }
                    }
                    else{
                        if (j == maxy - 1)
                        {
                                        mygroup.member[i][j].density[k] = (mygroup.density_grid[k][i][j]) * 0.25;
                        }else
                        {
                                        mygroup.member[i][j].density[k] = (mygroup.density_grid[k][i][j] \
                                            + mygroup.density_grid[k][i][j+1]) * 0.25;
                        }
                    }
                }
            }
        }
    }
#pragma omp parallel for shared(mygroup, templist)
   for(int i = _start; i < mygroup.member.size() - _end; i ++){
   //for(int i = 0; i < mygroup.member.size(); i ++){
        for(int j = _start; j < mygroup.member[0].size() - _end; j++){
        //for(int j = 0; j < mygroup.member[0].size(); j++){
#pragma omp critical(templist)
            mygroup.member[i][j].advance(delta_t, templist, myrank);
        }
    }
}

void __Cellgroup::set_sp_velocity(const int& sp, const __Vect3<double>& velocity){
    for(int i = _start; i < member.size() - _end; i ++){
        for(int j = _start; j < member[i].size() - _end; j ++){
            member[i][j].set_velocity(sp, velocity);
        }
    }
}


void __Cellgroup::set_velocity(const int& sp, const __Vect2<int>& position, \
        const __Vect3<double>& velocity){
    member[position.member[0]][position.member[1]].set_velocity(sp, velocity);
}

void __Cellgroup::init(int rank){
    // 非均匀网格初始化，怎么初始化？？？
    // 就先采用均匀的吧
    // 计算的时候不要包含这些区域就可以了
    __Vect2<double> zeros(0.0, 0.0);
    __Vect2<int> cposition(0, 0);
    __Vect2<double> dxy = xymax - xymin;
    dxy.member[0] = dxy.member[0] / cellnum.member[0];
    dxy.member[1] = dxy.member[1] / cellnum.member[1];
    for(int i = 0; i < cellnum.member[0]; i ++){
        double xbase = xymin.member[0] + dxy.member[0] * double(i);
        // 同一个i共享同一个x
        vector<__Cellinfo> itemp;
        // itemp存储当前i行的初始化cell
        for(int j = 0; j < cellnum.member[1]; j ++){
            double ybase = xymin.member[1] + dxy.member[1] * double(j);
            // xy base 是当前网格的左下角坐标
            // 下面c开头代表cell
            __Vect2<double> cxymin(xbase, ybase);
            __Vect2<double> cxymax(xbase + dxy.member[0], ybase + dxy.member[1]);
            // cposition 是这个group中的相对位置，MPI时使用更加方便
            cposition.member[0] = i;
            cposition.member[1] = j;
            __Cellinfo temp(cposition, cxymin, cxymax, species);
#ifdef __USRDENS
            calc_density(cxymin, temp.density);
#endif
            /***
            if(i < _start || j < _start || i >= cellnum.member[0] - _end || \
                    j >= cellnum.member[1] - _end) {
                for(int tt = 0; tt < temp.density.size(); tt ++){
                    temp.density[tt] = 0;
                }
            }
            ***/
            // 只是形成了cell没有初始化
            itemp.push_back(temp);
        }
        // i行完毕push到group中
        member.push_back(itemp);
        // 清空temp
        clear_vec(itemp);
        //itemp.clear();
        // 其实这步清空我怀疑是不是多余的？
    }
    // 初始化内部电荷与密度场
    __Vect3<double> v3zero(0.0, 0.0, 0.0);
    vector<__Vect3<double>> tempfield;
    vector<double> temprho;
    for(int j = 0; j < cellnum.member[1] + adder; j ++)
    {
        tempfield.push_back(v3zero);
        temprho.push_back(0.0);

    }
    for(int i = 0; i < cellnum.member[0] + adder; i ++)
    {
        jfield.push_back(tempfield);
        rhofield.push_back(temprho);
    }
    for(int i = 0; i < species; i ++)
    {
        density_grid.push_back(rhofield);
        energy_grid.push_back(rhofield);
        weight_grid.push_back(rhofield);
    }
    clear_vec(tempfield);
    clear_vec(temprho);
}

void init_ptcls(__Cellgroup& mygroup, int rank)
{
    // cell建立完毕，初始化内部粒子
#pragma omp parallel for shared(mygroup)
    for(int i = 0; i < mygroup.member.size(); i ++){
        // 不能回避掉0和max，guardcell
        // 正常初始化，最后不计算就可以了，不然会出错。。。。
        //cout<<" ptcls initing "<<i<<endl;
        for(int j = 0; j < mygroup.member[0].size(); j ++){
            // 采用另一个初始化方法，以粒子种类开始
            mygroup.member[i][j].init(mygroup.ppc, mygroup.spptcl, mygroup.disable_em, rank);
        }
    }
}







void __Cellgroup::merge_ptcls(const int& max_ppc, const double& delta_e, const double& delta_v, const int& sp)
{
    for(int i = 0; i < member.size(); i ++)
    {
        for(int j = 0; j < member[i].size(); j ++)
        {
            if(member[i][j].plist[sp].member.size() >= max_ppc)
            {
                member[i][j].plist[sp].merge(delta_e, delta_v, max_ppc);
            }
        }
    }
}
/// 拷贝cellinfo数据到cdata中，MPI传输
void copy_to_cdata(__Cellinfo& cellinfo, __Cdata& cdata)
{
    cdata.data_e[0] = cellinfo.fields.ezfield;
    cdata.data_e[1] = cellinfo.fields.exfield.member[0];
    cdata.data_e[2] = cellinfo.fields.exfield.member[1];
    cdata.data_e[3] = cellinfo.fields.eyfield.member[0];
    cdata.data_e[4] = cellinfo.fields.eyfield.member[1];
    cdata.data_b[0] = cellinfo.fields.bzfield;
    cdata.data_b[1] = cellinfo.fields.bxfield.member[0];
    cdata.data_b[2] = cellinfo.fields.bxfield.member[1];
    cdata.data_b[3] = cellinfo.fields.byfield.member[0];
    cdata.data_b[4] = cellinfo.fields.byfield.member[1];
}


void __Cellgroup::copy_to_mpidata(__Grid& cdatal, __Grid& cdatar, int direction, const int& myrank)
{
#if __Debug
    cout<<" "<<myrank<<" copy to mpi "<<endl;
#endif
    int xsize = density_grid[0].size(), ysize = density_grid[0][0].size();
    __Cgrid temp;
    if(direction == 0)
    {
        for(int i = 0; i < _jstart; i ++)
        {
            for(int j = 0; j < ysize; j ++)
            {
                for(int k = 0; k < species; k ++)
                {
                    temp.data[0] = density_grid[k][i][j];
                    temp.data[1] = energy_grid[k][i][j];
                    temp.data[2] = weight_grid[k][i][j];
                    cdatal.push_back(temp);


                    temp.data[0] = density_grid[k][xsize - _jstart + i][j];
                    temp.data[1] = energy_grid[k][xsize - _jstart + i][j];
                    temp.data[2] = weight_grid[k][xsize - _jstart + i][j];
                    cdatar.push_back(temp);
                }
            }
        }
    }
    if(direction == 1)
    {}
#if __Debug
    cout<<" "<<myrank<<" over copy to mpi "<<endl;
#endif
}


void __Cellgroup::mpi_copy_to(__Grid& cdatal, __Grid& cdatar, int direction, const int& myrank)
{
#if __Debug
    cout<<" "<<myrank<<" mpi copy to "<<endl;
#endif
    int xsize = density_grid[0].size(), ysize = density_grid[0][0].size();
    if(direction == 0)
    {
        for(int i = 0; i < _jstart; i ++)
        {
            for(int j = 0; j < ysize; j ++)
            {
                for(int k = 0; k < species; k ++)
                {
                    long index = i * ysize * species + j * species + k;
                    if(myrank != 0)
                    {
                        density_grid[k][i + _jstart - 1][j] += cdatal[index].data[0];
                        energy_grid[k][i + _jstart - 1][j] += cdatal[index].data[1];
                        weight_grid[k][i + _jstart - 1][j] += cdatal[index].data[2];
                    }

                    if(myrank != -1)
                    {
                        density_grid[k][xsize + i - 2 * _jstart + 1][j] += cdatar[index].data[0];
                        energy_grid[k][xsize + i - 2 * _jstart + 1][j] += cdatar[index].data[1];
                        weight_grid[k][xsize + i - 2 * _jstart + 1][j] += cdatar[index].data[2];
                    }
                }
            }
        }
    }
    if(direction == 1)
    {}
#if __Debug
    cout<<" "<<myrank<<" over mpi copy to "<<endl;
#endif
}





#endif
