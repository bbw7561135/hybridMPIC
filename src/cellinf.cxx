#include "cellinf.h"

void __Cellgroup::weight_to_grid(const int& myrank, const int& step){
    // 先清空cell内场数据, 只需要清空j和rho就可以了
    __Vect2<double> zero2(0, 0);
    __Vect2<double> xhalf(0.5 * dxy.member[0], 0.0);
    __Vect2<double> yhalf(0.0, 0.5 * dxy.member[1]);
    clear_rhoj();
    int xsize = jfield.size(), ysize = jfield[0].size();
    double area = dxy.member[0] * dxy.member[1];
    double frac = _frac2;
    double idtx = 1.0 / dxy.member[0] / delta_t, idty = 1.0 / dxy.member[1] / delta_t, \
                  idxy = 1.0 / dxy.member[0] / dxy.member[1], idt = 1.0 / delta_t, \
                  idx = 1.0 / dxy.member[0], idy = 1.0 / dxy.member[1];
    /// 保存临时单个粒子的贡献
    for(int i = _start; i < member.size() - _end; i ++){
        for(int j = _start; j < member[0].size() - _end; j ++){
            for(int k = 0; k < species; k ++){
                __Ptclslist& mylist = member[i][j].plist[k];
                if(mylist.size() <= 0)
                {
                    continue;
                }
                for(int l = 0; l < mylist.member.size(); l ++)
                {
                    /// 对相邻的三个cell中的场进行插值到粒子
                    /// 这里采用相对坐标local
                    __Ptcls& myptcl = mylist.member[l];
                    __Vect2<double> pre_pos = myptcl.position - __Vect2<double>(myptcl.velocity.member[0], \
                            myptcl.velocity.member[1]) * 0.5 * delta_t;
                    __Vect2<double> nex_pos = myptcl.position + __Vect2<double>(myptcl.velocity.member[0], \
                            myptcl.velocity.member[1]) * 0.5 * delta_t;
                    __Vect3<double> nqv = idxy * myptcl.charge * myptcl.weight * myptcl.velocity;
                    double cell_xr = (nex_pos.member[0] - xymin.member[0]) * idx, \
                            cell_yr = (nex_pos.member[1] - xymin.member[1]) * idy;
                    double cell_oxr = (pre_pos.member[0] - xymin.member[0]) * idx, \
                            cell_oyr = (pre_pos.member[1] - xymin.member[1]) * idy;
                    /// 两个是坐标是粒子最近格点，另一个是第一次的格点
                    int indx_new = floor(cell_xr + 0.5), indy_new = floor(cell_yr + 0.5);
                    int indx_old = floor(cell_oxr + 0.5), indy_old = floor(cell_oyr + 0.5);

                    double wtx0[5] = {}, wty0[5] = {}, wtx1[5] = {}, wty1[5] = {};
                    double tempjx[5][5] = {}, tempjy[5][6] = {}, tempjz[5][5] = {};
                    double fracx = indx_old - cell_oxr, fracy = indy_old - cell_oyr;
                    grid_wxy(fracx, fracy, wtx0, wty0);
                    //indx_old += 1; indy_old += 1;

                    fracx = indx_new - cell_xr; fracy = indy_new - cell_yr;
                    //indx_new += 1; indy_new += 1;
                    int dcellx = indx_new - indx_old, dcelly = indy_new - indy_old;
                    grid_dwxy(fracx, fracy, wtx1, wty1, dcellx, dcelly);
                    double cw = myptcl.charge * myptcl.weight / area, wtarea = myptcl.weight / area;
                    double wx, wy, wz, cwf = myptcl.charge * myptcl.weight * frac;

                    for(int temp = 0; temp < 5; temp ++)
                    {
                        wtx1[temp] -= wtx0[temp];
                        wty1[temp] -= wty0[temp];
                    }
                    double fjx =  cwf * idty, fjy = cwf * idtx, \
                                 fjz = cwf * idxy * myptcl.velocity.member[2];
                    /// 相邻的3个个点，xy方向都可能有影响
                    int cxmin = _xmin + (dcellx - 1) / 2, cxmax = _xmax + (dcellx + 1) / 2, \
                                cymin = _ymin + (dcelly - 1) / 2, cymax = _ymax + (dcellx + 1) / 2;
                    //cxmin = _xmin, cymin = _ymin, cxmax = _xmax, cymax = _ymax;

                    for(int s = cxmin; s <= cxmax; s ++)
                    {
//#pragma omp critical(jfield, rhofield, energy_grid, density_grid, rhofield)
                        for(int t = cymin; t <= cymax; t++)
                        {
                            //if(indx_old + s >= xsize || indy_old + t >= ysize) continue;
                            wx = wtx1[s + 2] * (wty0[t + 2] + 0.5 * wty1[t + 2]);
                            wy = wty1[t + 2] * (wtx0[s + 2] + 0.5 * wtx1[s + 2]);
                            wz = wtx0[s + 2] * (wty0[t + 2] + 0.5 * wty1[t + 2]) + \
                                 wtx1[s + 2] * (0.5 * wty0[t + 2] + 1.0 / 3 * wty1[t + 2]);
                            //cout<<"wx, wy, wz "<<wx<<"\t"<<wy<<"\t"<<wz<<endl;
                            tempjx[s + 2][t + 2] = tempjx[s + 1][t + 2] - fjx * wx;
                            tempjy[s + 2][t + 2] = tempjy[s + 2][t + 1] - fjy * wy;
                            tempjz[s + 2][t + 2] = fjz * wz;
                            jfield[indx_old + s][indy_old + t].member[0] += tempjx[s + 2][t + 2];
                            jfield[indx_old + s][indy_old + t].member[1] += tempjy[s + 2][t + 2];
                            jfield[indx_old + s][indy_old + t].member[2] += tempjz[s + 2][t + 2];
                            rhofield[indx_old + s][indy_old + t] += wtarea * myptcl.charge * \
                                        wtx0[s + 2] * wty0[t + 2] * frac;
                            density_grid[k][indx_old + s][indy_old + t] += wtarea * wtx0[s + 2] * \
                                        wty0[t + 2] * frac;
                            energy_grid[k][indx_old + s][indy_old + t] += myptcl.weight * myptcl.energy * \
                                        wtx0[s + 2] * wty0[t + 2] * frac;
                            weight_grid[k][indx_old + s][indy_old + t] += myptcl.weight * \
                                        wtx0[s + 2] * wty0[t + 2] * frac;
                        }
                    }
                }
            }
        }
    }
}

long __Cellinfo::size()
{
    long ret = 0;
    for (int i = 0; i < species; i ++)
    {
        ret += plist[i].member.size();
    }
    return ret;
}

void __Cellgroup::dist_en(double energy_start_end_and_dk[], int& dist_species, int& dist_size, double results[])
{
    for(int i = _start; i < member.size() - _end; i ++)
    {
        for(int j = _start; j < member[i].size() - _end; j ++)
        {
            __Ptclslist& mylist = member[i][j].plist[dist_species];
            for(int pl = 0; pl < mylist.member.size(); pl ++){
                __Ptcls& ptcl = mylist.member[pl];
                if(ptcl.energy >= energy_start_end_and_dk[0] \
                        && ptcl.energy <= energy_start_end_and_dk[1]){
                    long ek_index = ptcl.energy / energy_start_end_and_dk[2];
                    if(ek_index >= 0 && ek_index < dist_size)
                    {
                        results[ek_index] += ptcl.weight;
                    }
                }
            }
        }
    }
}




void __Cellgroup::dump(const int& rank, string prefix, const int& sp)
{
    int dumpstart = 2, dumpend = 4;
    string filename;
    if(sp < species)
    {
        //cout<<"sp "<<spptcl[sp].name<<endl;
        filename = prefix + "_density_" + spptcl[sp].name + ".txt";
        ofstream myout;
        myout.open(filename.c_str());
        for(int i = density_grid[sp][0].size() - dumpend; i >= dumpstart; i --){
           for(int j = dumpstart; j <= density_grid[sp].size() - dumpend; j ++){
#if DENSITY_CHANGE
                myout<<density_grid[sp][j][i]<<"\t";
#else
                myout<<density_grid[sp][j][i] + member[j][i].density[sp]<<"\t";
#endif
            }
            myout<<endl;
        }

        myout.close();
    }
    else if(sp < species * 2 && sp >= species)
    {
        int thissp = sp - species;
        filename = prefix + "_ek_" + spptcl[thissp].name + ".txt";
        ofstream myout;
        myout.open(filename.c_str());
        for(int i = energy_grid[thissp][0].size() - dumpend; i >= dumpstart; i --){
            for(int j = dumpstart; j <= energy_grid[thissp].size() - dumpend;\
                    j ++){
                myout<<energy_grid[thissp][j][i] / (1.0 + \
                        weight_grid[thissp][j][i])<<"\t";
            }
            myout<<endl;
        }
        myout.close();
    }
    else
    {
        cout<<"error sp "<<endl;
    }
}

void __Cellgroup::dump_disten(const int& rank, const int& n, string prefix)
{
    string filename;
    int dist_size = int((energy_start_end_and_dk[1] - energy_start_end_and_dk[0]) / energy_start_end_and_dk[2]);
    for(int isp = 0; isp < dist_species.size(); isp ++)
    {
        int sp = dist_species[isp];
        double results[dist_size];
        for(int i = 0; i < dist_size; i ++) results[i] = 0;
        dist_en(energy_start_end_and_dk, sp, dist_size, results);
        filename = prefix + "_dist_" + spptcl[sp].name + ".txt";
        ofstream myout;
        myout.open(filename.c_str());
        for(int i = 0; i < dist_size; i ++)
        {
            myout<<energy_start_end_and_dk[2] * i<<"\t"<<results[i]<<endl;
        }
        myout.close();
    }

}


void get_whxy(const __Vect2<double>& near1, const __Vect2<double>& near2, \
        double fx[3], double fy[3], double hx[3], double hy[3])
{
    double temp;
    temp = near1.member[0];
    fx[0] = 0.5 * (0.5 + temp) * (0.5 + temp);
    fx[1] = 0.75 - temp * temp;
    fx[2] = 0.5 * (0.5 - temp) * (0.5 - temp);
    temp = near1.member[1];
    fy[0] = 0.5 * (0.5 + temp) * (0.5 + temp);
    fy[1] = 0.75 - temp * temp;
    fy[2] = 0.5 * (0.5 - temp) * (0.5 - temp);

    temp = near2.member[0];
    hx[0] = 0.5 * (0.5 + temp) * (0.5 + temp);
    hx[1] = 0.75 - temp * temp;
    hx[2] = 0.5 * (0.5 - temp) * (0.5 - temp);
    temp = near2.member[1];
    hy[0] = 0.5 * (0.5 + temp) * (0.5 + temp);
    hy[1] = 0.75 - temp * temp;
    hy[2] = 0.5 * (0.5 - temp) * (0.5 - temp);


}




void get_j(const __Vect2<double>& near1, const __Vect2<double>& near2, \
        double jx[3][3], double jy[3][3], double jz[3][3], const __Ptcls& myptcl)
{
    double fx[3] = {}, fy[3] = {};
    double hx[3] = {}, hy[3] = {};
    double temp = 0;
    double vxq = myptcl.velocity.member[0] * myptcl.charge * myptcl.weight;
    double vyq = myptcl.velocity.member[1] * myptcl.charge * myptcl.weight;
    double vzq = myptcl.velocity.member[2] * myptcl.charge * myptcl.weight;
    get_whxy(near1, near2, fx, fy, hx, hy); 
    for(int i = 0; i < 3; i ++){
        for(int j = 0; j < 3; j ++)
        {
            jx[i][j] = vxq * hx[i] * fy[j];
            jy[i][j] = vyq * fx[i] * hy[j];
            jz[i][j] = vzq * fx[i] * fy[j];
        }
    }

}



// use the E.Zh.Esirkepov method
void __Cellgroup::weight_to_gridb(const int& myrank)
{
    __Vect2<double> zero2(0, 0);
    __Vect2<double> xhalf(0.5 * dxy.member[0], 0.0);
    __Vect2<double> yhalf(0.0, 0.5 * dxy.member[1]);
    clear_rhoj();
    double area = dxy.member[0] * dxy.member[1];
    double frac = _frac2;
    double dxdt = dxy.member[0] / delta_t, dydt = dxy.member[1] / delta_t, \
                  idxy = 1.0 / dxy.member[0] / dxy.member[1], idt = 1.0 / delta_t, \
                  idx = 1.0 / dxy.member[0], idy = 1.0 / dxy.member[1];
   double jx[3][3], jy[3][3], jz[3][3];
   double fx[3], fy[3], hx[3], hy[3];
   __Vect2<double> newpos;
   int xi, xl, yi, yl;
   __Vect2<double> near1;
   __Vect2<double> near2;
   for(int i = _start; i < member.size() - _start; i ++){
       for(int j = _start; j < member[0].size() - _start; j ++){
           __Cellinfo& mycell = member[i][j];
           for(int k = 0; k < mycell.species; k ++){
               __Ptclslist& mylist = mycell.plist[k];
               if(mylist.member.size() < 0) continue;
               for(int l = 0; l < mylist.member.size(); l ++){
                   __Ptcls& myptcl = mylist.member[l];
                   double wq = myptcl.charge * myptcl.weight;
                   //jx should be staggered to cell center, so jx is shift to right 0.5
                   //jy, jz is on grid, they do not need to shift
                   // nearest full grid
                   double cell_xr, cell_yr;
                   cell_xr = (myptcl.position.member[0] - xymin.member[0]) * idx;
                   cell_yr = (myptcl.position.member[1] - xymin.member[1]) * idy;
                   xi = floor(cell_xr + 0.5);
                   yi = floor(cell_yr + 0.5);
                   // nearest half grid
                   xl = floor(cell_xr);
                   yl = floor(cell_yr);
                   //cout<<xi<<"\t"<<yi<<"\t"<<xl<<"\t"<<yl<<"\t"<<i<<"\t"<<j<<"\t"<<myptcl.position<<"\t"<<xymin<<endl;
                   // full
                   near1 = __Vect2<double>(cell_xr - xi, cell_yr - yi);
                   // half
                   near2 = __Vect2<double>(cell_xr - xl + 0.5, cell_yr - yl + 0.5);

                   get_j(near1, near2, jx, jy, jz, myptcl);


                   for(int tempi = 0; tempi < 3; tempi ++)
                   {
                       for(int tempj = 0; tempj < 3; tempj ++)
                       {
                           jfield[xl + tempi - 1][yi + tempj - 1].member[0] += jx[tempi][tempj] / area;
                           jfield[xi + tempi - 1][yl + tempj - 1].member[1] += jy[tempi][tempj] / area;
                           jfield[xi + tempi - 1][yi + tempj - 1].member[2] += jz[tempi][tempj] / area;
                       }
                   }
                   // since this member is cell centered, but rho is always locate on the grid
                   get_whxy(near1, near2, fx, fy, hx, hy);
                   for(int rhoi = -1; rhoi < 2; rhoi ++)
                   {
                       for(int rhoj = -1; rhoj < 2; rhoj ++)
                       {
                           rhofield[xi + rhoi][yi + rhoj] += myptcl.charge * myptcl.weight * fx[1 + rhoi] \
                                                             * fy[1 + rhoj] * idxy;
                           density_grid[k][xi + rhoi][yi + rhoj] += myptcl.weight * fx[1 + rhoi] * \
                                                                    fy[1 + rhoj] * idxy;
                           energy_grid[k][xi + rhoi][yi + rhoj] += myptcl.energy * myptcl.weight * \
                                                                   fx[1 + rhoi] * fy[1 + rhoj] * idxy;
                       }
                   }
               }
           }
       }
   }

}
