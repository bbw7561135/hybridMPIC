#ifndef __mArray
#define __mArray
#include<fstream>
template<class T>
class myArray{
    public:
        myArray(int size1, int size2):size1(size1), size2(size2)
        {
            data = new T[size1 * size2];
        }
        myArray(int size2):size2(size2), size1(1)
        {
            data = new T[size2];
        }
		myArray() {};
        ~myArray()
        {
            delete[] data;
        }
        T& operator() (int offset)
        {
            return data[offset];
        }
        T& operator() (int offset1, int offset2)
        {
            return data[offset1 * size2 + offset2];
        }
        void init(int size)
        {
          data = new T[size];
          size1 = 1;
          size2 = size;
        }
        void init(int sizei1, int sizei2)
        {
          //std::cout<<sizei1<<"\t"<<sizei2<<std::endl;
          data = new T[sizei1 * sizei2];
          size1 = sizei1;
          size2 = sizei2;
        }
        void write_to_file(std::string& fname)
        {
          std::ofstream out;
          out.open(fname);
          for (size_t j = 0; j < size2; j++) {
            for (size_t i = 0; i < size1; i++) {
              out<<data[i * size2 + j]<<"\t";
            }
            out<<std::endl;
          }
          out.close();
        }
    protected:
        T* data;
        int size1;
        int size2;
};

/***
typedef myArray<double> Darray;
typedef myArray<int> Iarray;
typedef myArray<long> Larray;
typedef myArray<float>Farray;
***/




#endif
