#ifndef __Utility
#define __Utility
#include "simbox.h"
void size_checking(__Simbox& mybox)
{
    cout<<" box size now "<<sizeof(mybox)<<endl;
    cout<<" cellgroup size now "<<sizeof(mybox.cellgroup)<<endl;
    cout<<" fieldgroup size now "<<sizeof(mybox.fieldgroup)<<endl;
    cout<<" bd crossing now "<<sizeof(mybox.crossing_bd_ptcls)<<endl;
}



void print_star(int num)
{
    for(int i = 0; i < num; i ++)
    {
        cout<<"*";
    }
    cout<<endl;
}

void version_info()
{
    print_star(90);
    cout<<"                 hybridMPIC                  "<<endl;
    cout<<"                 version 1.9"<<endl;
    print_star(90);
    cout<<endl<<endl<<endl;
}



void whats_new()
{
    string mystring;
    ifstream whats("./whatsnew.txt");
    if(!whats) return;
    
    print_star(90);
    cout<<"whats_new in this version: "<<endl;
    print_star(90);
    print_star(90);

    while(getline(whats, mystring))
    {
        cout<<mystring<<endl;
    }
    cout<<endl;
    whats.close();
    print_star(90);
    print_star(90);

    cout<<"press any key to start"<<endl;
    //getchar();
}


#endif
