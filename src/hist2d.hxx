#ifndef __Hist2d
#define __Hist2d
#include "arrays.hxx"
class mHist2d: protected myArray<double>
{
public:
  using myArray::write_to_file;
  mHist2d(double x1, double x2, size_t size1, double y1, double y2, size_t size2, string prefix):
  x1(x1), x2(x2), y1(y1), y2(y2), myArray(size1, size2), prefix(prefix)
  {
    deltax = (x2 - x1) / size1;
    deltay = (y2 - y1) / size2;
    setRange(x1, x2, size1, y1, y2, size2, prefix);
  }
  mHist2d(){}
  void setRange(double mx1, double mx2, size_t msize1, double my1, double my2, size_t msize2, string prefix) {
    setPrefix(prefix);
    setXRange(mx1, mx2, msize1, prefix);
    setYRange(my1, my2, msize2, prefix);
  }
  ~mHist2d() {}

  void refresh() {
    for (size_t i = 0; i < size1; i++) {
      for (size_t j = 0; j < size2; j++) {
        data[i * size2 + j] = 0.0;
      }
    }
  }
  void record(double x, double y, double wt) {
    index1 = (x - x1) / deltax;
    index2 = (y - y1) / deltay;
    if(index1 < size1 && index1 >= 0 && index2 < size2 && index2 >= 0) data[index1 * size2 + index2] += wt;
  }
  void setPrefix(string prefixs)
  {
    prefix = prefixs;
  }

  void setXRange(double s1, double s2, size_t sizex1, string prefixs)
  {
    prefix = prefixs;
    x1 = s1;
    x2 = s2;
    size1 = sizex1;
    deltax = (s2 - s1) / size1;
    string filename = prefix + "_x.txt";
    std::ofstream xout(filename);
    for (size_t i = 0; i < size1; i++) {
      xout<<deltax * i<<std::endl;
    }
    xout.close();
  }

  void setYRange(double s1, double s2, size_t sizey1, string prefixs)
  {
    prefix = prefixs;
    y1 = s1;
    y2 = s2;
    size2 = sizey1;
    deltay = (s2 - s1) / size2;
    string filename = prefix + "_y.txt";
    std::ofstream xout(filename);
    for (size_t i = 0; i < size2; i++) {
      xout<<deltay * i<<std::endl;
    }
    xout.close();
  }
protected:
  string prefix;
  double x1, x2, y1, y2, deltax, deltay;
  size_t index1, index2;
  size_t size1, size2;
};





#endif
