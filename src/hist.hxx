#ifndef __Hist1d
#define __Hist1d
#include"arrays.hxx"

class mHist1d:protected myArray<double>
{
public:
	using myArray::write_to_file;
  mHist1d(double s1, double s2, size_t size):s1(s1), s2(s2), size(size), myArray(size)
  {
    delta = (s2 - s1) / size;
    setRange(s1, s2, size);
  }
  mHist1d() {}
  ~mHist1d() {}
  void setRange(double s1, double s2, size_t size);
  void refresh();
  void record(double re, double wt);
protected:
  double s1, s2, delta;
  size_t index;
  size_t size;
};


void mHist1d::setRange(double s1, double s2, size_t size) {
  init(2, size);
  //std::cout<<" after the init "<<std::endl;
  delta = (s2 - s1) / size;
  for (size_t i = 0; i < size; i++) {
    data[i] = delta * i + s1;
  }
}

void mHist1d::record(double re, double wt)
{
  index = (re - s1) / delta;
  if(index < size2 && index >= 0) data[size2 + index] += wt;
}

void mHist1d::refresh() {
  for (size_t i = 0; i < size2; i++) {
    data[size2 + i] = 0;
  }
}

#endif
