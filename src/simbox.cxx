#include "simbox.h"
#include "arrays.hxx"
#include "userdist.hxx"
long __Simbox::size(const int& sp)
{
    long ret = cellgroup.size(sp);
    ret += crossing_bd_ptcls[sp].size();
    /***
    for (int i = 0; i < species; i ++)
    {
        ret += crossing_bd_ptcls[i].size();
    }
    ***/
    return ret;
}

long __Simbox::cbp_num()
{
    long ret = 0;
    for (int i = 0; i < species; i ++) ret += crossing_bd_ptcls[i].capacity();
    return ret;
}


void __Simbox::set_data_file(const vector<string>& fnames1d, const vector<string>& fnames2d)
{
    for(int i = 0; i < fnames1d.size(); i ++)
    {
        filenames1d.push_back(fnames1d[i]);
    }
    for(int i = 0; i < fnames2d.size(); i ++)
    {
        filenames2d.push_back(fnames2d[i]);
    }
}


void __Simbox::set_field_boundary(const int bd_typer[], const int startindexr[], const int damplenr[])
{
    memcpy(bd_type, bd_typer, sizeof(int) * 4);
    memcpy(startindex, startindexr, sizeof(int) * 4);
    memcpy(damplen, damplenr, sizeof(int) * 4);
    memcpy(fieldgroup.bd_type, bd_typer, sizeof(int) * 4);
    memcpy(fieldgroup.startindex, startindexr, sizeof(int) * 4);
    memcpy(fieldgroup.damp_len, damplenr, sizeof(int) * 4);
}

void __Simbox::squeeze()
{
    cellgroup.squeeze();
    for(int i = 0; i < crossing_bd_ptcls.size(); i ++)
    {
        //clear_vec(crossing_bd_ptcls[i]);
        vector<__Ptcls>(crossing_bd_ptcls[i]).swap(crossing_bd_ptcls[i]);
    }
}

void __Simbox::init_flag()
{
    flag = (get_sp_num(0) || get_sp_num(1) || get_sp_num(2) || get_sp_num(3) != 0);
}

double __Simbox::get_sp_energy(int sp)
{
    double ret_ek = 0;
    for(int i = _start; i < cellgroup.member.size() - _end; i ++)
    {
        for(int j = _start; j < cellgroup.member[i].size() - _end; j ++)
        {
            for(int np = 0; np < cellgroup.member[i][j].plist[sp].size(); np ++)
            {
                __Ptcls& myptcl = cellgroup.member[i][j].plist[sp].member[np];
                ret_ek += myptcl.energy * myptcl.weight;
            }
        }
    }

    return ret_ek;
}

int __Simbox::get_sp_num(int sp)
{
    int retnum = 0;
    for(int i = _start; i < cellgroup.member.size() - _end; i ++)
    {
        for(int j = _start; j < cellgroup.member[i].size() - _end; j ++)
        {
            retnum += cellgroup.member[i][j].plist[sp].size();
            //cout<<" size "<<i<<"\t"<<j<<"\t"<<cellgroup.member[i][j].plist[sp].size()<<endl;
        }
    }

    return retnum;
}

double __Simbox::get_density(const __Vect2<double>& xycoords, int sp)
{
    double density  = 0;
    return density;
}

/// domain 代表是中心 0 还是边界 1
void __Simbox::cell_copy_to_field(int domain, const int& myrank)
{
    for(int i = 0; i < cellgroup.jfield.size(); i ++)
    {
	    for(int j = 0; j < cellgroup.jfield[i].size(); j ++)
	    {
            fieldgroup.member[i][j].jfield = cellgroup.jfield[i][j];
	        fieldgroup.member[i][j].rho = cellgroup.rhofield[i][j];
        }
    }
}



void __Simbox::field_copy_to_cell()
{
    int maxx = cellgroup.member.size(), maxy = cellgroup.member[0].size();
    for(int i = 0; i < maxx; i ++){
        for(int j = 0; j < maxy; j ++){
		if(j == maxy - 1)
		{
			cellgroup.member[i][j].fields.exfield.member[1] = 0;
			cellgroup.member[i][j].fields.exfield.member[1] = 0;
			cellgroup.member[i][j].fields.bxfield.member[1] = 0;
		}
		else{
			cellgroup.member[i][j].fields.exfield.member[1] = fieldgroup.member[i][j+1].efield.\
				      member[0];
			cellgroup.member[i][j].fields.exfield.member[1] = fieldgroup.member[i][j+1].efield.\
				      member[0];
			cellgroup.member[i][j].fields.bxfield.member[1] = fieldgroup.member[i][j+1].bfield.\
				      member[0];
		}
		if(i == maxx - 1){
			cellgroup.member[i][j].fields.byfield.member[1] = 0;
			cellgroup.member[i][j].fields.eyfield.member[1] = 0;
		}
		else{
			cellgroup.member[i][j].fields.byfield.member[1] = fieldgroup.member[i+1][j].bfield.\
				  member[1];
			cellgroup.member[i][j].fields.eyfield.member[1] = fieldgroup.member[i+1][j].efield.\
				  member[1];
		}
            cellgroup.member[i][j].fields.exfield.member[0] = fieldgroup.member[i][j].efield.\
                                                              member[0];
            cellgroup.member[i][j].fields.ezfield = fieldgroup.member[i][j].efield.member[2];
            cellgroup.member[i][j].fields.bxfield.member[0] = fieldgroup.member[i][j].bfield.\
                                                              member[0];
            cellgroup.member[i][j].fields.bzfield = fieldgroup.member[i][j].bfield.member[2];
            cellgroup.member[i][j].fields.byfield.member[0] = fieldgroup.member[i][j].bfield.\
                                                              member[1];
            cellgroup.member[i][j].fields.eyfield.member[0] = fieldgroup.member[i][j].efield.\
                                                              member[1];
        }
    }
#if __Debug
	cout<<" field copy over "<<endl;
#endif
}
/// 演化磁场，并damping
void __Simbox::advancest1(const int& rank, const int& step)
{
    if(disable_em)
    {
        return;
    }
    //cout<<"initing flag, em will be executed "<<endl;
    init_flag();
    fieldgroup.advancepre(rank, delta_t);
}

/// 演化电场
void __Simbox::advancest2(const int& rank, const int& step)
{
    if(disable_em)
    {
        return;
    }
    fieldgroup.update_efield(0.5 * delta_t);
}

/// 演化磁场反damping，拷贝场到cell，场weight到ptcls
void __Simbox::advancest3(const int& rank, const int& step)
{
    if(disable_em)
    {
        return;
    }
#if __Debug
    cout<<"field advancenex "<<endl;
#endif
    fieldgroup.advancenex(rank, delta_t);
#if __Debug
    cout<<" field copy to cell "<<endl;
#endif
    field_copy_to_cell();
}

/// 推进粒子
void __Simbox::advancest4(const int& rank, const int& n)
{
#if __Debug
    cout<<" weighting to ptcls "<<endl;
#endif
    weight_to_ptcls(rank);
#if __Debug
    cout<<" weighting over "<<endl;
#endif
    //cout<<"size "<<cellgroup.member[30][30].plist[0].size()<<endl;
    // 这两个不能交换，否则粒子被压栈到ｂｄ中就不能辐射了

#if __Debug
	cout<<rank<<" advancing cells "<<endl;
#endif
    advance(cellgroup, delta_t, crossing_bd_ptcls, rank, n);
#if __Debug
	cout<<rank<<" advancing cell over "<<endl;
#endif
}
// 交换数据后只后要进行migration

/// 进行migration，将粒子weight到grid上，拷贝到field中
void __Simbox::advancest5(const int& rank, const int& step)
{
#if __Debug
    cout<<" migrating "<<endl;
#endif
    migration(rank);
    /// 必须开启,因为密度更新也在此过程中.
    for(int i = 0; i < 4; i ++)
    {
        if(monte_flag[i]!=0)
        {
#if __Debug
            cout<<rank<<" begin monte_carlo "<<i<<endl;
#endif
#if DPHOTON
            if(i == 0 || i == 2)
            {
                monte_carlo(cellgroup, i, delta_t, tables1d, tables2d);
            }
            else{
                monte_carlo(cellgroup, i, delta_t, bh_tables1d, bh_tables2d);
            }
#endif
#if __Debug
            cout<<rank<<" over monte_carlo "<<i<<endl;
#endif
        }
    }
#if __Debug
    cout<<" weight to grid "<<endl;
#endif
    //cellgroup.weight_to_gridb(rank);
    cellgroup.weight_to_grid(rank, step);

#if __Debug
    cout<<" weight to grid over "<<endl;
#endif
}


//__Vect2<int> __Simbox::coords_to_cell(const __Vect2<double>& xycoords)
__Vect2<int> __Simbox::coords_to_cell(__Ptcls& ptcl, int myrank)
{
    __Vect2<double> xycoords = ptcl.position;
    __Vect2<int> cell_position(-1, -1);
    __Vect2<double> temp = xycoords - xymin;//cellgroup.member[0][0].xymin;
    if(temp.member[0] >= 0 && temp.member[1] >= 0)
    {
        cell_position.member[0] = floor(temp.member[0] / dxy.member[0]);
        cell_position.member[1] = floor(temp.member[1] / dxy.member[1]);
        if(cell_position.member[0] < cellgroup.member.size() - _start &&\
                cell_position.member[0] >= _start && \
                cell_position.member[1] < cellgroup.member[0].size() - _start &&\
                cell_position.member[1] >= _start)
        {
        }
        else{
            cell_position = __Vect2<int> (-1, -1);
        }
    }
    else{
        cell_position = __Vect2<int> (-1, -1);
    }
    return cell_position;
}


void __Simbox::migration(const int& myrank)
{
    /***
    for(int i = 0; i < cellgroup.member[0].size(); i ++)
    {
        for(int j = 0; j < species; j ++)
        {
            clear_vec(cellgroup.member[0][i].plist[j].member);
            clear_vec(cellgroup.member[cellgroup.member.size() - 1][i].plist[j].member);
        }
    }
    for(int i = 0; i < cellgroup.member.size(); i ++)
    {
        for(int j = 0; j < species; j ++)
        {
            clear_vec(cellgroup.member[i][0].plist[j].member);
            clear_vec(cellgroup.member[i][cellgroup.member[i].size() - 1].plist[j].member);
        }
    }
    ***/

    for(int i = 0; i < this->species; i++)
    {
        int total_size = crossing_bd_ptcls[i].size();
        for(int j = 0; j < total_size; j ++)
        {
            __Ptcls& thisptcl = crossing_bd_ptcls[i][j];
            __Vect2<int> cell_position = coords_to_cell(thisptcl, myrank);
            if(cell_position.member[0] > 0 && cell_position.member[1] > 0)
            {
                this->cellgroup.member[cell_position.member[0]]\
                [cell_position.member[1]].push(thisptcl);
            }
            else
            {
                __Ptcls::total_num --;
            }
        }
        clear_vec(crossing_bd_ptcls[i]);
	if(crossing_bd_ptcls[i].size() > 0) cout<<" bd not clear "<<endl;
    }
}

int __Simbox::read_tables2d()
{
    vector<vector<double> > tabletemp;
    for(int i = 0; i < filenames2d.size(); i ++)
    {
        //cout<<"reading "<<filenames2d[i]<<endl;
        ifstream in(filenames2d[i], ios::in);
        double temp;
        string line;
        vector<double> row;
        while(getline(in, line)){
            istringstream istr(line);
            while(istr>>temp){
                //cout<<"2d reading "<<temp<<endl;
                row.push_back(temp);
            }
            tabletemp.push_back(row);
            istr.clear();
            line.clear();
            row.clear();
        }
        if(i > 2)
        {
            bh_tables2d.push_back(tabletemp);
        }
        else{
            tables2d.push_back(tabletemp);
        }
        //cout<<"temp size "<<tabletemp.size()<<endl;
        clear_vec(tabletemp);
        clear_vec(row);
        //tabletemp.clear();
    }
    return 0;
}



int __Simbox::read_tables1d()
{
    double temp;
    for(int i = 0; i < filenames1d.size(); i ++)
    {
        //cout<<"reading "<<filenames1d[i]<<endl;
        ifstream in;
        in.open(filenames1d[i], ios::in);
        vector<double> tb;
        while(in>>temp)
        {
            tb.push_back(temp);
        }
        if(i > 6)
        {
            bh_tables1d.push_back(tb);
        }
        else{
            tables1d.push_back(tb);
        }
        clear_vec(tb);
    }
    return 0;
}



void __Simbox::init(string fname, int myrank)
{
    string prefix = getPrefix(fname, myrank, 0);
    userPreLoop(prefix, myrank);
    vector<__Ptcls> tempbundle;
    for(int i = 0; i < species; i ++)
    {
        crossing_bd_ptcls.push_back(tempbundle);
    }
    clear_vec(tempbundle);
    //cout<<"initing "<<crossing_bd_ptcls[0].size()<<endl;
    //cout<<"creating cell group"<<endl;
    __Cellgroup mygroup(xygridmin, xygridmax, xymin, xymax, xycell, species, ppc, spptcl, \
            delta_t, position, maxid);
    mygroup.dxy = dxy;
    cellgroup = mygroup;
    cellgroup.disable_em = disable_em;
    //cout<<"cell group initing"<<endl;
    cellgroup.init(myrank);
    cellgroup.set_bd(bd_type);
    //cout<<"initing field"<<endl;
    __Fieldgroup myfield(xycell.member[0] + adder, xycell.member[1] + adder, \
            dxy, bd_type, startindex, damplen);
    fieldgroup = myfield;
    fieldgroup.init();

    if(monte_flag[0] + monte_flag[1] + monte_flag[2] + monte_flag[3] != 0)
    {
        if( read_tables1d() != 0)
        {
            cout<<"read tables1d failed, monte_carlo will not work!"<<endl;
            for(int i = 0; i < 4; i ++)
            {
                monte_flag[i] = 0;
            }
        }
        if(read_tables2d() != 0)
        {
            cout<<"read tables2d failed, monte_carlo will not work!"<<endl;
            for(int i = 0; i < 4; i ++)
            {
                monte_flag[i] = 0;
            }
        }
    }
}


ostream& operator<<(ostream &out, const __Simbox& mybox){
    out<<" simname: "<<mybox.simname<<endl<<" simx: "<<mybox.xrange<<endl\
        <<" simy: "<<mybox.yrange<<endl<<" xycell: "<<mybox.xycell<<endl<<" xygridmin: "\
        <<mybox.xygridmin<<endl<<" xygridmax: "<<mybox.xygridmax\
        <<endl<<" time: "<<mybox.time;
	return out;
}


// direction 0: left & right, 1: down & up
void __Simbox::copy_to_pline(__Pline& plinel, __Pline& pliner, int direction, const int& myrank)
{
    __Pdata pdata;
    for(int i = 0; i < crossing_bd_ptcls.size(); i ++)
    {
        for(int j = 0; j < crossing_bd_ptcls[i].size(); j ++)
        {
            __Ptcls& ptcls = crossing_bd_ptcls[i][j];
            if(direction == 0)
            {
                //if(ptcls.position.member[0] < xymin.member[0] )
                if(ptcls.position.member[0] < xymin.member[0] + _start * dxy.member[0])
                {
                    //cout<<" id "<<ptcls.id<<" benn pushed "<<endl;
                    copy_to_pdata(ptcls, pdata, myrank);
                    plinel.push_back(pdata);
                }
                else if(ptcls.position.member[0] > xymax.member[0] - _start * dxy.member[0])
                {
                    //cout<<" id "<<ptcls.id<<" benn pushed "<<endl;
                    copy_to_pdata(ptcls, pdata, myrank);
                    pliner.push_back(pdata);
                }
                // 这里由于越界例子会在migration时删除因此，我不需要把它们从crossing_bd中删除
            }
            if(direction == 1)
            {
                if(ptcls.position.member[1] < xymin.member[1] + _start * dxy.member[1])
                {
                    copy_to_pdata(ptcls, pdata, myrank);
                    plinel.push_back(pdata);
                }
                else if(ptcls.position.member[1] > xymax.member[1] - _start * dxy.member[1])
                {
                    copy_to_pdata(ptcls, pdata, myrank);
                    pliner.push_back(pdata);
                }
                // 这里由于越界例子会在migration时删除因此，我不需要把它们从crossing_bd中删除
            }
        }
    }
}


void __Simbox::pline_copy_to(__Pline& plinel, __Pline& pliner, int direction, const int& myrank)
{
    // 这个其实跟方向就没有关系了,因为跟网格暂时没有关系
    if(plinel.size() + pliner.size() == 0)
    {
        return;
    }
    __Ptcls ptcls;
    if(direction == 0)
    {
        if(myrank != 0)
        {
            for(int i = 0; i < plinel.size(); i ++)
            {
                copy_to_ptcl(plinel[i], ptcls, myrank);
                crossing_bd_ptcls[ptcls.species].push_back(ptcls);
                ptcls.total_id ++;
            }
        }
        if(myrank != -1)
        {
            for(int i = 0; i < pliner.size(); i ++)
            {
                copy_to_ptcl(pliner[i], ptcls, myrank);
                crossing_bd_ptcls[ptcls.species].push_back(ptcls);
                ptcls.total_id ++;
            }
        }
    }


    if(direction == 1)
    {

        for(int i = 0; i < plinel.size(); i ++)
        {
            copy_to_ptcl(plinel[i], ptcls, myrank);
            crossing_bd_ptcls[ptcls.species].push_back(ptcls);
            ptcls.total_id ++;
        }
        for(int i = 0; i < pliner.size(); i ++)
        {
            copy_to_ptcl(pliner[i], ptcls, myrank);
            crossing_bd_ptcls[ptcls.species].push_back(ptcls);
            ptcls.total_id ++;
        }
    }
}


void __Simbox::fline_copy_to(__Fline& flinel, __Fline& fliner, int direction, const int& myrank, int fsp)
{
    //接收两列，总是先左后右，线下后上。
    //cout<<"fline copy to"<<endl;
    int xsize = fieldgroup.member.size(), ysize = fieldgroup.member[0].size();
    if(direction == 0)
    {
        if(fsp == 0)
        {
            if(myrank != 0)
            {
                for(int j = 0; j < mpi_from_left; j ++)
                {
                    for(int i = 0; i < ysize; i ++)
                    {
                        fieldgroup.member[j][i].efield.member[0] = flinel[ysize * j + i].data[0];
                        fieldgroup.member[j][i].efield.member[1] = flinel[ysize * j + i].data[1];
                        fieldgroup.member[j][i].efield.member[2] = flinel[ysize * j + i].data[2];
                    }
                }
            }

            if(myrank != - 1)
            {
                for(int j = 0; j < mpi_from_right; j ++)
                {
                    for(int i = 0; i < ysize; i ++)
                    {
                        fieldgroup.member[xsize + j - mpi_from_right][i].efield.member[0] =\
                            fliner[ysize * j + i].data[0];
                        fieldgroup.member[xsize + j - mpi_from_right][i].efield.member[1] =\
                            fliner[ysize * j + i].data[1];
                        fieldgroup.member[xsize + j - mpi_from_right][i].efield.member[2] =\
                            fliner[ysize * j + i].data[2];
                    }
                }
            }
        }
        else
        {
           if(myrank != 0)
            {
                for(int j = 0; j < mpi_from_left; j ++)
                {
                    for(int i = 0; i < ysize; i ++)
                    {
                        fieldgroup.member[j][i].bfield.member[0] = flinel[ysize * j + i].data[3];
                        fieldgroup.member[j][i].bfield.member[1] = flinel[ysize * j + i].data[4];
                        fieldgroup.member[j][i].bfield.member[2] = flinel[ysize * j + i].data[5];
                    }
                }
            }

            if(myrank != - 1)
            {
                for(int j = 0; j < mpi_from_right; j ++)
                {
                    for(int i = 0; i < ysize; i ++)
                    {
                        fieldgroup.member[xsize + j - mpi_from_right][i].bfield.member[0] =\
                            fliner[ysize * j + i].data[3];
                        fieldgroup.member[xsize + j - mpi_from_right][i].bfield.member[1] =\
                            fliner[ysize * j + i].data[4];
                        fieldgroup.member[xsize + j - mpi_from_right][i].bfield.member[2] =\
                            fliner[ysize * j + i].data[5];
                    }
                }
            }
        }

    }
    if(direction == 1)
    {
        if(myrank != 0)
        {
            for(int j = 0; j < mpi_from_left; j ++)
            {
                for(int i = 0; i < xsize; i ++)
                {
                    fieldgroup.member[i][j] = flinel[xsize * j + i];
                }
            }
        }

        if(myrank != - 1)
        {
            for(int j = 0; j < mpi_from_right; j ++)
            {
                for(int i = 0; i < xsize; i ++)
                {
                    fieldgroup.member[i][ysize + j - mpi_from_right] = fliner[xsize * j + i];
                }
            }
        }
    }
}
void __Simbox::copy_to_fline(__Fline& flinel, __Fline& fliner, int direction, const int& myrank)
{
    __Fdata fdatal, fdatar;
    int xsize = fieldgroup.member.size(), ysize = fieldgroup.member[0].size();
    if(direction == 0)
    {
        for(int j = 0; j < mpi_to_left; j ++)
        {
            for(int i = 0; i < ysize; i ++)
            {
                copy_to_fdata(fieldgroup.member[j + mpi_to_left + 1][i], fdatal);
                flinel.push_back(fdatal);
            }
        }
        for(int j = 0; j < mpi_to_right; j ++)
        {
            for(int i = 0; i < ysize; i ++)
            {
                copy_to_fdata(fieldgroup.member[xsize - 2 * mpi_to_right + j - 1][i], fdatar);
                fliner.push_back(fdatar);
            }
        }
    }

    if(direction == 1)
    {
        for(int j = 0; j < mpi_to_left; j ++)
        {
            for(int i = 0; i < xsize; i ++)
            {
                copy_to_fdata(fieldgroup.member[i][j + mpi_to_left], fdatal);
                flinel.push_back(fdatal);
            }
        }
        for(int j = 0; j < mpi_to_right; j ++)
        {
            for(int i = 0; i < xsize; i ++)
            {
                copy_to_fdata(fieldgroup.member[i][ysize - 2 * mpi_to_right + j], fdatar);
                fliner.push_back(fdatar);
            }
        }
    }
}

void __Simbox::copy_to_jline(__Jline& jlinel, __Jline& jliner, int direction, const int& myrank)
{
#if __Debug
    cout<<myrank<<" start copy to jline"<<endl;
#endif
    __Jdata jdatal, jdatar;
    int xsize = cellgroup.jfield.size(), ysize = cellgroup.jfield[0].size();
    if(direction == 0)
    {
        for(int j = 0; j < _jstart; j ++)
        {
            for(int i = 0; i < ysize; i ++)
            {
                copy_to_jdata(cellgroup.jfield[j][i], cellgroup.rhofield[j][i], jdatal);
                jlinel.push_back(jdatal);
                copy_to_jdata(cellgroup.jfield[xsize - _jstart + j][i], cellgroup.rhofield[xsize - _jstart + j][i], jdatar);
                jliner.push_back(jdatar);
            }
        }
    }

    if(direction == 1)
    {
        for(int j = 0; j < _jstart; j ++)
        {
            for(int i = 0; i < xsize; i ++)
            {
                copy_to_jdata(cellgroup.jfield[i][j], cellgroup.rhofield[i][j], jdatal);
                //copy_to_jdata(cellgroup.jfield[i][j], jdatal);
                jlinel.push_back(jdatal);
                copy_to_jdata(cellgroup.jfield[i][ysize - _jstart + j], cellgroup.rhofield[i][ysize - _jstart + j], jdatar);
                //copy_to_jdata(cellgroup.jfield[i][ysize - _jstart + j], jdatar);
                jliner.push_back(jdatar);
            }
        }
    }
#if __Debug
    cout<<myrank<<" over copy to jline"<<endl;
#endif
}


/// 将信息储存在边缘，最后叠加到１上
void __Simbox::jline_copy_to(__Jline& jlinel, __Jline& jliner, int direction, const int& myrank)
{
#if __Debug
    cout<<myrank<<" start jline copy to "<<endl;
#endif
    int xsize = cellgroup.jfield.size(), ysize = cellgroup.jfield[0].size();
    if(direction == 0)
    {
        if(myrank != 0)
        {
            for(int j = 0; j < _jstart; j ++)
            {
                for(int i = 0; i < ysize; i ++)
                {
                    for(int k = 0; k < 3; k ++)
                    {
                        cellgroup.jfield[j + _jstart - 1][i].member[k] += jlinel[ysize * j + i].data[k];
                    }
                    cellgroup.rhofield[j + _jstart - 1][i] += jlinel[ysize * j + i].data[3];
                }
            }
            //cout<<myrank<<" after add "<<cellgroup.jfield[2][100].member[0]<<endl;
        }

        if(myrank != - 1)
        {
            for(int j = 0; j < _jstart; j ++)
            {
                for(int i = 0; i < ysize; i ++)
                {
                    for(int k = 0; k < 3; k ++)
                    {
                        cellgroup.jfield[xsize - 2 * _jstart + j + 1][i].member[k] += jliner[ysize * j + i].data[k];
                    }
                    cellgroup.rhofield[xsize - 2 * _jstart + j + 1][i] += jliner[ysize * j + i].data[3];
                }
            }
        }
    }
    if(direction == 1)
    {
        if(myrank != 0)
        {
            for(int j = 0; j < _jstart; j ++)
            {
                for(int i = 0; i < xsize; i ++)
                {
                    for(int k = 0; k < 3; k ++)
                    {
                        cellgroup.jfield[i][j + _jstart - 1].member[k] += jlinel[xsize * j + i].data[k];
                    }
                    cellgroup.rhofield[i][j + _jstart - 1] += jlinel[xsize * j + i].data[3];
                }
            }
        }

        if(myrank != - 1)
        {
            for(int j = 0; j < _jstart; j ++)
            {
                for(int i = 0; i < xsize; i ++)
                {
                    for(int k = 0; k < 3; k ++)
                    {
                        cellgroup.jfield[i][ysize - 2 * _jstart + 1 + j].member[k] += jliner[xsize * j + i].data[k];
                    }
                    cellgroup.rhofield[i][ysize - 2 * _jstart + 1 + j] += jliner[xsize * j + i].data[3];
                }
            }
        }
    }
#if __Debug
    cout<<myrank<<" over jline copy to "<<endl;
#endif
}






/// 采用T.D.Pointon/CPC 179(2008) 535-544 算法
void __Simbox::weight_to_ptcls(const int& rank)
{
    if(disable_em) return;
    double frac = _frac2, idx = 1.0 / dxy.member[0], idy = 1.0 / dxy.member[1];
    __Vect3<double> zeros(0.0, 0.0, 0.0);

    for(int i = _start; i < cellgroup.member.size() - _end; i ++)
    {
        for(int j = _start; j < cellgroup.member[1].size() - _end; j ++)
        {
            __Vect2<double>& cellmin = cellgroup.member[i][j].xymin;
            for(int k = 0; k < cellgroup.member[i][j].plist.size(); k ++)
            {
                for(int l = 0; l < cellgroup.member[i][j].plist[k].size(); l ++)
                {
                    __Ptcls& myptcl = cellgroup.member[i][j].plist[k].member[l];
                    myptcl.efield = zeros;
                    myptcl.bfield = zeros;
                    /// 对相邻的三个cell中的场进行插值到粒子
                    /// 这里采用相对坐标local
                    /// 向前半步得到新的v
                    //__Vect2<double> nex_pos = myptcl.position + __Vect2<double>(myptcl.velocity.member[0], \
                            myptcl.velocity.member[1]) * 0.5 * delta_t;
                    __Vect2<double> nex_pos = myptcl.position;
                    double cell_xr = (nex_pos.member[0] - xymin.member[0]) * idx, \
                                     cell_yr = (nex_pos.member[1] - xymin.member[1]) * idy;
                    double wx[3], wy[3], hx[3], hy[3];

                    // full grid nearest
                    int indx1 = floor(cell_xr + 0.5), indy1 = floor(cell_yr + 0.5);
                    double fracx = indx1 - cell_xr, fracy = indy1 - cell_yr;
                    //最近半格点的权重
                    get_wxy(fracx, fracy, wx, wy);
                    // half grid nearest
                    int indx2 = floor(cell_xr), indy2 = floor(cell_yr);
                    // since index = lower bound of the cell number
                    fracx = indx2 - cell_xr + 0.5, fracy = indy2 - cell_yr + 0.5;
                    get_wxy(fracx, fracy, hx, hy);
                    //ex 在x方向半格点,y方向整数格点
                    //index n: 1: full, 2:half
                    myptcl.efield.member[0] = \
                                              wy[0] * (hx[0] * fieldgroup.member[indx2 - 1][indy1 - 1].efield.member[0]\
                                              +        hx[1] * fieldgroup.member[indx2    ][indy1 - 1].efield.member[0]\
                                              +        hx[2] * fieldgroup.member[indx2 + 1][indy1 - 1].efield.member[0])\
                                            + wy[1] * (hx[0] * fieldgroup.member[indx2 - 1][indy1    ].efield.member[0]\
                                              +        hx[1] * fieldgroup.member[indx2    ][indy1    ].efield.member[0]\
                                              +        hx[2] * fieldgroup.member[indx2 + 1][indy1    ].efield.member[0])\
                                            + wy[2] * (hx[0] * fieldgroup.member[indx2 - 1][indy1 + 1].efield.member[0]\
                                              +        hx[1] * fieldgroup.member[indx2    ][indy1 + 1].efield.member[0]\
                                              +        hx[2] * fieldgroup.member[indx2 + 1][indy1 + 1].efield.member[0]);
                    //ey is on the half grid of y direction and full grid of x direction
                    myptcl.efield.member[1] = \
                                              hy[0] * (wx[0] * fieldgroup.member[indx1 - 1][indy2 - 1].efield.member[1]\
                                              +        wx[1] * fieldgroup.member[indx1    ][indy2 - 1].efield.member[1]\
                                              +        wx[2] * fieldgroup.member[indx1 + 1][indy2 - 1].efield.member[1])\
                                            + hy[1] * (wx[0] * fieldgroup.member[indx1 - 1][indy2    ].efield.member[1]\
                                              +        wx[1] * fieldgroup.member[indx1    ][indy2    ].efield.member[1]\
                                              +        wx[2] * fieldgroup.member[indx1 + 1][indy2    ].efield.member[1])\
                                            + hy[2] * (wx[0] * fieldgroup.member[indx1 - 1][indy2 + 1].efield.member[1]\
                                              +        wx[1] * fieldgroup.member[indx1    ][indy2 + 1].efield.member[1]\
                                              +        wx[2] * fieldgroup.member[indx1 + 1][indy2 + 1].efield.member[1]);
                    // ez is on the grid of each dirction
                    myptcl.efield.member[2] = \
                                              wy[0] * (wx[0] * fieldgroup.member[indx1 - 1][indy1 - 1].efield.member[2]\
                                              +        wx[1] * fieldgroup.member[indx1    ][indy1 - 1].efield.member[2]\
                                              +        wx[2] * fieldgroup.member[indx1 + 1][indy1 - 1].efield.member[2])\
                                            + wy[1] * (wx[0] * fieldgroup.member[indx1 - 1][indy1    ].efield.member[2]\
                                              +        wx[1] * fieldgroup.member[indx1    ][indy1    ].efield.member[2]\
                                              +        wx[2] * fieldgroup.member[indx1 + 1][indy1    ].efield.member[2])\
                                            + wy[2] * (wx[0] * fieldgroup.member[indx1 - 1][indy1 + 1].efield.member[2]\
                                              +        wx[1] * fieldgroup.member[indx1    ][indy1 + 1].efield.member[2]\
                                              +        wx[2] * fieldgroup.member[indx1 + 1][indy1 + 1].efield.member[2]);

                    // bx is over lapped with ey, but with on the plane of yz, should be same with ey
                    myptcl.bfield.member[0] = \
                                              hy[0] * (wx[0] * fieldgroup.member[indx1 - 1][indy2 - 1].bfield.member[0]\
                                              +        wx[1] * fieldgroup.member[indx1    ][indy2 - 1].bfield.member[0]\
                                              +        wx[2] * fieldgroup.member[indx1 + 1][indy2 - 1].bfield.member[0])\
                                            + hy[1] * (wx[0] * fieldgroup.member[indx1 - 1][indy2    ].bfield.member[0]\
                                              +        wx[1] * fieldgroup.member[indx1    ][indy2    ].bfield.member[0]\
                                              +        wx[2] * fieldgroup.member[indx1 + 1][indy2    ].bfield.member[0])\
                                            + hy[2] * (wx[0] * fieldgroup.member[indx1 - 1][indy2 + 1].bfield.member[0]\
                                              +        wx[1] * fieldgroup.member[indx1    ][indy2 + 1].bfield.member[0]\
                                              +        wx[2] * fieldgroup.member[indx1 + 1][indy2 + 1].bfield.member[0]);
                    // by is over lapped with ex, but with on the plane of xz, should be same with ex
                    myptcl.bfield.member[1] = \
                                              wy[0] * (hx[0] * fieldgroup.member[indx2 - 1][indy1 - 1].bfield.member[1]\
                                              +        hx[1] * fieldgroup.member[indx2    ][indy1 - 1].bfield.member[1]\
                                              +        hx[2] * fieldgroup.member[indx2 + 1][indy1 - 1].bfield.member[1])\
                                            + wy[1] * (hx[0] * fieldgroup.member[indx2 - 1][indy1    ].bfield.member[1]\
                                              +        hx[1] * fieldgroup.member[indx2    ][indy1    ].bfield.member[1]\
                                              +        hx[2] * fieldgroup.member[indx2 + 1][indy1    ].bfield.member[1])\
                                            + wy[2] * (hx[0] * fieldgroup.member[indx2 - 1][indy1 + 1].bfield.member[1]\
                                              +        hx[1] * fieldgroup.member[indx2    ][indy1 + 1].bfield.member[1]\
                                              +        hx[2] * fieldgroup.member[indx2 + 1][indy1 + 1].bfield.member[1]);
                    // bz is on the center of cell, with half grid in each direction
                    myptcl.bfield.member[2] = \
                                              hy[0] * (hx[0] * fieldgroup.member[indx2 - 1][indy2 - 1].bfield.member[2]\
                                              +        hx[1] * fieldgroup.member[indx2    ][indy2 - 1].bfield.member[2]\
                                              +        hx[2] * fieldgroup.member[indx2 + 1][indy2 - 1].bfield.member[2])\
                                            + hy[1] * (hx[0] * fieldgroup.member[indx2 - 1][indy2    ].bfield.member[2]\
                                              +        hx[1] * fieldgroup.member[indx2    ][indy2    ].bfield.member[2]\
                                              +        hx[2] * fieldgroup.member[indx2 + 1][indy2    ].bfield.member[2])\
                                            + hy[2] * (hx[0] * fieldgroup.member[indx2 - 1][indy2 + 1].bfield.member[2]\
                                              +        hx[1] * fieldgroup.member[indx2    ][indy2 + 1].bfield.member[2]\
                                              +        hx[2] * fieldgroup.member[indx2 + 1][indy2 + 1].bfield.member[2]);


                    myptcl.efield = myptcl.efield * frac;
                    myptcl.bfield = myptcl.bfield * frac;
                }
            }
        }
    }
}



/***
/// 采用如同EPOCH中的粒子三角形，场均匀算法
void __Simbox::weight_to_ptcls()
{
    __Vect3<double> zeros(0.0, 0.0, 0.0);
    __Vect2<double> xhalf(dxy.member[0] * 0.5, 0);
    __Vect2<double> yhalf = dxy * 0.5 - xhalf;
    double frac = _frac2;
    for(int i = 1; i < cellgroup.member.size() - 1; i ++)
    {
        for(int j = 1; j < cellgroup.member[1].size() - 1; j ++)
        {
            int indx = i, indy = j;
            __Vect2<double>& cellmin = cellgroup.member[i][j].xymin;
            for(int k = 0; k < cellgroup.member[i][j].plist.size(); k ++)
            {
                for(int l = 0; l < cellgroup.member[i][j].plist[k].size(); l ++)
                {
                    __Ptcls& myptcl = cellgroup.member[i][j].plist[k].member[l];
                    if(std::isnan(myptcl.energy)) cout<<myptcl<<" nan energy"<<endl;
                    myptcl.efield = zeros;
                    myptcl.bfield = zeros;
                    /// 对相邻的三个cell中的场进行插值到粒子
                    /// 这里采用相对坐标local

                    ///已经验证，没有错误
                    indx = floor((myptcl.position.member[0] + dxy.member[0] * 0.5 - \
                                cellmin.member[0]) / dxy.member[0]) + i;
                    indy = floor((myptcl.position.member[1] + dxy.member[1] * 0.5 - \
                                cellmin.member[1]) / dxy.member[1]) + j;
                    __Vect2<double> ptcls_ratio((cellgroup.member[indx][indy].xymin - myptcl.position) / dxy);
                    __Vect2<double> ptcls_ratio_center;
                    /// 对中间点进行shift
                    ptcls_ratio_center.member[0] = (ptcls_ratio.member[0] > 0)?(0.5 - ptcls_ratio.member[0]):\
                                                   (0.5 + ptcls_ratio.member[0]);
                    ptcls_ratio_center.member[1] = (ptcls_ratio.member[1] > 0)?(0.5 - ptcls_ratio.member[1]):\
                                                   (0.5 + ptcls_ratio.member[1]);
                    double tempx0[5], tempy0[5], tempx1[5], tempy1[5];
                    //ptcls_ratio_center = ptcls_ratio;
                    grid_wxy(ptcls_ratio.member[0], ptcls_ratio.member[1], tempx0, tempy0);
                    grid_wxy(ptcls_ratio_center.member[0], ptcls_ratio_center.member[1], tempx1, tempy1);



                    /// 3 grid near the current cell, since field are all locate on the center
                    for(int xi = _xmin; xi <= _xmax; xi ++)
                    {
                        if(indx + xi < 1 || indx + xi > fieldgroup.member.size() - 1)
                        {
                            continue;
                        }
                        for(int yj = _ymin; yj <= _ymax; yj ++)
                        {
                            if(indy + yj < 1 || indy + yj > fieldgroup.member[0].size() - 1)
                            {
                                continue;
                            }

                            /// use square area weighting
#ifdef __Square
                            __Vect2<double> local = xhalf * (2.0 * xi) + yhalf * (2.0 * yj);
                            myptcl.efield.member[0] += fieldgroup.member[indx+xi][indy+yj].efield.member[0] \
                                                       * weightfunc(myptcl.position, cellmin + local + xhalf, dxy);
                            myptcl.efield.member[1] += fieldgroup.member[indx+xi][indy+yj].efield.member[1] \
                                                       * weightfunc(myptcl.position, cellmin + local + yhalf, dxy);
                            myptcl.efield.member[2] += fieldgroup.member[indx+xi][indy+yj].efield.member[2] \
                                                       * weightfunc(myptcl.position, cellmin + local + xhalf \
                                                       + yhalf, dxy);
                            myptcl.bfield.member[0] += fieldgroup.member[indx+xi][indy+yj].bfield.member[0] \
                                                       * weightfunc(myptcl.position, cellmin + local + xhalf, dxy);
                            myptcl.bfield.member[1] += fieldgroup.member[indx+xi][indy+yj].bfield.member[1] \
                                                       * weightfunc(myptcl.position, cellmin + local + yhalf, dxy);
                            myptcl.bfield.member[2] += fieldgroup.member[indx+xi][indy+yj].bfield.member[2] \
                                                       * weightfunc(myptcl.position, cellmin + local + xhalf \
                                                       + yhalf, dxy);
#endif
                            myptcl.efield.member[0] += fieldgroup.member[indx+xi][indy+yj].efield.member[0] \
                                                        * tempx1[xi + 2] * tempy0[yj + 2] * frac;
                            myptcl.efield.member[1] += fieldgroup.member[indx+xi][indy+yj].efield.member[1] \
                                                        * tempx1[xi + 2] * tempy0[yj + 2] * frac;
                            myptcl.efield.member[2] += fieldgroup.member[indx+xi][indy+yj].efield.member[2] \
                                                        * tempx1[xi + 2] * tempy1[yj + 2] * frac;
                            myptcl.bfield.member[0] += fieldgroup.member[indx+xi][indy+yj].bfield.member[0] \
                                                        * tempx1[xi + 2] * tempy0[yj + 2] * frac;
                            myptcl.bfield.member[1] += fieldgroup.member[indx+xi][indy+yj].bfield.member[1] \
                                                        * tempx0[xi + 2] * tempy1[yj + 2] * frac;
                            myptcl.bfield.member[2] += fieldgroup.member[indx+xi][indy+yj].bfield.member[2] \
                                                        * tempx1[xi + 2] * tempy1[yj + 2] * frac;
                        }
                    }
                }
            }
        }
    }
}
***/



void __Simbox::wide_merge(const int& sp, const int& cellnum)
{
    for(int cidx = cellnum; cidx < cellgroup.member.size() - cellnum; cidx ++)
    {
        for(int cidy = cellnum; cidy < cellgroup.member[0].size() - cellnum; cidy++)
        {
            __Ptclslist& current = cellgroup.member[cidx][cidy].plist[sp];
            int nid = 0;
            __Vect3<double> dvv;
            while(nid < current.member.size())
            {
                for(int xid = 0; xid < cellnum; xid ++)
                {
                    for(int yid = 0; yid < cellnum; yid ++)
                    {
                        __Ptclslist& mlist = cellgroup.member[xid][yid].plist[sp];
                        for(auto p = mlist.member.begin(); p != mlist.member.end(); p ++)
                        {
                            dvv = current.member[nid].velocity - p->velocity;
                            double delta_energy = abs(current.member[nid].energy - p->energy);
                            double delta_velocity = Mod(dvv);
                            /// 能量上加上1ev,防止粒子的能量为0, 速度上加1m/s,防止粒子完全静止
                            if(delta_energy / (current.member[nid].energy + 1e-19) <= delta_e && \
                                    (delta_velocity / (Mod(current.member[nid].velocity) + 1.0) < delta_v))
                            //if(delta_energy / member[nid].energy <= delta_e && delta_energy / p->energy <= delta_e && \
                                    (delta_velocity / Mod(member[nid].velocity) < delta_v && Mod(dvv) / \
                                        Mod(p->velocity) < delta_v))
                            {
                                current.member[nid].energy = (current.member[nid].energy * \
                                        current.member[nid].weight + p->energy * p->weight);
                                current.member[nid].velocity = (current.member[nid].velocity * \
                                        current.member[nid].weight + p->velocity * p->weight);
                                current.member[nid].position = (current.member[nid].position + p->position) * 0.5;
                                current.member[nid].weight += p->weight;
                                current.member[nid].energy /= current.member[nid].weight;
                                current.member[nid].velocity = current.member[nid].velocity / current.member[nid].weight;
                                /// 删除被merge的粒子
                                p = mlist.member.erase(p);
                                p --;
                            }
                        }
                        mlist.squeeze();
                        nid ++;
                    }
                    //cout<<" "<<member.size()<<endl;
                    /// 进行压缩空间
                }
            }
        }
    }
}




double __Simbox::get_field_energy()
{
    return fieldgroup.get_en();
}

double __Simbox::get_total_energy()
{
    double ret = 0;
    for(int i = 0; i < species; i ++) ret += get_sp_energy(i);
    return ret + fieldgroup.get_en();
}

template<typename T>
void __Simbox::loopPtcls(bool (*condition)(const __Ptcls& myptcl), const vector<double>& convec, \
        void (*userfunc)(const __Ptcls& myptcl, T& myhist), \
        const vector<double>& uservec, int sp, T& myhist)
{
  size_t xsize = cellgroup.member.size();
  size_t ysize = cellgroup.member[0].size();
  for (size_t i = _start; i < xsize - _end; i++) {
    for (size_t j = _start; j < ysize - _end; j++) {
      __Ptclslist& mylist = cellgroup.member[i][j].plist[sp];
      for (size_t k = 0; k < mylist.member.size(); k++) {
        __Ptcls& myptcl = mylist.member[k];
        if (condition(myptcl)) {
          userfunc(myptcl, myhist);
        }
      }
    }
  }
}
