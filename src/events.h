#ifndef __events
#define __events
#include"particles.h"
#include "physicalconstants.h"
#include"type.h"
#include "userdef.h"
#include <vector>
#include <algorithm>
#include "common.h"

#define tau_0 e_charge * e_charge / (6.0 * PI * epsilon_0 * e_mass * pow(c_light_speed, 3.0))
/// this is correct beenn checked
#define eta_pre 7.5559563e-19
#define etapre (e_charge * h_bar / (pow(e_mass, 3.0) * pow(c_light_speed, 4.0)))

#define coeff_eta sqrt(3.0 * lambdac / (2.0 * alpha_f * e_mass * pow(c_light_speed, 3)))
#define tau0 e_charge**2 / (6.0 * PI * epsilon_0 * e_mass * pow(c_light_speed, 3))
///说明：events调用具体的mc过程，然后对新粒子的od初始化和随机化
/// 在粒子中封装events，具体类型的sp调用不同的过程函数
/// 不能使用map，因为采用map时虽然产找方便，但变量的取值就出问题了。
/// 这个时外部接口,通过trigger,可以从这个对象中获得新的粒子，以及修改过的粒子

// 应该吧这两个函数放到common中。
/// return the index of param in matrix
//template<typename __Ty>
//int bs_search(const __Ty& param, const vector<__Ty>& matrix)
// give the low bound of para in the matrix
int bs_search(double& param, vector<double>& matrix)
{
    vector<double>::iterator locator;
    locator = lower_bound(matrix.begin(), matrix.end(), param);
    int ret = (locator - matrix.begin());
    return ret - 1;
}

double init_velocity(const double& lorentz_gamma)
{
    return c_light_speed * sqrt(1.0 - 1.0 / lorentz_gamma / lorentz_gamma);
}

void init_ptcls(__Ptcls& myptcl)
{
    myptcl.qed_factor = 0;
    myptcl.rod_factor = log(1.0 / (1.0 - random_generator()));
}


void reset_rod(__Ptcls& myptcl)
{
    myptcl.rod_factor = log(1.0 / (1.0 - random_generator()));
}

/// 得到该矢量方向的单位向量
template<typename __Ty>
__Vect3<__Ty> normalize(__Vect3<__Ty>& normalizer)
{
    return normalizer / Mod(normalizer);
}

///更新正负电子和光子的QED参数
void update_qed_factor(__Ptcls& particle)
{
    /***
    double gamma = 500.0;
    particle.efield = __Vect3<double>(1e15, 0, 0);
    particle.bfield = __Vect3<double>(0, 1e7, 0);
    particle.velocity = __Vect3<double>(0, 0, c_light_speed);
    //particle.velocity = __Vect3<double>(c_light_speed * sqrt(1.0 - 1.0 / pow(gamma, 2)), 0, 0);
    particle.lorentz_gamma = gamma;
    particle.energy = gamma * mc2;
    ***/ 
    __Vect3<double> norm_v = particle.velocity / Mod(particle.velocity);
    __Vect3<double> beta = particle.velocity / c_light_speed;
    switch(particle.species)
    {
        case 0:
        case 2:
            {
                /***
                __Vect3<double> partu = beta * particle.lorentz_gamma;
                double mod2 = (partu * partu > 1.0) ? partu * partu: 1.0;
                double udote = partu * particle.efield;
                __Vect3<double> fl = e_charge * (particle.efield - partu * (udote / mod2) + \
                        Cross(beta, particle.bfield) * c_light_speed);
                double i_e = tau_0 * particle.lorentz_gamma * particle.lorentz_gamma \
                             / e_mass * (fl * fl + pow(e_charge * beta * particle.efield / sqrt(mod2), 2.0));
                particle.qed_factor = coeff_eta * sqrt(i_e);
                ***/
                double part1 = particle.efield * beta;
                __Vect3<double> part2 = particle.efield + Cross(particle.velocity, particle.bfield);
                // mod of F_munu P^nu
                double fmnpn = sqrt(part2 * part2 - part1 * part1) * particle.lorentz_gamma \
                               * particle.mass * c_light_speed;
                particle.qed_factor = fmnpn * etapre;
            }
            break;
        case 3:
            {
                particle.qed_factor = 0.5 * particle.energy * iEcrit * imc2 * \
                                      Mod(particle.efield - norm_v * (norm_v * \
                     particle.efield) + Cross(norm_v, particle.bfield) * c_light_speed);

            }
            break;
        default:
            break;
    }
}



// 可能有2个表0,eta, 1, chi, 2 f, 3 h(eta), 4 T(chi), 5 chi for T, 6 eta for h
// 0 Pchi(eta, chi)  1, Pf(f, chi), 2: chi(eta, P)
// 返回值代表新的粒子个数
int synchrotron_rad(const double& dt, __Ptcls& parent, __Ptcls& child,\
        vector<vector<double> >& table, vector<vector<vector<double> > >& table_P)
{
    /// may not be right
    //double local_const = 6.1670031e19;
    double local_const = 1.56212e18;
    update_qed_factor(parent);
    /// 对于电子和正电子qed factor是ｅｔａ, 光子的是chi
    if(parent.qed_factor <= 1e-200) return 0;
    double temp_qed = log10(parent.qed_factor);
    /// 先得到eta的位置，利用eta的位置得到P，eat的位置处的chi
    int eta_index = bs_search(temp_qed, table[0]);
    int eta_index_h = bs_search(temp_qed, table[6]);

    if(eta_index < 0 || eta_index > table[0].size())
    {
        return 0;
    }
    double h_eta = table[3][eta_index_h];
    if(eta_index < table[3].size() - 1)
    {
        h_eta = h_eta + (table[3][eta_index_h + 1] - table[3][eta_index_h]) / (table[6][eta_index_h + 1]\
                - table[6][eta_index_h]) * (temp_qed - table[6][eta_index_h]);
    }
    
    parent.rod_factor -= dt * pow(10.0, h_eta) * \
                        parent.qed_factor / parent.lorentz_gamma * local_const;
    if(parent.rod_factor > 0)
    {
        return 0;
    }
    reset_rod(parent);
    double random_P = random_generator();
    // Ｐ是chi 和eta的函数
    int chi_index = bs_search(random_P, table_P[0][eta_index]);
    if( chi_index < 0 || chi_index > table_P[2][eta_index].size() - 1)
    {
        return 0;
    }
    double chi_f = table_P[2][eta_index][chi_index];
    if(chi_index < table_P[2][eta_index].size() - 1)
    {
        double p_grad = (table_P[2][eta_index][chi_index + 1] - \
                table_P[2][eta_index][chi_index]) / (table_P[0][eta_index][chi_index + 1] \
                - table_P[0][eta_index][chi_index]) * \
                (random_P - table_P[0][eta_index][chi_index]);
        chi_f = chi_f + p_grad;
    }
    chi_f = pow(10.0, chi_f);
    /// 通过从父粒子获取信息，因为很多东西共享
    //很明显ＩＤ这东西没法通用了
    child = parent;
    child.species = 3;
    child.charge = 0;
    child.mass = 0;
    double pold_e = parent.energy;
    child.energy = 2.0 * mc2 * chi_f * parent.lorentz_gamma / parent.qed_factor;
    if(child.energy > parent.energy) return 0;
    child.velocity = c_light_speed * normalize(parent.velocity);

    /// 这个地方计算正负电子的速度应该根据动量计算，一会儿再修改：
    /// p = (1 - hv / cp ) P
    double old_gamma = parent.lorentz_gamma;
    parent.energy = parent.energy - child.energy;
#if __Debug
    if(parent.energy < 0) 
    {
        cout<<" error, negtive electron or positron energy "<<endl;
        cout<<parent<<"\t"<<chi_f<<"\t"<<random_P<<"\t"<<endl;
        getchar();
    }
#endif
    parent.lorentz_gamma = parent.energy / mc2 + 1.0;
    parent.velocity = normalize(parent.velocity) * \
                      sqrt(pow(parent.lorentz_gamma, 2.0) - 1.0) * c_light_speed / \
                      parent.lorentz_gamma;
#if __Debug
    if(parent.energy < 0)
    {
        cout<<" parent negative energy "<<parent.energy<<endl;
        cout<<"parent qed "<<parent.qed_factor<<" chif "<<chi_f<<" parent old energy "<<pold_e<<" old gamma "<<old_gamma<<" child ek "<<child.energy<<endl;
        cout<<" rod factor "<<parent.rod_factor<<endl;
        cout<<" dtau "<<dt * pow(10.0, h_eta) * \
                        parent.qed_factor / parent.lorentz_gamma * local_const<<endl;
        getchar();
    }
    cout<<" energy "<<child.energy<<"\t"<<parent.energy<<endl;
    cout<<"chif = "<<chi_f<<" random p "<<random_P<<"\t eta index "<<eta_index<<" index "<<chi_index<<endl;
    if(std::isnan(parent.energy))
    {
        cout<<parent<<endl;
        cout<<" parent nan energy "<<endl;
        getchar();
    }
#endif
    init_ptcls(child);
    /// 需要设定光子能量的一个下限
    if(child.energy < ekmin)
    {
        return 0;
    }
    return 1;
}





/// tables1d 0: ek_ele,1: x = omega / ek
/// tables2d: 0 sigma_br(ek_ele, x), 1, sigma_bh(ek_photon, y)
int bremsstrahlung_rad(const double& dt, const double& density, \
        __Ptcls& parent, __Ptcls& child, vector<vector<double> >& tables1d, \
        vector<vector<vector<double> > >& tables2d)
{
    int energy_index = bs_search(parent.energy, tables1d[0]);
    if(energy_index < 0)
    {
        return 0;
    }
    /// offgrid 能量，采用区间端点概率决定用那个e
    int max_ind = tables1d[1].size() - 1;
    string type = "log";
    double sigma_total = interp_2d(tables2d[0], tables1d[0], 1, energy_index, max_ind, \
            parent.energy, type);
    
    double freq = Mod(parent.velocity) * density * sigma_total * dt;
    double probability = 1.0 - exp(-freq);
    double myrand = random_generator();
    if(myrand > probability)
    {
        return 0;
    }
#if __Debug
    cout<<"probaiblity\t"<<myrand<<"\t"<<probability<<endl;
#endif
    double sigma = sigma_total * random_generator();
    int ph_index = -1;
    while(ph_index < 0)
    {
        sigma = sigma_total * random_generator();
        ph_index = bs_search(sigma, tables2d[0][energy_index]);
#if __Debug
        cout<<"ph_index "<<ph_index<<" sigma "<<sigma<<"\t"\
            <<" max "<<sigma_total<<"\t sigmaph "<<\
            tables2d[0][energy_index][ph_index]<<"\t"\
            <<tables2d[0][energy_index][ph_index+1]<<"\t"
            <<tables2d[0][energy_index+1][ph_index]<<"\t"\
            <<tables2d[0][energy_index+1][ph_index+1]<<"\t"<<endl;
        cout<<"max id "<<max_ind<<endl;
        getchar();
#endif
    }
    child = parent;
    /// 对ph_index不在格点的sigma进行插值。
    double ek_ratio = 0;
    if(ph_index == tables2d[0][energy_index].size() - 1)
    {
        ek_ratio = tables1d[1][ph_index];
    }else
    {

        ek_ratio = tables1d[1][ph_index] + \
                      (tables1d[1][ph_index + 1] - tables1d[1][ph_index]) / \
                      (tables2d[0][energy_index][ph_index + 1] - \
                       tables2d[0][energy_index][ph_index]) * \
                       (sigma - tables2d[0][energy_index][ph_index]);
    }
    //cout<<"ek child is now "<<ek_ratio<<endl;
    child.energy *= ek_ratio;
    child.species = 3;
    child.charge = 0.0;
    child.mass = 0.0;
    child.velocity = c_light_speed * normalize(parent.velocity);
    update_qed_factor(child);
    init_ptcls(child);
    reset_rod(parent);
    /// 这个地方计算正负电子的速度应该根据动量计算，一会儿再修改：
    /// p = (1 - hv / cp ) P
    //这两项在boris　中更新
    double old_gamma = parent.lorentz_gamma;
    parent.energy = parent.energy - child.energy;
    parent.lorentz_gamma = parent.energy / mc2 + 1.0;
    //parent.old_vel = parent.velocity;
    parent.velocity = parent.velocity * old_gamma / parent.lorentz_gamma * (1.0 - child.energy / old_gamma / \
            parent.mass / c_light_speed / Mod(parent.velocity));
    //cout<<child.energy<<endl;
    if(child.energy < ekmin)
    {
        return 0;
    }
    return 1;

}
/***
double calc_angular(const __Vect3<double>& velocity)
{
    double ran1 = random_generator();
    double ran2 = random_generator();
    double ct = 2.0 * ran2 - 1.0;
    double beta = normalize(velocity) / c_light_speed;

***/

// 可能有2个表0,eta, 1chi, 2 f, 3 h(eta), 4 T(chi), 5, chiforT, 6, etaforh
// Pchi(eta, chi) Pf(chi, f), chi(eta, P)
// 返回值代表新的粒子个数

int bw_pair_create(const double& dt, __Ptcls& parent, __Ptcls& child0, __Ptcls& child1, \
        vector<vector<double> >& table, vector<vector<vector<double> > >& table_P)
{
    //parent.energy = 1.6559267762283087e-11;
    if(parent.energy < 2.0 * mc2)
    {
        return 0;
    }
    update_qed_factor(parent);
    if(parent.qed_factor <= 0) return 0;
    double temp_qed = log10(parent.qed_factor);
    int chi_index_T = bs_search(temp_qed, table[5]);
    if(chi_index_T < 0)
    {
        chi_index_T = 0;
        //return 0;
    }
    double t_chi = table[4][chi_index_T];
    if(chi_index_T < table[5].size() - 1)
    {
        t_chi += (table[4][chi_index_T + 1] - t_chi) / (table[5][chi_index_T + 1] \
                - table[5][chi_index_T]) * (temp_qed - table[5][chi_index_T]);
    }
    t_chi = pow(10.0, t_chi);
    /// 这里的系数先算成常数比较好
    /// this is correct
    parent.rod_factor -= alpha_fine * mc2 * c_light_speed /\
                        (lambda_compton * parent.energy) * parent.qed_factor *\
                        t_chi * dt;
    if(parent.rod_factor > 0)
    {
        return 0;
    }
    // Ｐ是chi 和eta的函数
    int chi_index = bs_search(temp_qed, table[1]);
    cout<<"will generate pair with chi index "<<chi_index<<endl;
    if(chi_index < 0)
    {
        return 0;
    }
    double random_P = random_generator();
    int f_index = bs_search(random_P, table_P[1][chi_index]);
    if( f_index < 0)
    {
        return 0;
    }
    double f_val = table[2][f_index];
    f_val = f_val + (table[2][f_index + 1] - table[2][f_index]) / \
            (table_P[1][chi_index][f_index + 1] - table_P[1][chi_index][f_index]) * \
            (random_P - table_P[1][chi_index][f_index]);
#if __Debug
    cout<<"parent rod "<<parent.rod_factor<<endl;
    cout<<" temp qed "<<temp_qed<<" in table "<<table[5][chi_index_T]<<endl;
    cout<<" chi "<<temp_qed<<" in table "<<table[1][chi_index]<<endl;
    cout<<"f index "<<table_P[1][chi_index][f_index]<<" random "<<random_P<<endl;
    cout<<" fval "<<f_val<<endl;
    getchar();
#endif
    /// 通过从父粒子获取信息，因为很多东西共享
    //很明显ＩＤ这东西没法通用了
    child0 = parent;
    child1 = parent;
    child0.species = 0;
    child1.species = 2;
    child0.mass = e_mass;
    child1.mass = e_mass;
    child0.charge = - e_charge;
    child1.charge = e_charge;
    /// 从Pf中查找f给出正电子的能量。
    child0.energy = (parent.energy - 2.0 * mc2) * (1.0 - f_val);
    child1.energy = (parent.energy - 2.0 * mc2) * f_val;
    if(child0.energy < 0|| child1.energy < 0)
    {
        cout<<" pair creating error, negative energy!"<<endl;
    }
    child0.lorentz_gamma = child0.energy / mc2 + 1.0;
    child1.lorentz_gamma = child1.energy / mc2 + 1.0;
    child0.velocity = normalize(parent.velocity) * \
                      sqrt(pow(child0.lorentz_gamma, 2.0) - 1.0) * \
                      c_light_speed / child0.lorentz_gamma;
    child1.velocity = normalize(parent.velocity) * \
                      sqrt(pow(child1.lorentz_gamma, 2.0) - 1.0) * \
                      c_light_speed / child1.lorentz_gamma;
    init_ptcls(child0);
    init_ptcls(child1);
#if __Debug
    cout<<" child0 "<<child0<<endl;
    cout<<" child1 "<<child1<<endl;
    cout<<" parent "<<parent<<endl;
    getchar();
#endif
    if(std::isnan(child0.energy * child1.energy)) \
        cout<<" nan electron or positron in bw pair "<<parent.energy<<"\t"<<f_val<<endl;
    return 2;
}


/// tables1d 0: ek,  1: x = omega / ek
/// tables2d: 0 sigma_br(ek_ele, x), 1, sigma_bh(ek_photon, y)
int bh_pair_create(const double& dt, const double& density, __Ptcls& parent, \
        __Ptcls& child0, __Ptcls& child1, vector<vector<double> >& tables1d, \
        vector<vector<vector<double> > >& tables2d)
{
    if(parent.energy < 2.0 * mc2)
    {
        return 0;
    }
    double ek_all_a = parent.energy;
    int ph_index = bs_search(parent.energy, tables1d[0]);
    //cout<<" ph f_index "<<ph_index<<endl;
    if(ph_index < 0)
    {
        return 0;
    }
    int max_ind = tables2d[0][0].size() - 1;
    //double sigma_interp = (log(tables2d[1][ph_index + 1][max_ind]) -  \
            log(tables2d[1][ph_index][max_ind])) / \
            (log(tables1d[1][ph_index + 1]) - log(tables1d[1][ph_index])) * \
            (log(parent.energy) - log(tables1d[1][ph_index])) + \
                          tables2d[1][ph_index][max_ind];
    /// 其实，最大截面与粒子的能量无关
    /***
    cout<<" ph f_index "<<ph_index<<endl;
    cout<<" parent ek "<<parent.energy<<" in table "<<tables1d[0][ph_index]<<endl;
    ***/
    double sigma_interp = tables2d[1][ph_index][max_ind];
    double freq = Mod(parent.velocity) * density * sigma_interp * dt;
    double probability = 1.0 - exp(-freq);
    //cout<<" probability is "<<probability<<endl;
    if(random_generator() > probability)
    {
        return 0;
    }
    /// 决定产生粒子的能量,这个地方给出的是正电子能量占光子能量比
    double ek_all = parent.energy;
    double sigma_new = sigma_interp * random_generator();
    int ek_index = bs_search(sigma_new, tables2d[1][ph_index]);
    if(ek_index < 0)
    {
        return 0;
    }
    child0 = parent;
    child1 = parent;
    double ratio = (tables1d[1][ek_index + 1] - tables1d[1][ek_index]) / \
                   (tables2d[1][ph_index][ek_index + 1] - tables2d[1][ph_index][ek_index]) * \
                   (sigma_new - tables2d[1][ph_index][ek_index]) + tables1d[1][ek_index];
    ek_all = parent.energy - 2.0 * mc2;
    child1.energy = ek_all * ratio;
    child0.energy = ek_all - child1.energy;
    child0.mass = e_mass;
    child1.mass = e_mass;
    child0.charge = - e_charge;
    child1.charge = e_charge;
    child0.species = 0;
    child1.species = 2;
    init_ptcls(child0);
    init_ptcls(child1);
    child0.lorentz_gamma = child0.energy / mc2 + 1.0;
    child1.lorentz_gamma = child1.energy / mc2 + 1.0;
    child1.velocity = normalize(parent.velocity) * init_velocity(child1.lorentz_gamma);
    child0.velocity = normalize(parent.velocity) * init_velocity(child0.lorentz_gamma);
    return 2;
}



#endif
