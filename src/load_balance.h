#include"denfunc.h"
#include<vector>
#include<valarray>
#include"common.h"
void calc_simbox(int ranksize, int rank, __Vect2<int>& cell_num,\
__Vect2<double>& boxmin, __Vect2<double>& boxmax, double dx, double dy)
{
    long pnum = 0;
    long pnums[x_cell];
    long offsets[ranksize + 1];
    vector<double> density(species_all);
    vector<Particles> spptcls;
    vector<int> ppc(species_all);
    init_sp(spptcls, ppc);
	int irank = 0;
    for(int i = 0; i < x_cell; i ++)
    {
        pnums[i] = 0;
        for(int j = 0; j < y_cell;  j ++)
        {
            __Vect2<double> pos(i * dx, j * dy);
            calc_density(pos, density);
            for(int sp = 0; sp < species_all; sp ++)
            {
                if (density[sp] > 1) pnums[i] += ppc[sp];
		density[sp] = 0;
            }
        }
	pnums[i] += y_cell * 3;
    }
    valarray<long> psumarray (pnums, x_cell);
    long ptcls_per_rank = psumarray.sum() / ranksize;
    offsets[0] = 0;
    for(int i = 0; i < x_cell; i ++)
    {
        pnum += pnums[i];
        if(pnum >= ptcls_per_rank * (irank + 1)) 
        {
            offsets[irank+1] = i;
	    irank += 1;
        }
    }
    offsets[ranksize] = x_cell;
    /***
    if(rank == 0)
    {
    	for(int i = 0; i < ranksize + 1; i ++) cout<<offsets[i]<<endl;
        for(int i = 0; i < x_cell; i ++) cout<<i<<"\t"<<pnums[i]<<endl;
    }
    ***/
    cell_num.member[0] = offsets[rank + 1] - offsets[rank] + _start + _end;
    cell_num.member[1] = y_cell + _start + _end;

    boxmin.member[0] = dx * (offsets[rank] - _start);
    boxmin.member[1] = -_start * dy;

    boxmax.member[0] = dx * (offsets[rank + 1] + _end);
    boxmax.member[1] = y_len + _end * dy; 
}

