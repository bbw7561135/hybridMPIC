#ifndef __common
#define __common
#include"physicalconstants.h"
#include<cmath>
#include "particles.h"
/// 根据给定的 激光 omega，返回等离子体临界密度
inline double ncrit(const double& omega)
{
    return (e_mass * epsilon_0 * omega * omega / e_charge / e_charge);
}

/// 根据给定的等离子体密度，返回等离子体频率
inline double omegape(const double& density)
{
    return sqrt(density * e_charge * e_charge / e_mass /epsilon_0);
}
///根据给定的使用omega归一化的矢势，返回激光强度

inline double get_intensity(const double& scaled_A, const double& omega)
{
    /// wavelength in m
    double lambda = c_light_speed / omega * 2.0 * PI;
    ///
    double ingauss = (scaled_A * scaled_A) * 1.37 * 1e18 * (um / lambda) * (um / lambda);
    return ingauss * w_cm2;
}
/***
double get_intensity(const double& scaled_A, const double& omega)
{
    return (e_mass * c_light_speed * omega * scaled_A / e_charge * 1e-3);
}
***/
/// 粒子数据结构体，用于MPI封装
struct __Pdata{
    /// 粒子种类
    int species;
    /// 粒子的id和weight
    long id;
    /// 粒子的速度，位置和能量
    double vel_pos_ek_rod_wt_fp[11];
};

/// 粒子种类信息结构体，用于MPI数据传输后的数据恢复
struct __Spinfo{
    /// 粒子种类
    int sp;
    /// 粒子质量
    double mass;
    /// 粒子电荷
    double charge;
};


/// 将粒子信息拷贝到mypdata结构体,用于MPI传输
void copy_to_pdata(__Ptcls& ptcl, __Pdata& mypdata, const int& myrank)
{
    mypdata.species = ptcl.species;
    mypdata.id = ptcl.id;
    //cout<<myrank<<" sending id "<<ptcl.id<<" position "<<ptcl.position<<endl;
    mypdata.vel_pos_ek_rod_wt_fp[0] = ptcl.velocity.member[0];
    mypdata.vel_pos_ek_rod_wt_fp[1] = ptcl.velocity.member[1];
    mypdata.vel_pos_ek_rod_wt_fp[2] = ptcl.velocity.member[2];
    mypdata.vel_pos_ek_rod_wt_fp[3] = ptcl.position.member[0];
    mypdata.vel_pos_ek_rod_wt_fp[4] = ptcl.position.member[1];
    mypdata.vel_pos_ek_rod_wt_fp[5] = ptcl.energy;
    mypdata.vel_pos_ek_rod_wt_fp[6] = ptcl.lorentz_gamma;
    mypdata.vel_pos_ek_rod_wt_fp[7] = ptcl.mass;
    mypdata.vel_pos_ek_rod_wt_fp[8] = ptcl.charge;
    mypdata.vel_pos_ek_rod_wt_fp[9] = ptcl.rod_factor;
    mypdata.vel_pos_ek_rod_wt_fp[10] = ptcl.weight;
    //mypdata.vel_pos_ek_rod_wt_fp[11] = ptcl.free_path;

}

/// 拷贝结构体粒子到粒子信息
void copy_to_ptcl(__Pdata& pdata, __Ptcls& ptcl, const int& myrank)
{


    ptcl.species = pdata.species;
    ptcl.id = pdata.id;
    ptcl.velocity.member[0] = pdata.vel_pos_ek_rod_wt_fp[0];
    ptcl.velocity.member[1] = pdata.vel_pos_ek_rod_wt_fp[1];
    ptcl.velocity.member[2] = pdata.vel_pos_ek_rod_wt_fp[2];
    ptcl.position.member[0] = pdata.vel_pos_ek_rod_wt_fp[3];
    ptcl.position.member[1] = pdata.vel_pos_ek_rod_wt_fp[4];
    ptcl.energy = pdata.vel_pos_ek_rod_wt_fp[5];
    ptcl.lorentz_gamma = pdata.vel_pos_ek_rod_wt_fp[6];
    ptcl.mass = pdata.vel_pos_ek_rod_wt_fp[7];
    ptcl.charge = pdata.vel_pos_ek_rod_wt_fp[8];
    ptcl.rod_factor = pdata.vel_pos_ek_rod_wt_fp[9];
    ptcl.weight = pdata.vel_pos_ek_rod_wt_fp[10];
    //ptcl.free_path = pdata.vel_pos_ek_rod_wt_fp[11];
    //cout<<myrank<<" receiving id "<<ptcl.id<<" position "<<ptcl.position<<endl;
}

/// 恢复粒子的种类信息根据spinfo
void set_spinfo(__Ptcls& ptcls, __Spinfo& spinfo)
{
    ptcls.charge = spinfo.charge;
    ptcls.mass = spinfo.mass;
}

/// 定义Pline为一个结构体数组,MPI传输使用
typedef std::vector<__Pdata> __Pline;



inline void shift_data_tags(long data_tags[])
{
    for(int i = 0; i < 16; i ++)
    {
        data_tags[i] += 100;
    }
}


inline double calc_gamma(const __Vect3<double>& vel)
{
    return 1.0 / sqrt(1.0 - (vel * vel) / c_light_speed / c_light_speed);
}


inline void grid_wxy(const double& fracx, const double& fracy, double wx[], double wy[])
{
    //double fx, fy;
    double cfx2 = fracx * fracx;
    double cfy2 = fracy * fracy;
        wx[0] = 0;
        wx[1] = 0.25 + cfx2 + fracx;
        wx[2] = 1.5 - 2.0 * cfx2;
        wx[3] = 0.25 + cfx2 - fracx;
        wx[4] = 0;

        wy[0] = 0;
        wy[1] = 0.25 + cfy2 + fracy;
        wy[2] = 1.5 - 2.0 * cfy2;
        wy[3] = 0.25 + cfy2 - fracy;
        wy[4] = 0;
}

inline void grid_dwxy(const double& fracx, const double& fracy, double wx[], double wy[], const int& dcx, const int& dcy)
{
    //double fx, fy;
    double cfx2 = fracx * fracx;
    double cfy2 = fracy * fracy;
        wx[dcx - 1 + 2] = 0.25 + cfx2 + fracx;
        wx[dcx     + 2] = 1.5 - 2.0 * cfx2;
        wx[dcx + 1 + 2] = 0.25 + cfx2 - fracx;

        wy[dcy - 1 + 2] = 0.25 + cfy2 + fracy;
        wy[dcy     + 2] = 1.5 - 2.0 * cfy2;
        wy[dcy + 1 + 2] = 0.25 + cfy2 - fracy;
}

inline void get_wxy(double fracx, double fracy, double wx[], double wy[])
{
    double cfx2 = fracx * fracx;
    double cfy2 = fracy * fracy;
        wx[0] = 0.25 + cfx2 + fracx;
        wx[1] = 1.5 - 2.0 * cfx2;
        wx[2] = 0.25 + cfx2 - fracx;

        wy[0] = 0.25 + cfy2 + fracy;
        wy[1] = 1.5 - 2.0 * cfy2;
        wy[2] = 0.25 + cfy2 - fracy;


        // sum in each direction is 2, so product is 4, should be multiply _frac2

}

double interp_1d(vector<double>& result_1d,\
        vector<double>& input_1d,\
        const int& lower_index, \
        const double& in,
        const string type)
{
    double res = result_1d[lower_index];
    if(lower_index == result_1d.size() - 1 ||\
            lower_index == input_1d.size() - 1)
    {
        return res;
    }
    double& y1 = result_1d[lower_index + 1], y0 = result_1d[lower_index];
    double& x1 = input_1d[lower_index + 1], x0 = input_1d[lower_index];
    if (type == "log")
    {
        res = log(res);
        res += (log(y1 / y0)) / (log(x1 / x0)) * (log(in / x0));
        res = exp(res);
    }
    else
    {
        res += (y1 - y0) / (x1 - x0) * (in - x0);
    }
    return res;
}





double interp_2d(vector<vector<double> >& result_2d,\
        vector<double>& input_1d,\
        int direction,\
        const int& lower_index0, \
        const int& lower_index1, \
        const double& in,
        const string type)
{
    double res = result_2d[lower_index0][lower_index1];
    // row can use the 1d case
    if(direction == 0)
    {
        res = interp_1d(result_2d[lower_index0], input_1d, \
                lower_index1, in, type);
        return res;
    }
    if (lower_index1 == result_2d[lower_index0].size() - 1 || \
            lower_index1 == input_1d.size() - 1) return res;
    cout<<lower_index0<<"\t"<<lower_index1<<endl;
    // col interp need recalculate
    double& y1 = result_2d[lower_index0 + 1][lower_index1], \
                 y0 = result_2d[lower_index0][lower_index1];
    double& x1 = input_1d[lower_index0 + 1], x0 = input_1d[lower_index1];
    if (type == "log")
    {
        res = log(res);
        res += (log(y1 / y0)) / (log(x1 / x0)) * (log(in / x0));
        res = exp(res);
    }
    else
    {
        res += (y1 - y0) / (x1 - x0) * (in - x0);
    }
    return res;
}


string getPrefix(const string& fname, const int& rank, const int& n)
{
  string filename = fname;
  stringstream tempstream;
  string tempint;
  if(n < 10){
      tempstream<<"0000";
  }
  else if(n < 100){
      tempstream<<"000";
  }else if(n < 1000){
      tempstream<<"00";
  }else if(n < 10000){
      tempstream<<"0";
  }
  tempstream<<n;
  tempstream<<"_rank_";
  tempstream<<rank;
  tempstream>>tempint;
  filename += tempint;
  return filename;
}


template<typename T>
double sign(T v)
{
    if(v > 0) return 1;
    if(v < 0) return -1;
    return 0;
}

#define _frac2 0.25
#define _xmin -1
#define _ymin -1
#define _xmax 1
#define _ymax 1


#define mpi_from_left 2
#define mpi_from_right 2
#define mpi_to_left 2
#define mpi_to_right 2
#define _start 2
#define _end 2
#define _jstart 3
#define _jend 3
#define adder 1

#define gst_start 2
#define gst_end 2


#endif
