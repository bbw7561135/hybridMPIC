/*
 * __Simbox.h
 *
 *  Created on:2015-1-27
 *      Author: movin
 *  This file will define the simulation box class
 */
#ifndef ____Simbox
#define ____Simbox
#include <iostream>
#include <string>
#include "fields.h"
//#include "particles.h"
#include "cellinf.h"
#include "common.h"
#include "type.h"
#include "userdef.h"
#include "arrays.hxx"
#include "hist.hxx"
//#include "dist_class.hxx"
///初始化一个模拟盒子，这个模拟盒子包含cell信息，粒子信息，场信息。粒子种类信息也在这里。
//class __Simbox:distData{
class __Simbox{
public:
    ///这个名字将会输出数据文件名的头部
 	string simname;
    ///所有总模拟盒子的边界
    __Vect2<double> basexymax;
    ///所有总模拟盒子的边界
    __Vect2<double> basexymin;
    ///盒子的网格标号最大和最小
	__Vect2<int> xygridmax;
    ///盒子的网格标号最大和最小
	__Vect2<int> xygridmin;
    ///盒子中网格的数目
	__Vect2<int> xycell;
    ///盒子x的范围
	__Vect2<double> xrange;
    ///盒子y的范围
	__Vect2<double> yrange;
    ///盒子，xy最大值
	__Vect2<double> xymax;
    ///盒子xy最小值
	__Vect2<double> xymin;
    ///模拟时间开始和结束点
    __Vect2<double> time;
    ///这个position本来是准备在2d MPI 中记录box位置的，在一维domain分解中没有必要
    __Vect2<int> position;
    ///网格长宽
    __Vect2<double> dxy;
    ///时间步数，不能自己指定，而是根据给定的网格宽度和时间长度、Corant 条件算出。
	int t_steps;
    ///粒子种类，4为默认，但可以不给粒子小于4时，最后面的粒子种类将不会启用 0：电子，1：离子，2：正电子，3：光子
	int species;
    /// speices struct info
    vector<Particles> spptcl;
    ///dt 根据网格宽度和Corant 条件算出
	double delta_t;
    ///代表当前box初始化时粒子id最大值
	int maxid;
    ///种类数组，当数组中对应位数为0时，该种类粒子不启用，为1时启用
    vector<int> ppc;
    ///当flag = 0时，模拟不会启动，因此必须设置该位
    bool flag; // 判断是否有粒子
    ///电磁场边界类型，为下、上、左、右的顺序，0：反射边界，1：吸收边界，2：周期
    int bd_type[4];
    ///damplen 开始的网格位置，同样为下、上、左、右
    int startindex[4];
    ///damplen 存储场的吸收边界情况下四个边界的damp 网格长度， 顺序为下、上、左、右
    int damplen[4];
    /// 单个网格单种粒子最大数目
    int max_ppc;
    /// 允许合并粒子的能量差异
    double delta_e;
    /// 允许合并粒子的速度差异 ratio
    double delta_v;
    /// 0 不需要开启，１需要开启, 顺序0-syn, 1-brem, 2-bwpair, 3-bhpair
    int monte_flag[4];
    /// 可能有2个表0,eta, 1, chi, 2 f, 3 h(eta), 4 T(chi), 5 chi for T, 6 eta for h
    vector<vector<double> > tables1d;
    /// 0 则开启电磁相互作用, 1 不开启, 默认开启
    int disable_em;
    /// 2d 目前只保存P(eta, kappa), P(f, kappa)
    vector<vector<vector<double> > > tables2d;
    /// 保存韧致辐射和ｂｈ过程的能量，以及能量比值
    vector<vector<double> > bh_tables1d;
    /// 保存韧致辐射, bh截面
    vector<vector<vector<double> > > bh_tables2d;
    ///存储越界粒子，当前box中所有越过母网格的粒子，是个vector，包含所有类别的粒子
    vector<vector<__Ptcls> >crossing_bd_ptcls;
    /// filenames1d, filenames2d
    vector<string> filenames1d;
    vector<string> filenames2d;
    ///当前box中的所有网格的二维数组，通过指标可以直接访问
    __Cellgroup cellgroup;
    ///当前box中所有grid上的场，是fields的一个二维数组
    __Fieldgroup fieldgroup;
    long size(const int& sp);
    /// 高阶电磁场插值到粒子
    void weight_to_ptcls(const int& rank);
	__Simbox(string& simuname, \
			__Vect2<int> xygridmin, __Vect2<int> xygridmax,\
            __Vect2<int> xycell, __Vect2<double> xrange, \
			__Vect2<double> yrange, __Vect2<double> time,\
			int& tsteps, double& deltat, const int& species, const vector<Particles>& spptcl, int maxid, \
            const vector<int>& ppcref, __Vect2<int> positionref, int disable_em = 0):simname(simuname),\
			xygridmin(xygridmin), xygridmax(xygridmax), xycell(xycell), xrange(xrange),\
            yrange(yrange), time(time), t_steps(tsteps), delta_t(deltat), position(positionref)\
            , disable_em(disable_em), maxid(maxid), species(species)
            {
                xymin.member[0] = xrange.member[0];
                xymin.member[1] = yrange.member[0];
                xymax.member[0] = xrange.member[1];
                xymax.member[1] = yrange.member[1];
                dxy.member[0] = (xrange.member[1] - xrange.member[0]) /xycell.member[0];
                dxy.member[1] = (yrange.member[1] - yrange.member[0]) /xycell.member[1];
                this->spptcl = spptcl;
                ppc = ppcref;
            };
    __Simbox(){};
    ///利用构造函数提供的数据进行初始化，初始化cell，粒子，和场
    void init(string fname, int myrank);
    /// 0 代表读取成功
    int read_tables1d();
    /// 0 代表读取成功
    int read_tables2d();
    ///通过这个函数得到某一左边，sp类粒子的密度，从而进行初始化
    double get_density(const __Vect2<double>& xycoords, int sp);
    ///对越界粒子进行处理
    void migration(const int& myrank);
    ///从坐标到网格的转换
    __Vect2<int> coords_to_cell(__Ptcls& ptcl, int myrank);
    //__Vect2<int> coords_to_cell(const __Vect2<double>& xycoords);
    ///场拷贝到网格
    void field_copy_to_cell();
    ///网格的流拷贝到场
    void cell_copy_to_field(int domain, const int& myrank);
    ///进行演化
    void advancest1(const int& rank, const int& step);
    void advancest2(const int& rank, const int& step);
    void advancest3(const int& rank, const int& step);
    void advancest4(const int& rank, const int& n);
    void advancest5(const int& rank, const int& step);
    ///数据dump，目前没用到
    void dump();
    ///得到当前时间点sp类粒子的总数目
    int get_sp_num(int sp);
    ///初始化flag，必须调用
    void init_flag();
    ///设置场的边界条件
    void set_field_boundary(const int bd_typer[], const int startindexr[], const int damplenr[]);
    ///拷贝越界粒子数据到MPI数据
    void copy_to_pline(__Pline& plinel, __Pline& pliner, const int direction, const int& myrank);
    ///拷贝ghostcell场数据到MPI数据
    void copy_to_fline(__Fline& flinel, __Fline& fliner, const int direction, const int& myrank);
    ///拷贝ghostcell数据到MPI数据
    void copy_to_jline(__Jline& jlinel, __Jline& jliner, const int direction, const int& myrank);
    ///拷贝MPI数据到边界场
    void fline_copy_to(__Fline& flinel, __Fline& fliner, const int direction, const int& myrank, int fsp);
    ///拷贝MPI数据到当前越界粒子束中
    void pline_copy_to(__Pline& plinel, __Pline& pliner, const int direction, const int& myrank);
    ///拷贝MPI网格数据到当前cellgroup数据中
    void jline_copy_to(__Jline& jlinel, __Jline& jliner, const int direction, const int& myrank);
    /// 设置输入数据文件名
    void set_data_file(const vector<string>& fnames1d, const vector<string>& fnames2d);
    /// 拷贝ｆｌａｇ
    void set_monte_carlo(int monte_flags[])
    {
        memcpy(monte_flag, monte_flags, sizeof(int) * 4);
    }
    ///对内存进行优化
    void squeeze();
    /// 对sp类型的粒子进行合并
    void merge(const int& sp){cellgroup.merge_ptcls(max_ppc, delta_e, delta_v, sp); }
    void wide_merge(const int& sp, const int& cellnum);
    // 检查crossing 的空间大小
    long cbp_num();
	~__Simbox(){}
    double get_sp_energy(int sp);
    double get_field_energy();
    double get_total_energy();
    // 得到角分布
    template<typename T>
    void loopPtcls(bool (*condition)(const __Ptcls& myptcl), const vector<double>& convec, \
            void (*userfunc)(const __Ptcls& myptcl, T& myhist), const vector<double>& uservec, int sp, T& myhist);
    void userPreLoop(string& prefix, int rank = 0);
    void userStep(string& prefix);
};






#endif
