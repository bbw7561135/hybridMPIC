#ifndef __FOPENED
#define __FOPENED
#include<fstream>
#include<vector>
class mFileManager{
public:
  mFileManager(){}
  void registers(std::ofstream* reg) {
    pool.push_back(reg);
  }
  ~mFileManager(){
    for(auto ptr: pool) ptr->close();
    pool.clear();
    pool.shrink_to_fit();
  }

protected:
  std::vector<std::ofstream*> pool;
};
#endif
