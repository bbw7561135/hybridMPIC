#ifndef __constants
#define __constants
#define NMAX 65535
#define c_light_speed 299792458.0
#define PMINIT 500
#define epsilon_0 8.854187187e-12
#define mu_0 1.256637061e-6
#define PI 3.1415926535897932
#define COMP_SET_CONST
#define e_mass 9.10938291e-31
#define e_charge 1.602176565e-19
#define eV e_charge
#define KeV (1e3 * eV)
#define MeV (1e6 * eV)
#define h_plank 6.62606957e-34
#define h_bar (h_plank / (2.0 * PI))
#define sigma_e 10000
#define sigma_i 100000
#define sigma_ei 10000
#define sigma_p 10000
#define sigma_ep 10000
#define E_crit 1.323285417001326061279735961512150e18
#define mc2 8.187105065459161e-14
#define um 1e-6
#define w_cm2 1e4
#define alpha_fine (1/137.0)
#define alpha_f (1/137.0)
#define lambda_compton (h_bar / e_mass / c_light_speed)
#define lambdac (h_plank / e_mass / c_light_speed)
#define mc e_mass * c_light_speed
#define iEcrit  1.0 / E_crit
#define imc2 1.0 / mc2
//#define noboris
#endif
