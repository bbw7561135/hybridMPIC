#!/bin/python
import numpy as np
import matplotlib.pyplot as plt
#from matplotlib.ticker import FormatStrFormatter
#sci = FormatStrFormatter('%.2f')
def mysubplots(abbrev, fname, figabbrev, tdict):
    data = np.loadtxt(abbrev + fname)
    fname = fname.split('.')[0]
    series = int(fname.split('_')[1])
    for i in np.arange(data.shape[0]):
        for j in np.arange(data.shape[1]):
            if data[i, j] < 1e-200 and data[i, j] > -1e-200:
                data[i, j] = 0.0
    data = np.log(data + 1.0) / np.log(10.0)
    dim = data.shape[0]
    dim2 = data.shape[1]
    plt.figure(figsize=(12, 8))
    f, ax = plt.subplots(2, 2)
    if len(tdict) > 0:
        f.suptitle(fname + " at %.2e sec" %tdict[series])
    else:
        f.suptitle(fname)
    ax[0, 0].plot(data[:, int(dim2 / 4)])
    ax[0, 0].set_title('xgrid %d' %(int(dim2/4)))
    #ax[0, 0].yaxis.set_major_formatter(sci)
    ax[0, 1].plot(data[:, int(dim2 / 2)])
    ax[0, 1].set_title('xgrid %d' %(int(dim2/2)))
    #ax[0, 1].yaxis.set_major_formatter(sci)
    ax[1, 0].plot(data[:, int(dim2 * 3 / 4)])
    ax[1, 0].set_title('xgrid %d' %(int(dim2 * 3 / 4)))
    #ax[1, 0].yaxis.set_major_formatter(sci)
    ax[1, 1].plot(data[int(dim / 2), :])
    ax[1, 1].set_title("ygrid %d" %(int(dim / 2)))
    #ax[1, 1].yaxis.set_major_formatter(sci)
    plt.savefig(figabbrev + fname + "_subplots.png")
    plt.clf()
    plt.close("all")
    plt.figure(figsize=(10, 10))
    plt.imshow(data)
    plt.colorbar(shrink = .5)
    if len(tdict) > 0:
        plt.title(fname + " at %.2e sec in 10^" %(tdict[series]))
    else:
        plt.title(fname + " in 10^")
    plt.xlabel('x')
    plt.ylabel('y in 10^')
    plt.savefig(figabbrev + fname + ".png")
    plt.clf()
    plt.close("all")
