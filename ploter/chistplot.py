import numpy as np
import matplotlib.pyplot as plt
MeV = 1.6e-13
bins = 1000
ycenter = 4e-6
def plots(abbrev, ff, figabbrev, tdict):
    data = np.loadtxt(abbrev + "/" + ff)[:, -4]
    res = np.mgrid[data.min(): data.max(): bins * 1j]
    dd = res[1] - res[0]
    print dd
    if dd < 10e-30:
        return

    wt = np.loadtxt(abbrev + "/" + ff)[:, -5]
    yy = np.abs(np.loadtxt(abbrev + "/" + ff)[:, 2] - ycenter)
    series = int(ff.split("_")[1])
    cnt = np.zeros(res.shape)
    for i in np.arange(data.shape[0]):
        index = int(data[i] / dd) - 1
        #print "pdata and min max ", pdata, res[0], res[1], data.max(), index
        if(index > bins - 1):
            index = bins - 1
        if(index <= 0):
            index = 0
        cnt[index] = cnt[index] + wt[i] * yy[i] * np.pi
    species = ff.split('_')[2]
    print "count over"
    if "detect" in ff:
        species = ff.split("_")[2] + "_" + ff.split("_")[3]
    species = species.split('.')[0]
    #histe = plt.hist(data / MeV, bins)
    #plt.plot(histe[1][1:], histe[0], 'r-')
    plt.plot(res / MeV, cnt)
    plt.xlabel("Energy $\epsilon$ in MeV")
    plt.ylabel("Counts")
    if len(tdict) > 0:
        plt.title("spectra of "+ species + " at %.2e sec" %(tdict[series]))
    else:
        plt.title("spectra of "+ species)
    plt.savefig(figabbrev + ff.split(".")[0] + ".png")
    print 'figure ', figabbrev + ff.split(".")[0] + ".png" + "has been generated "
    plt.close()


