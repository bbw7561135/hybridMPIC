#!/bin/python
import numpy as np
import matplotlib.pyplot as plt
#from matplotlib.ticker import FormatStrFormatter
#sci = FormatStrFormatter('%.2f')
mev = 1.6e-13
def mysubplots(abbrev, fname, figabbrev, tdict, info):
    data = np.loadtxt(abbrev + fname)
    info = info * 1e6
    print data.shape
    ycell = data.shape[0]
    xcell = data.shape[1]
    if(xcell != 2):
        x = np.mgrid[info[0]:info[2]:xcell*1j]
        y = np.mgrid[info[1]:info[3]:ycell*1j]
        extents = [info[0], info[2], info[1], info[3]]
        fname = fname.split('.')[0]
        series = int(fname.split('_')[1])
        for i in np.arange(data.shape[0]):
            for j in np.arange(data.shape[1]):
                if data[i, j] < 1e-200 and data[i, j] > -1e-200:
                    data[i, j] = 0.0
        dim = data.shape[0]
        dim2 = data.shape[1]
        print dim, dim2, x.shape, y.shape
        plt.figure(figsize=(12, 8))
        f, ax = plt.subplots(2, 2)
        if len(tdict) > 0:
            f.suptitle(fname + " at %.2e sec" %tdict[series])
        else:
            f.suptitle(fname)
        ax[0, 0].plot(y, data[:, int(dim2 / 4)])
        ax[0, 0].set_title('xgrid %d' %(int(dim2/4)))
        #ax[0, 0].yaxis.set_major_formatter(sci)
        ax[0, 1].plot(y, data[:, int(dim2 / 2)])
        ax[0, 1].set_title('xgrid %d' %(int(dim2/2)))
        #ax[0, 1].yaxis.set_major_formatter(sci)
        ax[1, 0].plot(y, data[:, int(dim2 * 3 / 4)])
        ax[1, 0].set_title('xgrid %d' %(int(dim2 * 3 / 4)))
        #ax[1, 0].yaxis.set_major_formatter(sci)
        ax[1, 1].plot(x, data[int(dim / 2), :])
        ax[1, 1].set_title("ygrid %d" %(int(dim / 2)))
        #ax[1, 1].yaxis.set_major_formatter(sci)
        plt.savefig(figabbrev + fname + "_subplots.png")
        plt.clf()
        plt.close("all")
        f, ax = plt.subplots(1, 1, figsize=(10, 10))
        im = ax.imshow(data, extent = extents)
        if len(tdict) > 0:
            ax.set_title(fname + " at %.2e sec" %(tdict[series]))
        else:
            ax.set_title(fname)
        ax.set_xlabel('x')
        ax.set_ylabel('y')
        cbar_ax = f.add_axes([0.92, 0.3, 0.02, 0.4])
        mybar = f.colorbar(im, cax=cbar_ax, norm = True)
        mybar.set_label("a.u.", rotation = 0, y = 1.05, labelpad = -0.0)
        plt.savefig(figabbrev + fname + ".png", bbox_inches = 'tight')
        plt.clf()
        plt.close("all")
    else:
        plt.plot(data[:, 0] / mev, data[:, 1], linewidth = 3)
        fname = fname.split('.')[0]
        series = int(fname.split('_')[1])
        if len(tdict) > 0:
            plt.title("spectra " + fname + " at %.2e sec" %tdict[series])
        else:
            plt.title("spectra " + fname)
        plt.xlabel("E_k in MeV")
        plt.ylabel("dN/dE")
        if np.sum(data[:, 1]) > 0:
            plt.yscale("log")
        plt.savefig(figabbrev + fname + ".png")
        plt.close()
        plt.clf()
