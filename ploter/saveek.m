eknames = dir('*rank*ekout.txt');
data = 0;
for i = 1:length(eknames)
    thisname = eknames(i).name
    temp = load(thisname, '-ascii');
    size(temp) == size(data)
    if i == 1
        data = temp;
    else
        if sum(size(temp) == size(data)) > 0
            data(:, 2:end) = data(:, 2:end) + temp(:, 2:end)
        end
    end
end
save 'ekout.txt' data;
plot(data(:, 1), data(:, 2))
hold on
plot(data(:, 1), data(:, 3))
plot(data(:, 1), data(:, 4))
