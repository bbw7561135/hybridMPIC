#!/usr/bin/python
import unequal as sp
import os
import numpy as np
progname = "test"
rootdir = "./../data_" + progname + "/"
prefix = progname + "_"
info = rootdir + prefix + "info.txt"
info = np.loadtxt(info)
fname = os.listdir(rootdir)
abbrev = rootdir
figabbrev = "./../fig_" + progname + "/"
tdict = {}
for ff in fname:
    if "time" in ff:
        timer = np.loadtxt(rootdir + ff)
        for i in np.arange(timer.shape[0]):
            tdict[int(timer[i][0])] = timer[i][1]
for ff in fname:
    if ".txt" in ff and ".png" not in ff and "rank" not in ff and "time" not in ff and "info" not in ff:
        print ff, "is plotting"
        sp.mysubplots(abbrev, ff, figabbrev, tdict, info)

