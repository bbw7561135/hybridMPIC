#!/usr/bin/python
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import os
os.mkdir("./fig")
os.mkdir("./fig/ex")
os.mkdir("./fig/ey")
os.mkdir("./fig/ez")
os.mkdir("./fig/bx")
os.mkdir("./fig/by")
os.mkdir("./fig/bz")
os.mkdir("./fig/jx")
os.mkdir("./fig/jy")
os.mkdir("./fig/jz")
os.mkdir("./fig/rho")
os.mkdir("./fig/ele")
os.mkdir("./fig/ion")

flist = os.listdir('./data')
pwd = os.getcwd()
prefix = "./fig/"
for fname in flist:
    if ".txt" in fname:
        print fname
        ifname = "./data/" + fname
        if 'ey' in fname:
            plt.figure(figsize=(12, 10))
            plt.imshow(np.loadtxt(ifname))
            plt.title("E_y")
            plt.xlabel("x")
            plt.ylabel("y")
            plt.colorbar(shrink = .5)
            plt.savefig(prefix + "ey/" + fname  + ".png")
        if 'ez' in fname:
            plt.figure(figsize=(12, 10))
            plt.imshow(np.loadtxt(ifname))
            plt.title("E_z")
            plt.xlabel("x")
            plt.ylabel("y")
            plt.colorbar(shrink = .5)
            plt.savefig(prefix + "ez/" + fname  + ".png")
            plt.close()
        if 'ex' in fname:
            plt.figure(figsize=(12, 10))
            plt.imshow(np.loadtxt(ifname))
            plt.title("E_x")
            plt.xlabel("x")
            plt.ylabel("y")
            plt.colorbar(shrink = .5)
            plt.savefig(prefix + "ex/" + fname  + ".png")
            plt.close()
        if 'bx' in fname:
            plt.figure(figsize=(12, 10))
            plt.imshow(np.loadtxt(ifname))
            plt.title("B_x")
            plt.xlabel("x")
            plt.ylabel("y")
            plt.colorbar(shrink = .5)
            plt.savefig(prefix + "bx/" + fname  + ".png")
            plt.close()
        if 'by' in fname:
            plt.figure(figsize=(12, 10))
            plt.imshow(np.loadtxt(ifname))
            plt.title("B_y")
            plt.xlabel("x")
            plt.ylabel("y")
            plt.colorbar(shrink = .5)
            plt.savefig(prefix + "by/" + fname  + ".png")
            plt.close()
        if 'bz' in fname:
            plt.figure(figsize=(12, 10))
            plt.imshow(np.loadtxt(ifname))
            plt.title("B_z")
            plt.xlabel("x")
            plt.ylabel("y")
            plt.colorbar(shrink = .5)
            plt.savefig(prefix + "bz/" + fname  + ".png")
            plt.close()
        if 'jx' in fname:
            plt.figure(figsize=(12, 10))
            plt.imshow(np.loadtxt(ifname))
            plt.title("J_x")
            plt.xlabel("x")
            plt.ylabel("y")
            plt.colorbar(shrink = .5)
            plt.savefig(prefix + "jx/" + fname  + ".png")
            plt.close()
        if 'jy' in fname:
            plt.figure(figsize=(12, 10))
            plt.imshow(np.loadtxt(ifname))
            plt.title("J_y")
            plt.xlabel("x")
            plt.ylabel("y")
            plt.colorbar(shrink = .5)
            plt.savefig(prefix + "jy/" + fname  + ".png")
            plt.close()
        if 'jz' in fname:
            plt.figure(figsize=(12, 10))
            plt.imshow(np.loadtxt(ifname))
            plt.title("J_z")
            plt.xlabel("x")
            plt.ylabel("y")
            plt.colorbar(shrink = .5)
            plt.savefig(prefix + "jz/" + fname  + ".png")
            plt.close()
        if 'rho' in fname:
            plt.figure(figsize=(12, 10))
            plt.imshow(np.loadtxt(ifname))
            plt.title("rho")
            plt.xlabel("x")
            plt.ylabel("y")
            plt.colorbar(shrink = .5)
            plt.savefig(prefix + "rho/" + fname  + ".png")
            plt.close()
        if 'ele' in fname:
            plt.figure(figsize=(12, 10))
            plt.imshow(np.loadtxt(ifname))
            plt.title("e-")
            plt.xlabel("x")
            plt.ylabel("y")
            plt.colorbar(shrink = .5)
            plt.savefig(prefix + "ele/" + fname  + ".png")
            plt.close()
        if 'ion' in fname:
            plt.figure(figsize=(12, 10))
            plt.imshow(np.loadtxt(ifname))
            plt.title("ion")
            plt.xlabel("x")
            plt.ylabel("y")
            plt.colorbar(shrink = .5)
            plt.savefig(prefix + "ion/" + fname  + ".png")
            plt.close()








