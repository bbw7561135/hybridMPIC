function mycombine2d(midname, rank)
timef = ls('*time.txt');
prefix = regexp(timef, '_', 'split');
prefix = prefix(1);
time = load([prefix{1}, '_time.txt'], '-ascii');
len = length(time(:, 1));
for i = 1:len
    num = int32(time(i, 1));
    if num < 10
        num = strcat('0000', num2str(num));
    elseif num < 100
        num = strcat('000', num2str(num));
    elseif num < 1000
        num = strcat('00', num2str(num));
    elseif num < 10000
        num = strcat('0', num2str(num));
    else
        num = num2str(num);
    end
    data = 0;
    for j = 1:rank
        fname = strcat(prefix, '_', num, '_rank_', num2str(j-1), '_', midname, '.txt');
        if j == 1
            data = load(fname{1}, '-ascii');
        else
            data = [data, load(fname{1}, '-ascii')];
        end
    end
    fname = strcat(prefix, '_', num, '_', midname, '.txt')
    save(fname{1}, 'data', '-ascii');
end
end