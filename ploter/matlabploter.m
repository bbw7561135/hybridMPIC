datadir = '../data';
figdir = '../figures/';
cd(datadir);
data_name = dir('*.txt');
time_name = dir('*time.txt');
timer = readtable(time_name.name);
for i = 1:length(data_name)
    if(sum(ismember('time',data_name(i).name)) == 0)
        continue;
    end
    data=load(data_name(i).name);
    figname = [figdir, data_name(i).name, '.png'];
    disp(['ploting ', figname])
    hand = figure();
    imagesc(data);colorbar();
    title(figname);
    xlabel('x');
    ylabel('y');
    set(hand, 'visible', 'off');
    saveas(hand, figname);
    delete(hand);
end
