#!/usr/bin/python
import subplots as sp
import os
fname = os.listdir("./../data")
abbrev = "./../data/"
figabbrev = "./../figures/"
for ff in fname:
    if ".txt" in ff and ".png" not in ff:
        print ff, "is plotting"
        sp.mysubplots(abbrev, ff, figabbrev)

