#!/usr/bin/python
import exunequal as sp
import exhistplot as hp
import os
import numpy as np
rootdir = "./../onehalf/"
fname = os.listdir(rootdir)
abbrev = rootdir
figabbrev = "./../onefigures/"
tdict = {}
for ff in fname:
    if "time" in ff:
        timer = np.loadtxt(rootdir + ff)
        for i in np.arange(timer.shape[0]):
            tdict[int(timer[i][0])] = timer[i][1]
for ff in fname:
    if ".txt" in ff and ".png" not in ff and "rank" not in ff and "time" not in ff:
        print ff, "is plotting"
        sp.mysubplots(abbrev, ff, figabbrev, tdict)
fname = os.listdir(rootdir + "ptcls")
abbrev = rootdir + "ptcls"
figabbrev = "./../figures/"
for ff in fname:
    if ".txt" in ff and ".png" not in ff and "rank" not in ff and "time" not in ff:
        print ff, "is plotting"
        hp.plots(abbrev, ff, figabbrev, tdict)

